const { series, parallel } = require('gulp');
var gulp = require('gulp');
var clean = require('gulp-clean');
const { task } = require('gulp');
const { src, dest } = require('gulp');
const uglify_js = require('uglify-es');
const uglify_css = require('uglifycss');
const gp_concat = require('gulp-concat');
const through2 = require('through2');
const fs = require('fs');

const inputPath='../';
const outputPath='../../QTrace_release/';

function qadmin(cb) {
	parallel(qadminJS,qadminCSS);
	cb();
}

function qinterface(cb) {
	parallel(qinterfaceJS,qinterfaceCSS);
	cb();
}

function qanalysis(cb) {
	parallel(qanalysisJS,qanalysisCSS);
	cb();
}

function qadminJS(cb) {
	minifyJSs([	inputPath+'js/QAdmin.js'],
				outputPath+'js/QAdmin.min.js');
	cb();
}

function qinterfaceJS(cb) {
	minifyJSs([	inputPath+'js/QInterface.js'],
				outputPath+'js/QInterface.min.js');
	cb();
}

function qanalysisJS(cb) {
	minifyJSs([inputPath+'js/QAnalysis.js'],outputPath+'js/QAnalysis.min.js');
	minifyJSs([inputPath+'js/QAnalysisWasm.js'],outputPath+'js/QAnalysisWasm.min.js');
	copyFiles([inputPath+'js/QAnalysisWasm.wasm'],outputPath+'js/');
	cb();
}

function liQertJS(cb) {
	minifyJSs([	inputPath+'js/LiQert.js'],
				outputPath+'js/LiQert.min.js');
	cb();
}

function minifyJSs(names,output)
{
	return src(names)
			.pipe(through2.obj(function(file,_,cb){if(file.isBuffer()){const code=uglify_js.minify(file.contents.toString());if(code.code){file.contents=Buffer.from(String(code.code));console.log('Minify done.');cb(null,file);return;}else{cb(new Error(JSON.stringify(code, null, 4)));return;}}}))
			.pipe(gp_concat(output))
			.pipe(clean({force: true}))
			.pipe(through2.obj(function(file,_,cb){fs.open(output,'a+',function(err,fd){if(err){throw'could not open file:'+err;}fs.write(fd,file.contents,0,file.contents.length,null,function(err){if(err)throw'error writing file:'+err;fs.close(fd,function(){console.log('wrote the file successfully');});})})}));
}

function copyFiles(names,outputFolder)
{
	return src(names)
			.pipe(dest(outputFolder));
}

function qadminCSS(cb) {
	minifyCSSs([inputPath+'css/QAdmin.css',inputPath+'css/QInterface.css',inputPath+'css/TextEditor.css'],
				outputPath+'css/QAdmin.min.css');
	cb();
}

function qinterfaceCSS(cb) {
	minifyCSSs([	inputPath+'css/QInterface.css',inputPath+'css/d&dManager.css'],
				outputPath+'css/QInterface.min.css');
	cb();
}

function qanalysisCSS(cb) {
	cb();
}

function copyCSS(cb)
{
	copyFiles([inputPath+'css/QAdmin.css',inputPath+'css/QInterface.css',inputPath+'css/TextEditor.css',inputPath+'css/d&dManager.css',inputPath+'css/inconsolata.css',inputPath+'css/materialize.min.css'],outputPath+'css/');
	cb();
}

function copyConfig(cb)
{
	copyFiles([inputPath+'admin',inputPath+'analysis'],outputPath+'css/');
	cb();
}

function minifyCSSs(names,output)
{
	return src(names)
			//.pipe(through2.obj(function(file,_,cb){if(file.isBuffer()){const code=uglify_css.minify(file.contents.toString());if(code.code){file.contents=Buffer.from(String(code.code));console.log('Minify done.');cb(null,file);return;}else{cb(new Error(JSON.stringify(code, null, 4)));return;}}}))
			.pipe(gp_concat(output))
			.pipe(through2.obj(function(file,_,cb){fs.open(output,'a+',function(err,fd){if(err){throw'could not open file:'+err;}fs.write(fd,file.contents,0,file.contents.length,null,function(err){if(err)throw'error writing file:'+err;fs.close(fd,function(){console.log('wrote the file successfully');});})})}));
}

exports.default = parallel(qadminJS,qinterfaceJS,qanalysisJS,copyCSS,copyConfig);
exports.qadmin = parallel(qadminJS,/*qadminCSS*/copyCSS,copyConfig);
exports.qinterface = parallel(qinterfaceJS,/*qinterfaceCSS*/copyCSS);
exports.qanalysis = parallel(qanalysisJS,/*qanalysisCSS*/copyCSS,copyConfig);
exports.liqert = parallel(liQertJS,/*qanalysisCSS*/copyCSS,copyConfig);