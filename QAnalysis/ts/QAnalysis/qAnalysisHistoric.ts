import { HistoricEvent, Historic } from "../Global/historic"

/**
 * Local Storage 
 */

export class QAnalysisHistoric extends Historic
{
	constructor(storageName: string) {
		super(storageName);
	}

	GetLastStatement(id: string): HistoricEvent {
		return this.FindLastEvent(this.currentHistoricalIndex - 1, "statement", id);
	}

	GetLastQSort(): HistoricEvent {
		return this.FindLastEvent(this.currentHistoricalIndex - 1, "qsort");
	}

	GetLastThreeStates(id: string): HistoricEvent {
		return this.FindLastEvent(this.currentHistoricalIndex - 1, "threestates", id);
	}

	GetLastExoQuizz(): HistoricEvent {
		return this.FindLastEvent(this.currentHistoricalIndex - 1, "exoquizz");
	}

	GetLastWallOfText(id: string): HistoricEvent {
		return this.FindLastEvent(this.currentHistoricalIndex - 1, "texts", id);
	}

	GetLastInterview(): HistoricEvent {
		return this.FindLastEvent(this.currentHistoricalIndex - 1, "interview");
	}

	GetLastHeadPos(): HistoricEvent {
		return this.FindLastEvent(this.currentHistoricalIndex - 1, "isCrescent");
	}

	GetLastStatAlign(): HistoricEvent {
		return this.FindLastEvent(this.currentHistoricalIndex - 1, "statAlign");
	}

	GetLastActiveInterview(): HistoricEvent {
		return this.FindLastEvent(this.currentHistoricalIndex - 1, "Interview");
	}

	GetLastActiveExogen(): HistoricEvent {
		return this.FindLastEvent(this.currentHistoricalIndex - 1, "Exogen");
	}

	GetLastIsDeck(id: string): HistoricEvent {
		return this.FindLastEvent(this.currentHistoricalIndex - 1, "threeStatesDeck", id);
	}

}