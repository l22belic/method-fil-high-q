import { DOMGenerator, getAlignementType } from "../Global/domGenerator"
import { QAnalysisProcessor } from "./qAnalysisProcessor"
import { TextEditor } from "../Global/textEditor"
import { Tab, Statement } from "../Global/types";
import { GraphGenerator } from "../Global/graphGenerator"


/**
 * IHM
 */

export class QAnalysisGenerator extends DOMGenerator {
	processor: QAnalysisProcessor;
	graphGenerator: GraphGenerator;
	editor: TextEditor;
	interviewAdditive: boolean;
	interviewShow: Array<number>;

	constructor() {
		super();
		this.interviewAdditive = false;
		this.interviewShow = new Array<number>();
	}


	GetQSortCheckFunctor(): Function {
		return function (manager: QAnalysisGenerator) { return function () { if (document.getElementById("qSort") != null) this.GetMain().removeChild(document.getElementById("qSort")); } }(this);
	}

	GetQSortInsertToTitleCellFunctor(): Function {
		return function (qConfig: Array<Tab>, node: HTMLElement, iterator: number, tab: Array<number>) { node.innerHTML = qConfig[tab[iterator]].title; };
	}

	GetQSortInsertFunctor(parent: HTMLElement): Function {
		return function (manager: QAnalysisGenerator, parent: HTMLElement) {
			return function (node) {
				parent.appendChild(node);
			};
		}(this, parent);
	}

	GenerateStatement(statement: Statement): HTMLElement {
		if (!statement)
			return;
		if (statement.type === "text") {
			let text: HTMLElement = document.createElement("div");
			text.innerHTML = statement.value;
			return text;
		}
		if (statement.type === "image") {
			let image: HTMLImageElement = document.createElement("img");
			image.setAttribute("src", document.baseURI + statement.value);
			if (statement.description) {
				let div: HTMLDivElement = document.createElement("div");
				div.appendChild(image);
				div.appendChild(document.createTextNode(statement.description));
				return div;
			}
			return image;
		}
		if (statement.type === "audio") {
			let audio: HTMLAudioElement = new Audio(document.baseURI + statement.value);
			audio.id = 'audio-player-' + statement.id;
			audio.controls = true;
			if (statement.description) {
				let div: HTMLDivElement = document.createElement("div");
				div.appendChild(audio);
				div.appendChild(document.createTextNode(statement.description));
				return div;
			}
			if (statement.illustration) {
				let div: HTMLDivElement = document.createElement("div");
				div.appendChild(audio);
				let image: HTMLImageElement = document.createElement("img");
				div.appendChild(image);
				image.setAttribute("src", document.baseURI + statement.illustration);
				return div;
			}
			return audio;
		}
		if (statement.type === "video") {
			let video: HTMLMediaElement = document.createElement("video");
			video.id = 'video-player-' + statement.id;
			video.controls = true;
			video.src = statement.value;
			return video;
		}
		alert(this.processor.configuration.unknowstatementype);
		return null;
	}

	InsertStatementInQSort(statement: HTMLElement) {
		let tabID: string = this.processor.GetStatementPositionInQSort(statement.id);
		if (!tabID.length)
			alert("bug");
		let qTab: HTMLElement = document.getElementById(tabID);
		if (qTab.childNodes.length === 1)
			qTab.firstChild.appendChild(statement);
		else {
			for (let iterator: number = 0; iterator < qTab.childNodes.length; iterator++) {
				if (!qTab.childNodes[iterator].hasChildNodes()) {
					qTab.childNodes[iterator].appendChild(statement)
					break;
				}
			}
		}
	}

	GenerateStatements() {
		for (let key in this.processor.studyConfiguration.statements) {
			let li: HTMLDivElement = document.createElement('div');
			li.id = this.processor.studyConfiguration.statements[key].id.toString();
			let liId: HTMLSpanElement = li.appendChild(document.createElement("span"));
			liId.innerHTML = "<b>" + (li.id.length > 1 ? li.id : "0" + li.id) + "</b>";
			liId.style.backgroundColor = "black";
			liId.style.color = "white";
			liId.style.border = "border: 1px solid";
			liId.style.borderRadius = "6px";
			li.appendChild(this.GenerateStatement(this.processor.studyConfiguration.statements[key]));
			li.className = "nested-item noselect";
			li["baseFontSize"] = 1.2;
			li.style.width = Math.ceil(320 * this.processor.studyConfiguration.scalinganimationminvalue) + "px";
			li.style.height = Math.ceil(180 * this.processor.studyConfiguration.scalinganimationminvalue) + "px";
			li.style.fontSize = (li["baseFontSize"] * this.processor.studyConfiguration.scalinganimationminvalue) + "em";
			this.InsertStatementInQSort(li);
		}
	}

	GenerateInterviewDiv(cardid: string) {
		let tooltip: HTMLElement = document.getElementById("interview");
		tooltip.style.visibility = "visible";
		let id: number = Number.parseInt(cardid);
		if (this.interviewShow.includes(id))
			return;
		let html: string = "";
		for (let iteratorInterv in this.processor.interview[id]) {
			html += "<b><u>User:</u> " + this.processor.interview[id][iteratorInterv].user + "<br><u>Card:</u>" + cardid + "</b><br>";
			html += "<i><u>Position:</u> " + this.processor.interview[id][iteratorInterv].category + "</i><br>";
			html += this.processor.interview[id][iteratorInterv].text + "<br>";
		}
		if (this.interviewAdditive)
			tooltip.innerHTML += html;
		else
			tooltip.innerHTML = html;
		this.interviewShow.push(id);
	}

	CleanInterviewDiv() {
		let tooltip: HTMLElement = document.getElementById("interview");
		tooltip.style.visibility = "hidden";
		if (!this.interviewAdditive) {
			tooltip.innerHTML = "";
			this.interviewShow = new Array<number>();
		}
	}

	LinkInterviews() {
		for (let iterator in this.processor.interview) {
			let card: HTMLElement = document.getElementById(iterator);
			card.onmouseenter = function (manager: QAnalysisGenerator, id: string) { return function (event: MouseEvent) { manager.GenerateInterviewDiv(id); } }(this, card.id);
			card.onclick = function (manager: QAnalysisGenerator) { return function (event: MouseEvent) { manager.interviewAdditive = !manager.interviewAdditive } }(this);
			card.onmouseleave = function (manager: QAnalysisGenerator) { return function (event: MouseEvent) { manager.CleanInterviewDiv(); } }(this);
			card.style.backgroundColor = "AliceBlue";
		}
		let div: HTMLDivElement = document.createElement("div");
		div.id = "interview";
		//div.style.position = "absolute";
		this.GetMain().insertBefore(div, document.getElementById("qSort"));
	}

	GenerateQSortResult() {
		let main: HTMLElement = this.GetMain();
		this.GenerateQSort(this.processor.studyConfiguration.qSortConfiguration.isCrescent,
			getAlignementType(this.processor.studyConfiguration.qSortConfiguration.statementsAlignement),
			this.processor.studyConfiguration.qSortConfiguration.qSort,
			this.processor.studyConfiguration.statements.length,
			this.GetQSortCheckFunctor(),
			this.GetQSortInsertToTitleCellFunctor(),
			this.GetQSortInsertFunctor(main));
		this.GenerateStatements();
		this.LinkInterviews();
	}

	ToPdf()
	{
		//var pdf = new jsPDF('p', 'pt', 'letter');
		// source can be HTML-formatted string, or a reference
		// to an actual DOM element from which the text will be scraped.
		//source = $('#content')[0];

		// we support special element handlers. Register them with jQuery-style 
		// ID selector for either ID or node name. ("#iAmID", "div", "span" etc.)
		// There is no support for any other type of selectors 
		// (class, of compound) at this time.
		/*specialElementHandlers = {
			// element with id of "bypass" - jQuery style selector
			'#bypassme': function (element, renderer) {
				// true = "handled elsewhere, bypass text extraction"
				return true
			}
		};
		margins = {
			top: 80,
			bottom: 60,
			left: 40,
			width: 522
		};
		// all coords and widths are in jsPDF instance's declared units
		// 'inches' in this case
		pdf.fromHTML(
			source, // HTML string or DOM elem ref.
			margins.left, // x coord
			margins.top, { // y coord
			'width': margins.width, // max width of content on PDF
			'elementHandlers': specialElementHandlers
		},

			function (dispose) {
				// dispose: object with X, Y of the last line add to the PDF 
				//          this allow the insertion of new lines after html
				pdf.save('Test.pdf');
			}, margins
		);*/
	}

	GenerateGeneralPage()
	{
		let main = this.GetMain();
		this.CleanMain();
		let subdiv = document.createElement("div");
		let table = subdiv.appendChild(document.createElement("table"));
		table.style.width = "100%";
		let row = table.insertRow();
		let cell = row.insertCell();
		cell.style.width = "50%";
		this.GenerateDropZone("studydropZone", this.processor.configuration.loadConfigurationFile, "", cell, "*", false, function (dom: QAnalysisGenerator) { return function (files) { dom.processor.ProcessConfigFile(files[0], dom); }; }(this));
		cell = row.insertCell();
		cell.style.width = "50%";
		this.GenerateDropZone("tracesdropZone", this.processor.configuration.loadResultsFiles, "", cell, "*", true, function (processor: QAnalysisProcessor) { return function (files) { processor.ProcessTraceFiles(files); }; }(this.processor));
		main.appendChild(this.GenSubTitle("1." + this.processor.configuration.loadStudyFiles, subdiv, this.processor.configuration.loadStudyFilesDesc));

		subdiv = document.createElement("div");
		table = subdiv.appendChild(document.createElement("table"));
		row = table.insertRow();
		cell = row.insertCell();
		//cell.style.width = "25%";
		let button = cell.appendChild(document.createElement("button"));
		button.appendChild(document.createTextNode(this.processor.configuration.downloadPQMethodFile));
		button.onclick = function (processor: QAnalysisProcessor) { return function () { processor.GetPQMethodResult("PQMethod.dat"); }; }(this.processor);
		cell = row.insertCell();
		//cell.style.width = "25%";
		button = cell.appendChild(document.createElement("button"));
		button.appendChild(document.createTextNode(this.processor.configuration.downloadExoDataFile));
		button.onclick = function (processor: QAnalysisProcessor) { return function () { processor.GetExogenData("ExoData.csv"); }; }(this.processor);
		cell = row.insertCell();
		//cell.style.width = "25%";
		button = cell.appendChild(document.createElement("button"));
		button.appendChild(document.createTextNode(this.processor.configuration.downloadInterviewFile));
		button.onclick = function (processor: QAnalysisProcessor) { return function () { processor.GetInterviews("Interview.csv"); }; }(this.processor);;
		cell = row.insertCell();
		//cell.style.width = "25%";
		button = cell.appendChild(document.createElement("button"));
		button.appendChild(document.createTextNode(this.processor.configuration.downloadExoGraphsFile));
		button.onclick = function (processor: QAnalysisProcessor) { return function () { processor.GetExogenStats("exoGraph.json"); }; }(this.processor);
		cell = row.insertCell();
		//cell.style.width = "25%";
		button = cell.appendChild(document.createElement("button"));
		button.appendChild(document.createTextNode(this.processor.configuration.downloadSetupGraphsFile));
		button.onclick = function (processor: QAnalysisProcessor) { return function () { processor.GetSetupStats("setupGraph.json"); }; }(this.processor);
		main.appendChild(this.GenSubTitle("2." + this.processor.configuration.exportProcess, subdiv, null));
		subdiv.id = "processDiv";

		subdiv = document.createElement("div");
		table = subdiv.appendChild(document.createElement("table"));
		row = table.insertRow();

		cell = row.insertCell();
		this.GenerateDropZone("tracesdropZone", "Repair", "", cell, "*", true, function (processor: QAnalysisProcessor) { return function (files) { processor.RepairTraceFiles(files); }; }(this.processor));

		cell = row.insertCell();
		button = cell.appendChild(document.createElement("button"));
		button.appendChild(document.createTextNode("dump"));
		button.onclick = function (processor: QAnalysisProcessor) { return function () { processor.SaveDataBase("dump.sql"); }; }(this.processor);

		cell = row.insertCell();
		this.GenerateDropZone("restoresql", "sqlrestore", "", cell, "*.sql", false, function (dom: QAnalysisGenerator) { return function (files) { dom.processor.ProcessSQLFile(files[0], dom); }; }(this));

		cell = row.insertCell();
		this.GenerateDropZone("loadGraph", "loadGraph", "", cell, "*.json", false, function (dom: QAnalysisGenerator) { return function (files) { dom.processor.ProcessSQLFile(files[0], dom); }; }(this));

		cell = row.insertCell();
		button = cell.appendChild(document.createElement("button"));
		button.appendChild(document.createTextNode("testing"));
		button.onclick = function (manager: QAnalysisGenerator) { return function () { manager.processor.TestMethod(); manager.GenerateQSortResult(); }; }(this);

		cell = row.insertCell();
		button = cell.appendChild(document.createElement("button"));
		button.appendChild(document.createTextNode("pdf"));
		button.onclick = function (manager: QAnalysisGenerator) { return function () { manager.ToPdf();}; }(this);
	
		main.appendChild(this.GenSubTitle(" In dev actions", subdiv, null));
		subdiv.id = "processDiv";
	}
}