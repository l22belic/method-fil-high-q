export class QAnalysisWasmModule
{   
	constructor()
	{}
	
	AddFileToDatabase(stream)
	{
		Module.ccall("addFileToDatabase", 'void', ["string"], [stream]);
	}

	RestoreDatabase(stream)
	{
		Module.ccall("restoreDatabase", 'void', ["string"], [stream]);
	}

	DumpDatabase()
	{
		return Module.ccall("dumpDatabase", "string");
	}

	GetPQMethodResult()
	{
		return Module.ccall("getPQMethodResult","string");
	}

	GetInterviews()
	{
		return Module.ccall("getInterviews", "string");
	}

	GetExogenData()
	{
		return Module.ccall("getExogenData", "string");
	}

	GetExogenStats()
	{
		return Module.ccall("getExogenStats", "string");
	}

	GetSetupStats()
	{
		return Module.ccall("getSetupStats", "string");
	}

	GetGraphsData()
	{
		return Module.ccall("getGraphsData", "string");
	}

	GetQSortForUser(id)
	{
		return Module.ccall("getQSortForUser", "string",["number"],[id]);
	}

	GetInterviewsForUsers(ids)
	{
		return Module.ccall("getInterviewsForUsers", "string", ["array", "number"], [new Uint8Array(new Uint32Array(ids).buffer), ids.length]);
	}

	GetExogenDataForUsers(ids)
	{
		return Module.ccall("getExogenDataForUsers", "string", ["array", "number"], [new Uint8Array(new Uint32Array(ids).buffer), ids.length]);
	}

	GetUserCount()
	{
		return Module.ccall("getNumberOfUser", "number");
	}
};