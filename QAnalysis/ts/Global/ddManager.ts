/*
*   Copyright 2019 Aurélien Milliat <aurelien.milliat@gmail.com>
*   Licensed under the Apache License, Version 2.0 (the "License");
*   you may not use this file except in compliance with the License.
*   You may obtain a copy of the License at
*
*       http://www.apache.org/licenses/LICENSE-2.0
*
*  Unless required by applicable law or agreed to in writing, software
*  distributed under the License is distributed on an "AS IS" BASIS,
*  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
*  See the License for the specific language governing permissions and
*  limitations under the License.
*
*/

import { DOMGenerator } from "./DOMGenerator";

/**
 * Basic interface & Type
 */

interface IPostion
{
	x: number;
	y: number;
}

export type Position = IPostion;

/**
 * DragAndDropManager
 */

export class DragAndDropManager
{
    containerClassName: string;
	itemClassName: string;
	dom: DOMGenerator;
	dragstartlistner: Function;
	dragendlistner: Function;
	draggablelistner: Function;
	isacceptchildlistner: Function;

	activeNode: HTMLElement;
	activateClone: boolean;
	cloneNode: HTMLElement;
	oldMousePosition: Position;
	mouseDistance: number;
	isMobile: boolean;

	constructor(dom: DOMGenerator, itemClassName: string, containerClassName: string, dragstartlistner: Function, dragendlistner: Function, draggablelistner: Function, isacceptchildlistner: Function, activateClone: boolean)
	{
		this.dom = dom;
		this.containerClassName = containerClassName;
		this.itemClassName = itemClassName;
		this.dragstartlistner = dragstartlistner;
		this.dragendlistner = dragendlistner;
		this.draggablelistner = draggablelistner;
		this.isacceptchildlistner = isacceptchildlistner;
		this.activeNode = null;
		this.cloneNode = null;
		this.activateClone = activateClone;
		this.mouseDistance = 0;
		this.oldMousePosition = { x: -1, y: -1 };

		window['mobileAndTabletcheck'] = function () {
			var check = false;
			(function (a) { if (/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino|android|ipad|playbook|silk/i.test(a) || /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0, 4))) check = true; })(navigator.userAgent || navigator.vendor /*|| window.opera*/);
			return check;
		};
		this.isMobile = window['mobileAndTabletcheck'];
	}

	IsDragging(): boolean
	{
		return this.activeNode !== null;
	}

	CleanCloneNode()
	{  
		if (this.cloneNode)
			document.body.removeChild(this.cloneNode);
		this.cloneNode=null;
		this.activeNode = null;
		this.mouseDistance = 0;
		this.oldMousePosition.x = -1;
	}

	Start()
	{
		this.activeNode = null;
		this.CleanCloneNode();
		let containers: HTMLCollectionOf<Element> = document.getElementsByClassName(this.containerClassName);
		for (let iterator: number = 0; iterator < containers.length; iterator++)
		{
			let htmlNode: HTMLElement = <HTMLElement>containers[iterator];
			htmlNode.onmousemove = function (manager: DragAndDropManager) { return function (event: MouseEvent) { if (manager.activeNode) manager.OnMouseMove(event); }; }(this);
		}

		let items: HTMLCollectionOf<Element> = document.getElementsByClassName(this.itemClassName);
		for (let iterator: number= 0; iterator < items.length; iterator++)
		{
			let htmlNode: HTMLElement = <HTMLElement>items[iterator];
			htmlNode.onclick = function (manager: DragAndDropManager) { return function (event: MouseEvent) { if (!manager.activeNode) manager.OnDragStart(event); }; }(this);
		}
	}
	
	Stop()
	{
        this.activeNode=null;
		this.CleanCloneNode();
		let containers: HTMLCollectionOf<Element> = document.getElementsByClassName(this.containerClassName);
		for (let iterator: number = 0; iterator < containers.length; iterator++)
		{
			let htmlNode: HTMLElement = <HTMLElement>containers[iterator];
			htmlNode.onmousemove = null;
		}

		let items: HTMLCollectionOf<Element>=document.getElementsByClassName(this.itemClassName);
		for (let iterator: number = 0; iterator < items.length; iterator++) {
			let htmlNode: HTMLElement = <HTMLElement>items[iterator];
			htmlNode.onclick = null;
		}
	}

	MoveCard(event: MouseEvent)
	{
		if (this.cloneNode)
		{
			this.cloneNode.style.left = (event.pageX + 1) + "px";
			this.cloneNode.style.top = (event.pageY + 1) + "px";
			if (this.oldMousePosition.x != -1)
			{
				let x: number = event.pageX - this.oldMousePosition.x;
				let y: number = event.pageY - this.oldMousePosition.y;
				this.mouseDistance += Math.sqrt(x * x + y * y);
			}
			this.oldMousePosition.x = event.pageX;
			this.oldMousePosition.y = event.pageY; 
		}
		else
		{
			document.onmousemove = null;
			this.mouseDistance = 0;
		}
	}

	OnDragStart(event: MouseEvent)
	{
		let card: HTMLElement = this.dom.GetParentWithClassName(<HTMLElement>event.target,this.itemClassName);
		if(!card)
			return;
		let parentNode: HTMLElement = <HTMLElement>card.parentNode;
		this.dragstartlistner(card.id, this.dom.GetParentWithId(parentNode, null, true).id);
		this.activeNode = card;
		this.activeNode.classList.add("dragged");
		if (this.activateClone)
		{
			this.cloneNode = <HTMLElement>this.activeNode.cloneNode(true);
			document.body.appendChild(this.cloneNode);
			this.cloneNode.classList.add("clone");
			this.cloneNode.id += "clone";
		}
		this.MoveCard(event);
		event.stopPropagation();
		document.onclick = function (manager: DragAndDropManager) { return function (event: MouseEvent) { event.stopPropagation(); event.preventDefault(); manager.OnDragStop(event); }; }(this);
		document.onmousemove = function (manager: DragAndDropManager) { return function (event: MouseEvent) { manager.MoveCard(event); }; }(this);
		document.oncontextmenu = function (event) { event.stopPropagation(); event.preventDefault(); };
	}

	OnDragStop(event: MouseEvent)
	{
		document.onclick = null;
		document.oncontextmenu = null;
		document.onmousemove = null;
		if (this.activeNode.classList.contains('dragged'))
			this.activeNode.classList.remove('dragged');
		let parentNode = <HTMLElement>this.activeNode.parentNode;
		this.dragendlistner(this.activeNode.id, this.dom.GetParentWithId(parentNode, null, true).id);
		this.CleanCloneNode();	
	}

	OnMouseMove(event: MouseEvent)
	{
		//10% of height
		if (!this.isMobile && this.mouseDistance < window.innerHeight/10)
			return;
		this.mouseDistance = 0;
		let container: HTMLElement = this.isacceptchildlistner(event.target, this.activeNode);
        if(container)
        {
        	if(this.activeNode.parentNode !== container)
				this.draggablelistner(this.activeNode.id, this.dom.GetParentWithId(container, null, true).id);
			let insertion: HTMLElement=this.GetNodeBeforeInsertion(container,{x:event.clientX,y:event.clientY});
			if(insertion)
				container.insertBefore(this.activeNode,insertion);
			else
				container.appendChild(this.activeNode);
         }
    }

	GetNodeBeforeInsertion(container: HTMLElement, position: Position): HTMLElement
	{
		let canBe: Array<number> = new Array<number>();
		for (let iterator: number = 0; iterator < container.childNodes.length; iterator++)
		{
			let htmlNode: HTMLElement = <HTMLElement>container.childNodes[iterator];
			let bcr: ClientRect = htmlNode.getBoundingClientRect();
			if(position.y > bcr.top && position.y <= bcr.bottom)
				canBe.push(iterator);
		}
		if(!canBe.length)
			return null;
		let htmlNode: HTMLElement = <HTMLElement>container.childNodes[canBe[canBe.length - 1]]
		let bcr: ClientRect = htmlNode.getBoundingClientRect();
		let size:number=(bcr.right-bcr.left)/2;
		if(bcr.right-size<position.x)
			return null;
		let nodeBefore: HTMLElement = <HTMLElement>container.childNodes[canBe[0]];
		for (let iterator = canBe.length - 1; iterator >= 0; iterator--)
		{
			htmlNode = <HTMLElement>container.childNodes[canBe[iterator]]
			bcr = htmlNode.getBoundingClientRect();
			size = (bcr.right - bcr.left) / 2;
			if (position.x > bcr.left + size) 
				return <HTMLElement>(container.childNodes[canBe[iterator] + 1 >= container.childNodes.length ? canBe[iterator] : canBe[iterator] + 1]);
		}	
		return nodeBefore;
	}
}