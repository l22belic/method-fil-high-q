/*
*   Copyright 2019 Aur�lien Milliat <aurelien.milliat@gmail.com>
*   Licensed under the Apache License, Version 2.0 (the "License");
*   you may not use this file except in compliance with the License.
*   You may obtain a copy of the License at
*
*       http://www.apache.org/licenses/LICENSE-2.0
*
*  Unless required by applicable law or agreed to in writing, software
*  distributed under the License is distributed on an "AS IS" BASIS,
*  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
*  See the License for the specific language governing permissions and
*  limitations under the License.
*
*/

import tauCharts, { Chart } from 'taucharts';

import { Dictionary } from './types'

interface IGraph
{
    general: Dictionary<HTMLElement>;
    phase: Dictionary<HTMLElement>;
    individual: Dictionary<HTMLElement>;
}

type Graph = IGraph;


export class GraphGenerator
{
    Filter4DPlugin: Function;
    indicators: Graph;
    selection: Array<number>;
    counter: number;
    activeNode: HTMLElement;
    section: string;
    ind: string;

    constructor()
    {
        this.counter = 0;
        this.activeNode = null;
        this.ind = null;
        this.section = null;
        this.Generate4DPlugin(); 
    }

    LoadGraphs(data: any, cluster?: Dictionary<HTMLElement>)
    {
        for (let key in data)
        {
        switch (data[key].type)
        {
            case 'histogram':
                this.GenerateHistogram(data[key], cluster, this.section, this.ind);
                break;
            case 'stacked-histogram':
                this.GenerateStackedHistogram(data[key], cluster, this.section, this.ind);
                break;
            case 'stacked-histogram-w':
                this.GenerateStackedHistogram4D(data[key], cluster, this.section, this.ind);
                break;
            case 'scatterplot':
                this.GenerateScatterPlot(data[key], cluster, this.section, this.ind);
                break;
            case 'indicators':
                this.section = data[key].parameter.title;
                this.LoadGraphs(data[key].data, this.indicators.phase);
                this.section = null;
                break;
            case 'individuals':
                this.LoadGraphs(data[key].data);
                break;
            case 'individual':
                this.ind = data[key].id;
                this.LoadGraphs(data[key].data, this.indicators.individual);
                this.ind = null;
                break;
            default:
                continue;
        }
    }
}

    CalculateNexDisplayZone(target: any)
    {
        if (!target || !target.childNodes[0] || !target.childNodes[1] || !("chart" in target))
            return;
        let style: CSSStyleDeclaration = window.getComputedStyle(target);
        let offset: number = parseInt(style.getPropertyValue("padding")) + parseFloat(style.getPropertyValue("border")) * 2;
        let width: number = target.clientWidth - offset;
        style = window.getComputedStyle(target.childNodes[0]);
        offset += parseFloat(style.getPropertyValue("margin")) * 2 + 10;
        let height: number = target.clientHeight - target.childNodes[0].clientHeight - offset;
        target.childNodes[1].style.width = width + 'px';
        target.childNodes[1].style.height = height + 'px';
        target["chart"].renderTo('#' + target.childNodes[1].id);
    }

    TransfertDisplayZone(target: any)
    {
        if (!target || !target.childNodes[0] || !target.childNodes[1] || !("chart" in target))
            return;
        let style: CSSStyleDeclaration = window.getComputedStyle(this.activeNode);
        let offset: number = parseInt(style.getPropertyValue("padding")) + parseFloat(style.getPropertyValue("border")) * 2;
        let width: number = this.activeNode.clientWidth - offset;
        let node: HTMLElement = <HTMLElement>this.activeNode.childNodes[0];
        style = window.getComputedStyle(node);
        offset += parseFloat(style.getPropertyValue("margin")) * 2 + 10;
        let height: number = this.activeNode.clientHeight - node.clientHeight - offset;
        if (height < 10)
        {
            height = this.activeNode.clientHeight + node.clientHeight + offset;
            setTimeout(function () { target["chart"].renderTo('#' + target.childNodes[1].id); }, 100);
        }
        target.childNodes[1].style.width = width + 'px';
        target.childNodes[1].style.height = height + 'px';
        target["chart"].renderTo('#' + target.childNodes[1].id);
    }

    Hide_(target: HTMLElement)
    {
        let style: CSSStyleDeclaration = window.getComputedStyle(target);
        if (target.style.width !== "")
            target["hideWidth"] = target.style.width;
        else
            target["hideWidth"] = style.getPropertyValue("width");
        if (target.style.height !== "")
            target["hideHeight"] = target.style.height;
        else
            target["hideHeight"] = style.getPropertyValue("height");
        target.style.width = "";
        target.style.height = "";
    }

    Show_(target: HTMLElement)
    {
        target.style.width = target["hideWidth"];
        target.style.height = target["hideHeight"];
    }

    Hide(target: HTMLElement)
    {
        let group: HTMLElement = <HTMLElement>target.parentNode.childNodes[1];
        this.Hide_(group);
        this.Hide_(<any>(target).parentNode);
        group.style.display = "none";
    }

    Show(target: HTMLElement)
    {
        let group: HTMLElement = <HTMLElement>target.parentNode.childNodes[1];
        if (group.style.display !== "" && group.style.display !== "none")
            return;
        this.CalculateNexDisplayZone(target.parentNode);
        group.style.display = "inherit";
        this.Show_(<any>(target).parentNode);
        this.Show_(group);
    }

    ShowHide(target: HTMLElement)
    {
        let group: HTMLElement = <HTMLElement>target.parentNode.childNodes[1];
        if (group.style.display !== "" && group.style.display !== "none") {
            this.Hide_(group);
            this.Hide_(<any>(target).parentNode);
            group.style.display = "none";
        }
        else {
            group.style.display = "inherit";
            this.Show_(<any>(target).parentNode);
            this.Show_(group);
            this.CalculateNexDisplayZone(target.parentNode);
        }
    }

    GenerateSectionTitle(titletext: string, parentNode: HTMLTableElement)
    {
        let cell: HTMLTableCellElement;
        if (!parentNode.rows.length)
            cell = parentNode.insertRow().insertCell();
        else
            cell = parentNode.rows[0].insertCell();
        let title = cell.appendChild(document.createElement("h2"));
        title.appendChild(document.createTextNode(titletext));
        //title.oncontextmenu=function(event){event.preventDefault();event.stopPropagation();let group=event.target.parentNode.childNodes[1];if(group.style.display!=="none")group.style.display="none";else group.style.display="inherit";};
        return parentNode;
    }

    GenerateGraphTitle(data: any, name: string, classNameDiv: string, classNameGraph: string, cluster: Dictionary<HTMLElement>, phase: string, individual: string): HTMLDivElement
    {
        let div: HTMLDivElement = document.createElement("div");
        let obj = {};
        let title: HTMLHeadingElement = div.appendChild(document.createElement("h4"));
        let strtitle: string = data.parameter.title + ' - ' + phase + ':' + individual;
        if (phase) {
            obj["section"] = phase;
            if (individual) {
                strtitle = data.parameter.title + ' - ' + phase + ':' + individual;
                obj["individual"] = individual;
            }
            else {
                strtitle = data.parameter.title + ' - ' + phase;
                obj["individual"] = 0;
            }
        }
        else {
            strtitle = data.parameter.title;
            obj["section"] = "General";
        }
        title.appendChild(document.createTextNode(strtitle));
        console.log(strtitle);
        div.id = strtitle;
        let graph: HTMLDivElement = div.appendChild(document.createElement("div"));
        div.className = "noselect " + classNameDiv;
       // DOMGenerator.SetDraggable(div);
        graph.className = classNameGraph;
        graph.id = name;
        title.oncontextmenu = function (graph: GraphGenerator) { return function (event) { event.preventDefault(); event.stopPropagation(); graph.ShowHide(event.target); }; }(this);
        div.onmousedown = function (graph: GraphGenerator) { return function (event) { graph.activeNode = event.target; };}(this);
        div.onmouseup = function (graph: GraphGenerator) { return function (event) { event.preventDefault(); event.stopPropagation(); graph.CalculateNexDisplayZone(graph.activeNode); graph.activeNode = null; };}(this);
        if (cluster)
            cluster[data.parameter.title] = div;
        return div;
    }

    GenerateHistogram(data: any, cluster: Dictionary<HTMLElement>, phase: string, individual: string) {
        if (!data.length)
            return;
        let name: string = 'bar' + this.counter++;
        let div: HTMLDivElement = this.GenerateGraphTitle(data, name, "graph bar", 'graph-bar', cluster, phase, individual);
        let chart: Chart = new tauCharts.Chart({
            type: 'bar',
            data: data.data,
            x: 'x',
            y: 'y',

            plugins: [tauCharts.api.plugins.get('tooltip')({ fields: ['y'] })/*, tauCharts.api.plugins.get('export-to')({ cssPaths: ['css/taucharts.min.css'] })*/],
            guide: { x: { label: { text: data.parameter.xtitle, padding: 0 } }, y: { label: { text: data.parameter.ytitle, padding: 0 } } },
            dimensions: { x: { type: 'order' } },
            settings: { asyncRendering: true }
        });
        //chart.renderTo('#'+name);
        div["chart"] = chart;
    }

    GenerateStackedHistogram(data: any, cluster: Dictionary<HTMLElement>, phase: string, individual: string)
    {
        if (!data.length)
            return;
        let name: string = 'stacked-bar' + this.counter++;
        let div: HTMLDivElement = this.GenerateGraphTitle(data, name, "graph stacked-bar", 'graph-stacked-bar', cluster, phase, individual);
        var chart: Chart = new tauCharts.Chart({
            type: 'stacked-bar',
            data: data.data,
            x: 'x',
            y: 'y',
            color: 'z',
            plugins: [tauCharts.api.plugins.get('legend')(), tauCharts.api.plugins.get('tooltip')({ fields: ['z', 'y'], formatters: { z: { label: data.parameter.ztitle, format: function (data: string): string { return data; } }, y: { label: data.parameter.ytitle, format: function (data: string): string { return data; } } } })/*, tauCharts.api.plugins.get('export-to')({ cssPaths: ['css/taucharts.min.css'] })*/],
            guide: { x: { label: { text: <string>(data.parameter.xtitle), padding: 0 } }, y: { label: { text: <string>(data.parameter.ytitle), padding: 0 } } },
            dimensions: { x: { type: 'order' } },
            settings: { asyncRendering: true }
        });
        //chart.renderTo('#'+name);
        div["chart"] = chart;
        //DOMGenerator.RegisterResizeEvent(div);
    }

    Filter(min: number, max: number, data: any): any
    {
        let newdata = [];
        for (let key in data) {
            if (data[key].w >= min && data[key].w <= max)
                newdata.push(data[key]);
        }
        return newdata;
    }

    Generate4DPlugin()
    {
        this.Filter4DPlugin = function (graphGenerator: GraphGenerator) {
            return function (settings) {
                let min: number, max: number;
                let data;
                return {
                    init: function (chart) {
                        if (settings.wmin === settings.wmax)
                            return;
                        this._label = document.createElement('label');
                        this._label.textContent = settings.wtitle + " -> ";
                        this._labelmax = document.createElement('label');
                        this._labelmax.textContent = "Max: ";
                        let maxinput = this._labelmax.appendChild(document.createElement('input'));
                        maxinput.type = "number";
                        maxinput.value = settings.wmax;
                        maxinput.max = settings.wmax;
                        maxinput.min = settings.wmin;
                        maxinput.addEventListener('change', function (event) { event.preventDefault(); event.stopPropagation(); max = Number.parseInt(event.target.value); chart.setData(graphGenerator.Filter(min, max, data)); });
                        this._labelmin = document.createElement('label');
                        this._labelmin.textContent = "Min: ";
                        let mininput = this._labelmin.appendChild(document.createElement('input'));
                        mininput.type = "number";
                        mininput.value = settings.wmin;
                        mininput.max = settings.wmax;
                        mininput.min = settings.wmin;
                        mininput.addEventListener('change', function (event) { event.preventDefault(); event.stopPropagation(); min = Number.parseInt(event.target.value); chart.setData(graphGenerator.Filter(min, max, data)); });
                        chart.insertToFooter(this._label);
                        chart.insertToFooter(this._labelmin);
                        chart.insertToFooter(this._labelmax);
                        data = chart.getData();
                        min = settings.wmin;
                        max = settings.wmax;
                    },
                    destroy: function (chart) {
                        if (this._label.parentElement) {
                            this._labelmax.parentElement.removeChild(this._labelmax);
                            this._labelmin.parentElement.removeChild(this._labelmin);
                            this._label.parentElement.removeChild(this._label);
                        }
                    },
                    onRender: function (chart) { }
                };
            };
        }(this);
    }

    GenerateStackedHistogram4D(data: any, cluster: Dictionary<HTMLElement>, phase: string, individual: string)
    {
        if (!data.length)
            return;
        let name: string = 'stacked-bar' + this.counter++;
        let div: HTMLDivElement = this.GenerateGraphTitle(data, name, "graph stacked-bar", 'graph-stacked-bar', cluster, phase, individual);
        var chart: Chart = new tauCharts.Chart({
            type: 'stacked-bar',
            data: data.data,
            x: 'x',
            y: 'y',
            color: 'z',
            plugins: [this.Filter4DPlugin(data.parameter), tauCharts.api.plugins.get('legend')(), tauCharts.api.plugins.get('tooltip')({ fields: ['z', 'y', 'w'], formatters: { z: { label: data.parameter.ztitle, format: function (data: string): string { return data; } }, y: { label: data.parameter.ytitle, format: function (data: string): string { return data; } }, w: { label: data.parameter.wtitle, format: function (data: string): string { return data; } } } })/*, tauCharts.api.plugins.get('export-to')({ cssPaths: ['css/taucharts.min.css'] })*/],
            guide: { x: { label: { text: data.parameter.xtitle, padding: 0 } }, y: { label: { text: data.parameter.ytitle, padding: 0 } } },
            dimensions: { x: { type: 'order' } },
            settings: { asyncRendering: true }
        });
        //chart.renderTo('#'+name);
        div["chart"] = chart;
       // DOMGenerator.RegisterResizeEvent(div);
    }

    GenerateScatterPlot(data: any, cluster: Dictionary<HTMLElement>, phase: string, individual: string)
    {
        let name: string = 'scatterplot' + this.counter++;
        let div: HTMLDivElement = this.GenerateGraphTitle(data, name, 'graph scatterplot', 'graph-scatterplot', cluster, phase, individual);
        let chart: Chart = new tauCharts.Chart({
            type: 'scatterplot',
            data: data.data,
            x: 'x',
            y: 'y',
            size: "z",
            plugins: [tauCharts.api.plugins.get('tooltip')({ fields: ['z'], formatters: { z: { label: data.parameter.ztitle, format: function (data: string): string { return data; }} } })/*, tauCharts.api.plugins.get('export-to')({ cssPaths: ['css/taucharts.min.css'] })*/],
            guide: { x: { label: { text: data.parameter.xtitle, padding: 0 } }, y: { label: { text: data.parameter.ytitle, padding: 0 } }},
            dimensions: { x: { type: 'order' } },
            settings: { asyncRendering: true }
        });
        //chart.renderTo('#'+name);
        div["chart"] = chart;
       // DOMGenerator.RegisterResizeEvent(div);
    }

    GenerateDatatable(titletext: string, parentNode: HTMLElement): HTMLTableElement
    {
        let div: HTMLDivElement = document.createElement("div");
        let title: HTMLHeadingElement = div.appendChild(document.createElement("h2"));
        title.appendChild(document.createTextNode(titletext));
        let table: HTMLTableElement = div.appendChild(document.createElement("table"));
        table.className = "nestable";
        table.align = "center";
        div.className = "noselect";
        if (parentNode)
            parentNode.appendChild(div);
        else
            document.getElementById('main').appendChild(div);
        return table;
    }

    GenerateDataText(titletext: string, datatext: string, parentNode: HTMLTableElement): HTMLTableElement
    {
        let cell: HTMLTableCellElement;
        if (!parentNode.rows.length)
            cell = parentNode.insertRow().insertCell();
        else
            cell = parentNode.rows[0].insertCell();
        let div: HTMLDivElement = cell.appendChild(document.createElement("div"));
        let title: HTMLHeadingElement = div.appendChild(document.createElement("h3"));
        title.appendChild(document.createTextNode(titletext));
        div.appendChild(document.createTextNode(datatext));
        div.className = "nested-item noselect";
        return parentNode;
    }

   /* RemoveGraphs()
    {
        let picking = DOMGenerator.GetPicking();
        if (!picking.childNodes.length)
            return;
        for (let key in indicators.individual)
            for (let key2 in indicators.individual[key])
                if ((this.selection.indexOf(indicators.individual[key][key2].individual)>-1) && indicators.individual[key][key2].element.parentNode === picking)
                    indicators.individual[key][key2].element.parentNode.removeChild(indicators.individual[key][key2].element);
    }

    AddGraphs(value)
    {
        //if (!(value in indicators[name]))
         //   return;
        let chart: any = indicators[name][value];
        if (!chart)
            return;
        for (let key in chart) {
            if ((name !== "individual" || !(this.selection.indexOf(chart[key].individual)>-1)) && !chart[key].element.parentNode)
                DOMGenerator.GetPicking().appendChild(chart[key].element);
        }
    }

    SetSelection(value: string)
    {
        let array: Array<string> = (<any>document.getElementById("comboFilter")).value.split(/[^\d+\-?]/);
        this.selection = new Array<number>();
        if (array.length && array[0] !== "") {
            for (let iterator = 1; iterator <= configuration.individuals; iterator++)
                this.selection.push(iterator);
        }
        else
            return;
        for (let key in array) {
            if (array[key].includes("-")) {
                let domain: Array<string> = array[key].split("\-");
                if (domain.length > 2) {
                    alert("Error: " + array[key] + " is incorrect!");
                    return;
                }
                if (domain[0] < domain[1])
                    this.selection.splice(selection.indexOf(Number.parseInt(domain[0])), 1 + Number.parseInt(domain[1]) - Number.parseInt(domain[0]));
                else
                    this.selection.splice(selection.indexOf(Number.parseInt(domain[1])), 1 + Number.parseInt(domain[0]) - Number.parseInt(domain[1]));
                continue;
            }
            this.selection.splice(selection.indexOf(Number.parseInt(array[key])), 1);
        }
        this.RemoveGraphs();
    }*/
}