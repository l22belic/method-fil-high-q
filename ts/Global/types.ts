/**
 * Global interfaces & types
 */

interface IStatement {
    id: number;
    type: string;
    value: string;
    description?: string;
    illustration?: string;
}

export type Statement = IStatement;
export type Statements = Array<Statement>;

interface IThreeStates {
    agree: Set<string>;
    neutral: Set<string>;
    disagree: Set<string>;
}

export type ThreeStates = IThreeStates;

interface ITab {
    limit?: number;
    title: string;
    type?: string;
}

export type Tab = ITab;
export type Tabs = Array<Tab>;

export interface ISort {
    id: string;
    value: Set<string>;
}

export type Sort = ISort;
export type QSort = Array<Sort>;

export interface Dictionary<T>
{
    [Key: string]: T;
}