import { Tab, Tabs, Statement, Statements } from "../Global/types"
import { EStatementAlignement, getAlignementType, getAlignementTypeText } from "../Global/domGenerator"
import { QAnalysisHistoric } from "./qAnalysisHistoric"
import { QAnalysisGenerator } from "./qAnalysisGenerator"
import { HistoricEvent } from "../Global/historic"
import { FileManager } from "../Global/fileManager"
import { QAnalysisWasmModule } from "./qAnalysisWasmModule"

interface interviewValue
{
	cardid: number;
	userid: number;
	text: string;
}

interface Contexte
{
	id: number;
	name: string;
	description: string;
	users: Array<number>;
}

/**
 *	Processor 
 */

export class QAnalysisProcessor {
	historic: QAnalysisHistoric;
	configuration: any;  //TO DO: probably need a spec
	studyConfiguration: any;  //TO DO: probably need a spec
	wasmModule: QAnalysisWasmModule;
	userCount: number;

	interview: any;
	qsort: any;


	constructor() {
		this.userCount = 0;
		this.wasmModule = new QAnalysisWasmModule();
		/*let importObject = {
			imports: {
				imported_func: function (arg) {
					console.log(arg);
				}
			}
		};
		fetch("js/QAnalysisAsm.wasm").then(response => response.arrayBuffer()).then(bytes => WebAssembly.instantiate(bytes, importObject));*/
		//var importObject = { imports: { imported_func: arg => console.log(arg) } };

		//WebAssembly.instantiateStreaming(fetch('js/a.out.wasm'), importObject);
		//.then(obj => obj.instance.exports.exported_func());
		/*WebAssembly.instantiateStreaming(fetch("js/simple.wasm"),
			{
				env: {
					abort(_msg, _file, line, column) {
						console.error("abort called at qAnalysisProcessor.ts:" + line + ":" + column);
					}
				},
			}).then(result => {
				this.processorModule = result.instance.exports;
			}).catch(console.error);*/
	}

	AutoSave() {
		//let id = setInterval(function (processor: QAnalysisProcessor) { return function () { if (!processor.SaveStudy(true)) { clearInterval(id); } }; }(this), 3000000); //every 5 minutes 
	}

	Undo(dom: QAnalysisGenerator) {/*
		if (!this.historic.currentHistoricalIndex)
			return;
		this.historic.currentHistoricalIndex--;
		let prevEvent: HistoricEvent;
		switch (this.historic.historicalEvents[this.historic.currentHistoricalIndex].what) {
			case 'create':
				switch (this.historic.historicalEvents[this.historic.currentHistoricalIndex].type) {
					case "statement":
						dom.DeleteCard(this.historic.historicalEvents[this.historic.currentHistoricalIndex].target.id);
						break;
					case "threeStates":
						this.threeStates = new Array<Tab>();
						dom.DeleteThreeStates();
						break;
					case "qsort":
						this.qSort = new Array<Tab>();
						dom.DeleteQSort();
						break;
					case "texts":
						this.texts = new Array<Text>();
						dom.DeleteWallOfTexts();
						break;
				}
				break
			case 'set':
				switch (this.historic.historicalEvents[this.historic.currentHistoricalIndex].type) {
					case "statement":
						prevEvent = this.historic.GetLastStatement(this.historic.historicalEvents[this.historic.currentHistoricalIndex].target.id);
						if (!prevEvent || prevEvent.what === "create")
							this.ResetStatementObject(this.historic.historicalEvents[this.historic.currentHistoricalIndex].target.id);
						else if (!prevEvent || prevEvent.what === "delete")
							return; // if happend then historic is corrupted
						else
							this.statements[this.historic.historicalEvents[this.historic.currentHistoricalIndex].target.id] = prevEvent.target;
						dom.ReloadCard(this.historic.historicalEvents[this.historic.currentHistoricalIndex].target.id);
						break;
					case "threeStates":
						prevEvent = this.historic.GetLastThreeStates();
						if (!prevEvent) {
							this.threeStates = new Array<Tab>();
							return dom.DeleteThreeStates();
						}
						this.threeStates = <Array<Tab>>prevEvent.target;
						dom.GenerateThreeStates(this.threeStates, dom.GetThreeStatesCheckFunctor(), dom.GetThreeStatesInsertToTitleCellFunctor(), dom.GetThreeStatesInsertFunctor());
						break;
					case "qsort":
						prevEvent = this.historic.GetLastQSort();
						if (!prevEvent) {
							this.qSort = new Array<Tab>();
							return dom.DeleteQSort();
						}
						this.qSort = <Tabs>prevEvent.target;
						dom.GenerateQSort(this.isCrescent, this.statAlign, this.qSort, this.statements.length, dom.GetQSortCheckFunctor(), dom.GetQSortInsertToTitleCellFunctor(), dom.GetQSortInsertFunctor());
						dom.SetQSortInputValue();
						break;
					case "isCrescent":
						prevEvent = this.historic.GetLastHeadPos();
						if (!prevEvent)
							this.isCrescent = false;
						else
							this.isCrescent = <boolean>prevEvent.target;
						dom.GenerateQSort(this.isCrescent, this.statAlign, this.qSort, this.statements.length, dom.GetQSortCheckFunctor(), dom.GetQSortInsertToTitleCellFunctor(), dom.GetQSortInsertFunctor());
						dom.SetRadioToValue("headerPosition", this.isCrescent ? 1 : 0);
						break;
					case "statAlign":
						prevEvent = this.historic.GetLastStatAlign();
						if (!prevEvent)
							this.statAlign = EStatementAlignement.Top;
						else
							this.statAlign = prevEvent.target;
						dom.GenerateQSort(this.isCrescent, this.statAlign, this.qSort, this.statements.length, dom.GetQSortCheckFunctor(), dom.GetQSortInsertToTitleCellFunctor(), dom.GetQSortInsertFunctor());
						dom.SetRadioToValue("alignementType", this.statAlign);
						break;
					case "interview":
						prevEvent = this.historic.GetLastInterview();
						if (prevEvent) {
							this.interviewType = prevEvent.target.type;
							this.interviewNumber = prevEvent.target.value;
						}
						else {
							this.interviewType = 0;
							this.interviewNumber = 0;
						}
						dom.SetInterview();
						break;
					case "exoquizz":
						prevEvent = this.historic.GetLastExoQuizz();
						if (!prevEvent) {
							this.exoQuizz = new Array<Exogen>();
							return dom.DeleteExoView();
						}
						this.exoQuizz = prevEvent.target;
						dom.GenerateExoView();
						break;
					case "texts":
						prevEvent = this.historic.GetLastWallOfText(this.historic.historicalEvents[this.historic.currentHistoricalIndex].target.id);
						if (!prevEvent)
							return dom.SetTextValue(this.historic.historicalEvents[this.historic.currentHistoricalIndex].target.id, "");
						dom.SetTextValue(prevEvent.target.id, prevEvent.target.value);
						break;
					case "activeInterview":
						prevEvent = this.historic.GetLastActiveInterview();
						if (prevEvent)
							this.isActive.interview = prevEvent.target;
						else
							this.isActive.interview = true;
						dom.ShowHideInterview();
						break;
					case "activeExogen":
						prevEvent = this.historic.GetLastActiveExogen();
						if (prevEvent)
							this.isActive.exogen = prevEvent.target;
						else
							this.isActive.exogen = true;
						dom.ShowHideExogen();
					case "threeStatesDeck":
						prevEvent = this.historic.GetLastIsDeck();
						if (prevEvent)
							this.isThreeStatesDeck = prevEvent.target;
						else
							this.isThreeStatesDeck = false;
						dom.UpdateDeckView(this.isThreeStatesDeck);
						dom.SetRadioToValue("threeStatesDeck", this.isThreeStatesDeck ? 1 : 0);
						break;
				}
				break;
			case 'delete':
				switch (this.historic.historicalEvents[this.historic.currentHistoricalIndex].type) {
					case "statement":
						this.statements[this.historic.historicalEvents[this.historic.currentHistoricalIndex].target.id] = this.historic.historicalEvents[this.historic.currentHistoricalIndex].target;
						dom.GenerateCard(this.historic.historicalEvents[this.historic.currentHistoricalIndex].target);
						break;
					case "threeStates ":
						this.threeStates = this.historic.historicalEvents[this.historic.currentHistoricalIndex].target;
						dom.GenerateThreeStates(this.threeStates, dom.GetThreeStatesCheckFunctor(), dom.GetThreeStatesInsertToTitleCellFunctor(), dom.GetThreeStatesInsertFunctor());
						break;
					case "qsort":
						this.qSort = this.historic.historicalEvents[this.historic.currentHistoricalIndex].target;
						dom.GenerateQSort(this.isCrescent, this.statAlign, this.qSort, this.statements.length, dom.GetQSortCheckFunctor(), dom.GetQSortInsertToTitleCellFunctor(), dom.GetQSortInsertFunctor());
						dom.SetQSortInputValue();
						break;
					case "exoquizz":
						this.exoQuizz = this.historic.historicalEvents[this.historic.currentHistoricalIndex].target;
						dom.GenerateExoView();
						break;
					case "texts":
						this.texts = this.historic.historicalEvents[this.historic.currentHistoricalIndex].target;
						dom.GenerateWallOfTexts();
						break;
				}
				break;
			case 'reset':
				switch (this.historic.historicalEvents[this.historic.currentHistoricalIndex].type) {
					case "statement":
						this.statements = this.historic.historicalEvents[this.historic.currentHistoricalIndex].target;
						dom.GenerateCards();
				}
				break;
		}*/
	}

	Redo(dom: QAnalysisGenerator) {/*
		if (this.historic.historicalEvents.length < this.historic.currentHistoricalIndex + 1)
			return;
		switch (this.historic.historicalEvents[this.historic.currentHistoricalIndex].what) {
			case 'create':
				switch (this.historic.historicalEvents[this.historic.currentHistoricalIndex].type) {
					case "statement":
						this.statements[this.historic.historicalEvents[this.historic.currentHistoricalIndex].target.id] = this.historic.historicalEvents[this.historic.currentHistoricalIndex].target;
						dom.GenerateCard(this.historic.historicalEvents[this.historic.currentHistoricalIndex].target);
						break;
					case "threeStates":
						this.threeStates = this.historic.historicalEvents[this.historic.currentHistoricalIndex].target;
						dom.GenerateThreeStates(this.threeStates, dom.GetThreeStatesCheckFunctor(), dom.GetThreeStatesInsertToTitleCellFunctor(), dom.GetThreeStatesInsertFunctor());
						break;
					case "qsort":
						this.qSort = <Tabs>this.historic.historicalEvents[this.historic.currentHistoricalIndex].target;
						dom.GenerateQSort(this.isCrescent, this.statAlign, this.qSort, this.statements.length, dom.GetQSortCheckFunctor(), dom.GetQSortInsertToTitleCellFunctor(), dom.GetQSortInsertFunctor());
						dom.SetQSortInputValue();
						break;
					case "texts":
						this.texts = this.historic.historicalEvents[this.historic.currentHistoricalIndex].target;
						dom.GenerateWallOfTexts();
						break;
				}
				break;
			case 'set':
				switch (this.historic.historicalEvents[this.historic.currentHistoricalIndex].type) {
					case "statement":
						this.statements[this.historic.historicalEvents[this.historic.currentHistoricalIndex].target.id] = this.historic.historicalEvents[this.historic.currentHistoricalIndex].target;
						dom.ReloadCard(this.historic.historicalEvents[this.historic.currentHistoricalIndex].target.id);
						break;
					case "threeStates":
						this.threeStates = this.historic.historicalEvents[this.historic.currentHistoricalIndex].target;
						dom.GenerateThreeStates(this.threeStates, dom.GetThreeStatesCheckFunctor(), dom.GetThreeStatesInsertToTitleCellFunctor(), dom.GetThreeStatesInsertFunctor());
						break;
					case "qsort":
						this.qSort = <Tabs>this.historic.historicalEvents[this.historic.currentHistoricalIndex].target;
						dom.GenerateQSort(this.isCrescent, this.statAlign, this.qSort, this.statements.length, dom.GetQSortCheckFunctor(), dom.GetQSortInsertToTitleCellFunctor(), dom.GetQSortInsertFunctor());
						dom.SetQSortInputValue();
						break;
					case "isCrescent":
						this.isCrescent = this.historic.historicalEvents[this.historic.currentHistoricalIndex].target;
						dom.GenerateQSort(this.isCrescent, this.statAlign, this.qSort, this.statements.length, dom.GetQSortCheckFunctor(), dom.GetQSortInsertToTitleCellFunctor(), dom.GetQSortInsertFunctor());
						dom.SetRadioToValue("headerPosition", this.isCrescent ? 1 : 0);
						break;
					case "statAlign":
						this.statAlign = this.historic.historicalEvents[this.historic.currentHistoricalIndex].target;
						dom.GenerateQSort(this.isCrescent, this.statAlign, this.qSort, this.statements.length, dom.GetQSortCheckFunctor(), dom.GetQSortInsertToTitleCellFunctor(), dom.GetQSortInsertFunctor());
						dom.SetRadioToValue("alignementType", this.statAlign);
						break;
					case "interview":
						this.interviewType = this.historic.historicalEvents[this.historic.currentHistoricalIndex].target.type;
						this.interviewNumber = this.historic.historicalEvents[this.historic.currentHistoricalIndex].target.value;
						this.SetInterviewState();
						break;
					case "exoQuizz":
						this.exoQuizz = this.historic.historicalEvents[this.historic.currentHistoricalIndex].target;
						dom.GenerateExoView();
						break;
					case "texts":
						dom.SetTextValue(this.historic.historicalEvents[this.historic.currentHistoricalIndex].target.id, this.historic.historicalEvents[this.historic.currentHistoricalIndex].target.value);
						break;
					case "activeInterview":
						this.isActive.interview = this.historic.historicalEvents[this.historic.currentHistoricalIndex].target;
						dom.ShowHideInterview();
						break;
					case "activeExogen":
						this.isActive.exogen = this.historic.historicalEvents[this.historic.currentHistoricalIndex].target;
						dom.ShowHideExogen();
						break;
					case "threeStatesDeck":
						this.isThreeStatesDeck = this.historic.historicalEvents[this.historic.currentHistoricalIndex].target;
						dom.UpdateDeckView(this.isThreeStatesDeck);
						dom.SetRadioToValue("threeStatesDeck", this.isThreeStatesDeck ? 1 : 0);
						break;
				}
				break;
			case 'delete':
				switch (this.historic.historicalEvents[this.historic.currentHistoricalIndex].type) {
					case "statement":
						dom.DeleteCard(this.historic.historicalEvents[this.historic.currentHistoricalIndex].target.id);
						break;
					case "threeStates ":
						this.threeStates = new Array<Tab>();
						dom.DeleteThreeStates();
						break;
					case "qsort":
						this.qSort = new Array<Tab>();
						dom.DeleteQSort();
						break;
					case "exoQuizz":
						this.exoQuizz = new Array<Exogen>();
						dom.DeleteExoView();
						break;
					case "texts":
						dom.DeleteWallOfTexts();
						break;
				}
				break;
			case 'reset':
				switch (this.historic.historicalEvents[this.historic.currentHistoricalIndex].type) {
					case "statement":
						this.statements = new Array<Statement>();
						dom.CleanNodeByName("pickingZone");
						break;
				}
				break;
			default:
				return;
		}
		this.historic.currentHistoricalIndex++;*/
	}

	AppendObjectsToList(trace, list)
	{
		for (let iterator = 0; iterator < trace.data.length; iterator++)
		{
			let tempObj = trace.data[iterator];
			if (!tempObj)
				return;
			tempObj.tracetype = trace.name;
			list.push(tempObj);
		}
	}

	ReconstructTimeLine(traces: Array<any>, invert: boolean, tracetype?: string)
	{
		let list = []
		if (tracetype)
		{
			for (let iterator of traces)
			{
				if (iterator.name === tracetype)
				{ 
					list = iterator.data;
					break;
				}
			}
		}
		else
			for (let iterator = 0; iterator < traces.length; iterator++)
				this.AppendObjectsToList(traces[iterator], list);
		if (invert)
			list.sort(function (a, b) { return b.t - a.t });
		else
			list.sort(function (a, b) { return a.t - b.t });
		return list;
	}

	ExtractFinalPositions(data): string {
		let list = this.ReconstructTimeLine(data.traces, true, "drag");
		let mapQSortInv: Map<string, string> = new Map();
		let mapThreeStatesInv: Map<string, string> = new Map();
		for (let item of list) {
			if (item.value === "start")
				continue;
			if (item.extra.substr(0, 4) === "tab_") {
				if (!mapQSortInv.has(item.id))
					mapQSortInv.set(item.id, item.extra);
			}
			else {
				if (!mapThreeStatesInv.has(item.id))
					mapThreeStatesInv.set(item.id, item.extra);
			}
		}

		let mapThreeStates: Map<string, Array<string>> = new Map();
		for (let [key, val] of mapThreeStatesInv)
		{
			if (mapThreeStates.has(val))
				mapThreeStates.get(val).push(key);
			else
				mapThreeStates.set(val, Array<string>(key));
		}
		data.threestatesresults.disagree = mapThreeStates.get("disagree");
		data.threestatesresults.neutral = mapThreeStates.get("neutral");
		data.threestatesresults.agree = mapThreeStates.get("agree");

		let mapQSort: Map<string, Array<string>> = new Map();
		for (let [key, val] of mapQSortInv)
		{
			if (mapQSort.has(val))
				mapQSort.get(val).push(key);
			else
				mapQSort.set(val, Array<string>(key));
		}
		for (let sort of data.qsortresult)
			sort.data = mapQSort.get(sort.id);

		return JSON.stringify(data);
	}

	ProcessConfig(dom: QAnalysisGenerator, data: string)
	{
		this.studyConfiguration = FileManager.JSONParse(data);
	}

	GetStatementPositionInQSort(id: string): string
	{
		let nId: number = Number.parseInt(id);
		for (let iterator in this.qsort)
			if (this.qsort[iterator].includes(nId))
				return iterator;
		return "";
	}

	ProcessGraphFile(file: File, dom: QAnalysisGenerator) {
		FileManager.ReadFile(file, function (dom: QAnalysisGenerator) { return function (text) { dom.graphGenerator.LoadGraphs(FileManager.JSONParse(text)); }; }(dom));
	}

	ProcessConfigFile(file: File, dom: QAnalysisGenerator) {
		FileManager.ReadFile(file, function (dom: QAnalysisGenerator) { return function (text) { dom.processor.ProcessConfig(dom, text); }; }(dom));
	}

	ProcessTraceFiles(files: FileList) {
		for (let iterator: number = 0; iterator < files.length; iterator++) {
			FileManager.ReadFile(files.item(iterator), function (processor: QAnalysisProcessor) { return function (text) { processor.AddFileToDatabase(processor.PrepareTraceFile(text)); }; }(this));
		}
	}

	RepairTraceFiles(files: FileList) {
		for (let iterator: number = 0; iterator < files.length; iterator++) {
			FileManager.ReadFile(files.item(iterator), function (processor: QAnalysisProcessor, filename: string) { return function (text) { FileManager.GenerateFile(filename, processor.ExtractFinalPositions(FileManager.JSONParse(text))); }; }(this, files.item(iterator).name));
		}
	}

	ProcessSQLFile(file: File, dom: QAnalysisGenerator) {
		FileManager.ReadFile(file, function (processor: QAnalysisProcessor) { return function (text) { processor.RestoreDataBase(text); }; }(this));
	}

	ProcessFileList(list: any, dom: QAnalysisGenerator) {
		/*if (typeof list === "object") {
			for (let iterator = 0; iterator < list.length; iterator++)
				this.ProcessFileList(list[iterator], dom);
		}
		else (typeof list === "string")
		{
			let fileType: string = list.name.substring(list.name.lastIndexOf(".") + 1).toLowerCase();
			switch (fileType) {
				case "sta":
				case "txt":
					let txtReader: FileReader = new FileReader();
					txtReader.onload = function (processor: QAnalysisProcessor) { return function (event: any) { processor.ProcessTXT(event.target.result); }; }(this);
					txtReader.readAsText(list);
					break;
				case "csv":
					let csvReader: FileReader = new FileReader();
					csvReader.onload = function (processor: QAnalysisProcessor) { return function (event: any) { processor.ProcessCSV(event.target.result, ";"); }; }(this);
					csvReader.readAsText(list);
					break;
				case "png":
				case "bmp":
				case "jpg":
				case "jpeg":
					let imgReader: FileReader = new FileReader();
					imgReader.onload = function (dom: QAnalysisGenerator, file: File, id: number) { return function (event: any) { dom.processor.mediacontents[dom.processor.GenerateMediaFullPath(file)] = event.target.result; dom.processor.SetImageStatement(id, file); dom.ReloadCard(id); }; }(dom, list, this.globalCounter);
					dom.GenerateEmptyCard(this.globalCounter);
					imgReader.readAsDataURL(list);
					break;
				case "mp3":
					let audioReader: FileReader = new FileReader();
					audioReader.onload = function (dom: QAnalysisGenerator, file: File, id: number) { return function (event: any) { dom.processor.mediacontents[dom.processor.GenerateMediaFullPath(file)] = event.target.result; dom.processor.SetAudioStatement(id, file, 0); dom.ReloadCard(id) }; }(dom, list, this.globalCounter);
					dom.GenerateEmptyCard(this.globalCounter);
					audioReader.readAsDataURL(list);
					break;
				case "mov":
				case "mp4":
					let videoReader: FileReader = new FileReader();
					videoReader.onload = function (dom: QAnalysisGenerator, file: File, id: number) { return function (event: any) { dom.processor.mediacontents[dom.processor.GenerateMediaFullPath(file)] = event.target.result; dom.processor.SetVideoStatement(id, file); dom.ReloadCard(id) }; }(dom, list, this.globalCounter);
					dom.GenerateEmptyCard(this.globalCounter);
					videoReader.readAsDataURL(list);
					break;
			}
		}*/
	}

	ProcessImageFile(dom: QAnalysisGenerator, file: File, id: number) {/*
		let reader: FileReader = new FileReader();
		if (this.statements[id].type === "img")
			reader.onload = function (dom: QAnalysisGenerator, node: number, file: File) { return function (event: any) { dom.processor.mediacontents[dom.processor.GenerateMediaFullPath(file)] = event.target.result; dom.processor.SetImageStatement(node, file); dom.ReloadCard(node); }; }(dom, id, file);
		else
			reader.onload = function (dom: QAnalysisGenerator, node: number, file: File) { return function (event: any) { dom.processor.mediacontents[dom.processor.GenerateMediaFullPath(file)] = event.target.result; dom.processor.SetIllustration(node, file); dom.ReloadCard(node); }; }(dom, id, file);
		reader.readAsDataURL(file);*/
	}

	ProcessAudioFile(dom: QAnalysisGenerator, file: File, id: number) {/*
		let reader: FileReader = new FileReader();
		reader.onload = function (dom: QAnalysisGenerator, node: number, file: File) { return function (event: any) { dom.processor.mediacontents[dom.processor.GenerateMediaFullPath(file)] = event.target.result; dom.processor.SetAudioStatement(node, file, 0); dom.ReloadCard(node); }; }(dom, id, file);
		reader.readAsDataURL(file);*/
	}

	ProcessVideoFile(dom: QAnalysisGenerator, file: File, id: number) {/*
		let reader: FileReader = new FileReader();
		reader.onload = function (dom: QAnalysisGenerator, node: number, file: File) { return function (event: any) { dom.processor.mediacontents[dom.processor.GenerateMediaFullPath(file)] = event.target.result; dom.processor.SetVideoStatement(node, file); dom.ReloadCard(node); }; }(dom, id, file);
		reader.readAsDataURL(file);*/
	}

	ProcessExotable(table, joker) {/*
		this.exoQuizz = new Array<Exogen>();
		for (let row = 0; row < table.rows.length; row++) {
			if (row === Number.parseInt(joker))
				continue;
			let object: Exogen;
			for (let element in table.rows[row].cells[0].childNodes)
				if (table.rows[row].cells[0].childNodes[element].id)
					object[table.rows[row].cells[0].childNodes[element].id] = table.rows[row].cells[0].childNodes[element].childNodes[1].value;
			for (let element in table.rows[row].cells[1].childNodes) {
				if (table.rows[row].cells[1].childNodes[element].id) {
					let value: ParameterValue;
					switch (table.rows[row].cells[1].childNodes[element].id) {
						case "values":
							for (let iterator = 0; iterator < table.rows[row].cells[1].childNodes[element].childNodes.length; iterator++) {
								if (!object.values)
									object.values = [];
								object.values.push(table.rows[row].cells[1].childNodes[element].childNodes[iterator].value);
							}
							break;
						case "isRequired":
							value.name = table.rows[row].cells[1].childNodes[element].id;
							value.value = table.rows[row].cells[1].childNodes[element].childNodes[1].checked;
							object.parameters.add(value);
							break;
						case "min":
						case "max":
						case "step":
						case "max-length":
							value.name = table.rows[row].cells[1].childNodes[element].id;
							value.value = table.rows[row].cells[1].childNodes[element].childNodes[1].value;
							object.parameters.add(value);
							break;
						default:
							for (let iterator = 0; iterator < table.rows[row].cells[1].childNodes[element].childNodes.length; iterator++) {
								value.name = table.rows[row].cells[1].childNodes[element].childNodes[iterator].childNodes[1].value;
								value.value = table.rows[row].cells[1].childNodes[element].childNodes[iterator].childNodes[3].value;
								object.parameters.add(value);
							}
							break;
					}
				}
			}
			this.exoQuizz.push(object);
		}*/
	}

	GenerateStatementsFile() {/*
		if (!this.statements.length)
			return false;
		let staFile: string;
		for (let key in this.statements) {
			staFile += key + "." + this.statements[key].value
			switch (this.statements[key].type) {
				case "audio":
					if ('illustration' in this.statements[key]) {
						staFile += " - " + this.statements[key].illustration;
						break;
					}
				case "img":
					if ('descirption' in this.statements[key])
						staFile += " - " + this.statements[key].description;
					break;
			}
			staFile += "\n";
		}
		FileManager.GenerateFile("statements.sta", staFile);
		return true;*/
	}

	GenerateConfig(autoSave?: boolean) {/*
		let sum: number = 0;
		let qSortLength: number = 0;
		for (let sort in this.qSort) {
			sum += this.qSort[sort].limit;
			qSortLength++;
		}
		if (!qSortLength) {
			if (!autoSave)
				alert(this.configuration.needQsort);
			return null;
		}
		if (sum && this.statements.length !== sum) {
			if (!autoSave)
				alert(this.configuration.needEqualStatementsNumber)
			return null;
		}
		if (!this.texts.length) {
			if (!autoSave)
				alert(this.configuration.needTexts)
			return null;
		}
		let json: string = '{ "serverUrlSubmit":"submit.php","opacityAnimationInterval": 100,"opacityAnimationDuration": 1000,"scalingAnimationInterval": 10,"scalingAnimationDuration": 500,"scalingAnimationMaxValue": 3,"scalingAnimationMinValue": 0.65,"qSortConfiguration":{"qSort":{';
		sum = 0;
		for (let key in this.qSort) {
			json += '"' + key + '":' + JSON.stringify(this.qSort[key]);
			if (sum < qSortLength - 1)
				json += ",";
			sum++;
		}
		json += '},"statementsAlignement":' + getAlignementTypeText(this.statAlign);
		json += ',"isCrescent":"';
		if (this.isCrescent)
			json += 'true"';
		else
			json += 'false"';
		json += '},"statements":[';
		for (let iterator in this.statements) {
			json += JSON.stringify(this.statements[iterator]);
			if (Number.parseInt(iterator) < this.statements.length - 1)
				json += ',';
		}
		json += '],';
		if (this.threeStates.length) {
			json += '"isThreeStatesDeck":' + this.isThreeStatesDeck;
			json += ',"threeStates":[';
			for (let iterator in this.threeStates) {
				json += '{ "type": "' + this.threeStates[iterator].type + '", "title":"' + this.threeStates[iterator].title + '"}';
				if (Number.parseInt(iterator) < this.threeStates.length - 1)
					json += ",";
			}
			json += ']';
		}
		for (let text of this.texts)
			json += ',"' + text.id + '":"' + text.value.replace(/\n/g, "<br>").replace(/\r/g, "") + '"';
		if (this.isActive.interview) {
			json += ',"isInterviewActive":"true"';
			json += ',"dynamicType":' + this.interviewType;
			json += ',"dynamicStatementNumber":' + this.interviewNumber;
		}
		else
			json += ',"isInterviewActive":"false"';
		let maxitem = this.exoQuizz.length - 1;
		if (this.isActive.exogen && maxitem !== -1) {
			json += ',"isExogenActive":"true"';
			json += ',"exoQuizz":[';
			let iterator = 0;
			for (let exo of this.exoQuizz) {
				iterator++;
				json += '{ "id":"' + exo.id + '","type":"' + exo.type + '", "text":"' + exo.text + '"';
				if (exo.values && exo.values.length)
					json += ',"values":' + JSON.stringify(exo.values);
				json += ',"parameters":{';
				sum = 0;
				for (let parameter of exo.parameters) {
					json += '"' + parameter.name + '":"' + parameter.value + '"';
					if (sum < exo.parameters.size - 1)
						json += ",";
					sum++;
				}
				json += "}}";
				if (iterator <= maxitem)
					json += ",";
			}
			json += "]";
		}
		else
			json += ',"isExogenActive":"false"';
		json += "}";
		return json;*/
	}

	PrepareTraceFile(stream: string): string {
		let traces: any = FileManager.JSONParse(stream);
		if (!traces.traces)
			return stream;
		for (let iterator: number = traces.traces.length - 1; iterator >= 0; iterator--) {
			switch (traces.traces[iterator].name) {
				case "mouseclick":
				case "range":
				case "error":
				case "focus":
				case "change":
				case "keypress":
				case "mousemove":
				case "scrolling":
				case "zooming":
					traces.traces.splice(iterator, 1);
			}
		}
		return JSON.stringify(traces);
	}

	SaveConfig(autoSave) {
		/*let json: string = this.GenerateConfig(autoSave)
		if (!json)
			return false;
		return this.historic.SaveInStorage(json);*/
	}

	LoadConfig(dom: QAnalysisGenerator) {
		let olderData: string = this.historic.GetSavedData();
		if (!olderData || olderData === "undefined") {
			alert(this.configuration.noDataSaved);
			return;
		}
		this.ProcessConfig(dom, olderData);
	}

	AddFileToDatabase(stream: string) {
		this.wasmModule.AddFileToDatabase(stream);
	}

	RestoreDataBase(stream: string) {
		this.wasmModule.RestoreDatabase(stream);
	}

	SaveDataBase(name: string) {
		let data: string = this.wasmModule.DumpDatabase();
		if (data.length)
			FileManager.GenerateFile(name, data);
	}

	GetPQMethodResult(name: string) {
		let data: string = this.wasmModule.GetPQMethodResult();
		if (data.length)
			FileManager.GenerateFile(name, data);
	}

	GetInterviews(name: string) {
		let data: string = this.wasmModule.GetInterviews();
		if (data.length)
			FileManager.GenerateFile(name, data);
	}

	GetExogenData(name: string) {
		let data: string = this.wasmModule.GetExogenData();
		if (data.length)
			FileManager.GenerateFile(name, data);
	}

	GetGraphsData(name: string) {
		let data: string = this.wasmModule.GetGraphsData();
		if (data.length)
			FileManager.GenerateFile(name, data);
	}

	GetExogenStats(name: string) {
		let data: string = this.wasmModule.GetExogenStats();
		if (data.length)
			FileManager.GenerateFile(name, data);
	}

	GetSetupStats(name: string) {
		let data: string = this.wasmModule.GetSetupStats();
		if (data.length)
			FileManager.GenerateFile(name, data);
	}

	TestMethod()
	{
		this.userCount = this.wasmModule.GetUserCount();
		let data: string = this.wasmModule.GetInterviewsForUsers([1]);
		//let data2: string = this.wasmModule.GetExogenDataForUsers(ids);
		let data3: string = this.wasmModule.GetQSortForUser(1);
		this.interview = FileManager.JSONParse(data);
		//let data2F: string = FileManager.JSONParse(data2);
		this.qsort = FileManager.JSONParse(data3);
	}
};