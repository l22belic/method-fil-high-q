import { QInterfaceStorage } from "./qInterfaceStorage";
import { QInterfaceProcessor } from "./qInterfaceProcessor";
import { QInterfaceGenerator } from "./qInterfaceGenerator";
import { FileManager } from "../Global/fileManager";

/**
* Server Mode
*/
const isServer = false;
// If true : try to load automatically the file config.json in the root folder.
// If false : genrerate a drop window to select manually the file.
const isCompression = true;

/**
 *  Variables
 */

const text = "Configuration file (json)";
export const error = "Can't load configuration file";

/*
 * Function calls
 */

export default function start() {
	FileManager.isCompression = isCompression;
	let hmi: QInterfaceGenerator = new QInterfaceGenerator();
	hmi.processor = new QInterfaceProcessor();
	hmi.processor.storage = new QInterfaceStorage(true);
	document.body.onmousemove = function (storage: QInterfaceStorage) {
		return function (event: MouseEvent) { storage.StoreMousePositionData(event); };
	}(hmi.processor.storage);
	document.body.onclick = function (manager: QInterfaceGenerator) {
		return function (event: MouseEvent) { manager.UnscaleElement(); manager.processor.storage.StoreMouseClickData(event); };
	}(hmi);
	document.body.onscroll = function (storage: QInterfaceStorage) {
		return function (event: Event) { storage.StoreScrollingData(event); };
	}(hmi.processor.storage);
	document.body.oncontextmenu = function (manager: QInterfaceGenerator) {
		return function (event: MouseEvent) { manager.UnscaleElement(); event.preventDefault();};
	}(hmi);
	if (isServer)
	{
		hmi.processor.LoadConfig(function (manager: QInterfaceGenerator) {
			return function () { manager.GenerateStartPage(); };
		}(hmi));
	}
	else
		hmi.GenerateDropZone("id", text, "dropZone", hmi.GetMain(), "json", false,
			function (manager: QInterfaceGenerator)
			{
				return function (event: any)
				{
					if (!event.length)
						return;
					FileManager.ReadFile(event[0], function (text: string)
					{
						try {
							let configuration = FileManager.JSONParse(text);
							if (!configuration.serverUrlSubmit) {
								alert(error);
								return false;
							}
							manager.processor.configuration = configuration;
							manager.processor.storage.isActive = configuration.isTrace;
							manager.processor.LoadStatement();
							manager.GenerateStartPage();
							return true;
						}
						catch (e)
						{
							console.log(e);
							alert(error);
							return false;
						}
					});
				};
			}(hmi));
}