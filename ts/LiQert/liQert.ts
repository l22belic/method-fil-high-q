/*
*   Copyright 2019 Aur�lien Milliat <aurelien.milliat@gmail.com>
*   Licensed under the Apache License, Version 2.0 (the "License");
*   you may not use this file except in compliance with the License.
*   You may obtain a copy of the License at
*
*       http://www.apache.org/licenses/LICENSE-2.0
*
*  Unless required by applicable law or agreed to in writing, software
*  distributed under the License is distributed on an "AS IS" BASIS,
*  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
*  See the License for the specific language governing permissions and
*  limitations under the License.
*
*/

import { LiQertStorage } from "./liQertStorage";
import { LiQertProcessor } from "./liQertProcessor";
import { LiQertGenerator } from "./liQertGenerator";
import { FileManager } from "../Global/fileManager";

/**
* Server Mode
*/
const isServer = true;
// If true : try to load automatically the file config.json in the root folder.
// If false : genrerate a drop window to select manually the file.
const isCompression = true;

/**
 *  Variables
 */

const text = "Configuration file (json)";
export const error = "Can't load configuration file";

/*
 * Function calls
 */

export default function start() {
	FileManager.isCompression = isCompression;
	let hmi: LiQertGenerator = new LiQertGenerator();
	hmi.processor = new LiQertProcessor();
	hmi.processor.storage = new LiQertStorage(true);
	document.body.onmousemove = function (storage: LiQertStorage) {
		return function (event: MouseEvent) { storage.StoreMousePositionData(event); };
	}(hmi.processor.storage);
	document.body.onclick = function (manager: LiQertGenerator) {
		return function (event: MouseEvent) { manager.UnscaleElement(); manager.processor.storage.StoreMouseClickData(event); };
	}(hmi);
	document.body.onscroll = function (storage: LiQertStorage) {
		return function (event: Event) { storage.StoreScrollingData(event); };
	}(hmi.processor.storage);
	document.body.oncontextmenu = function (manager: LiQertGenerator) {
		return function (event: MouseEvent) { manager.UnscaleElement(); event.preventDefault(); };
	}(hmi);
	if (isServer) {
		hmi.processor.LoadConfig(function (manager: LiQertGenerator) {
			return function () { manager.GenerateStartPage(); };
		}(hmi));
	}
	else
		hmi.GenerateDropZone("id", text, "dropZone", hmi.GetMain(), "json", false,
			function (manager: LiQertGenerator) {
				return function (event: any) {
					if (!event.length)
						return;
					FileManager.ReadFile(event[0], function (text: string) {
						try {
							let configuration = FileManager.JSONParse(text);
							if (!configuration.serverUrlSubmit) {
								alert(error);
								return false;
							}
							manager.processor.configuration = configuration;
							manager.processor.storage.isActive = configuration.isTrace;
							manager.processor.LoadLikerts();
							manager.GenerateStartPage();
							return true;
						}
						catch (e) {
							console.log(e);
							alert(error);
							return false;
						}
					});
				};
			}(hmi));
}