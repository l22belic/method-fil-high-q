/*
*   Copyright 2019 Aur?lien Milliat <aurelien.milliat@gmail.com>
*   Licensed under the Apache License, Version 2.0 (the "License");
*   you may not use this file except in compliance with the License.
*   You may obtain a copy of the License at
*
*       http://www.apache.org/licenses/LICENSE-2.0
*
*  Unless required by applicable law or agreed to in writing, software
*  distributed under the License is distributed on an "AS IS" BASIS,
*  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
*  See the License for the specific language governing permissions and
*  limitations under the License.
*
*/

import { LiQertGenerator } from "./liQertGenerator"
import { LiQertStorage, Trace } from "./liQertStorage"
import { FileManager } from "../Global/fileManager"
import { Sort, Statement } from "../Global/types"
import { error } from "./liQert"

/**
 * Shuffle
 * 
 * http://jsfromhell.com/array/shuffle [v1.0]
 */

function ShuffleList(list: Array<any>): Array<any> {
	for (let j, x, i = list.length; i; j = Math.floor(Math.random() * i), x = list[--i], list[i] = list[j], list[j] = x) { };
	return list;
}

/**
 * Basic interface & type
 */

interface IPair_N_S {
	key: number;
	value: string;
}

type ArrayIfPair_N_S = Array<IPair_N_S>;

/**
 * LiQertProcessor
 */

export class LiQertProcessor {
	storage: LiQertStorage;
	configuration: any; //TO DO: probably need a spec
	statements: Array<Statement>;
	randomLikerts: Array<number>;
	randomStatements: Array<number>;
	cardsMovements: Map<string, number>;
	cardsDistances: Map<string, number>;
	state: string;
	currentLikertId: number;
	currentLikertSize: number;
	cardsForInterview: Sort;

	LoadLikerts(): boolean {
		/* For extended version ??
		if (this.configuration.statementsFile && this.configuration.statementsType)
		{
			if (this.configuration.statementsType === "xml")
				FileManager.LoadFile(this.configuration.statementsFile, function (processor: LiQertProcessor) { return function parser(text: string) { try { this.statements = FileManager.XMLParse(text); return true; } catch (e) { console.log(e); return false; } }; }(this), function (processor: LiQertProcessor) { return function () { return processor.GenerateStatementsList(); }; }(this));
			else if (this.configuration.statementsType === "json")
				FileManager.LoadFile(this.configuration.statementsFile, function (processor: LiQertProcessor) { return function parser(text: string) { try { this.statements = FileManager.JSONParse(text); return true; } catch (e) { console.log(e); return false; } }; }(this), function (processor: LiQertProcessor) { return function () { return processor.GenerateStatementsList(); }; }(this));
			else if (this.configuration.statementsType === "csv")
				FileManager.LoadFile(this.configuration.statementsFile, function (processor: LiQertProcessor) { return function parser(text: string) { try { this.statements = FileManager.CSVParse(text); return true; } catch (e) { console.log(e); return false; } }; }(this), function (processor: LiQertProcessor) { return function () { return processor.GenerateStatementsList(); }; }(this));
			else
				alert(this.configuration.wrongstatementformatmessage);

		}
		else*/ if (this.configuration.likerts) {
			this.CleanUp();
			this.GenerateLikertList();
			this.storage.PrapareLikerts(this.configuration.likerts.length);
			this.currentLikertId = 0;
			this.state = "";
			return true;
		}
		return false;
	}

	LoadStatements(likertId: number): boolean	{
		if (this.configuration.likerts) {
			this.CleanForNextLikert();
			this.statements = this.configuration.likerts[likertId].statements;
			this.GenerateStatementsList();
			return true;
		}
		return false;
	}

	LoadConfig(functor: Function) {
		FileManager.LoadFile('configLikert.json', function (processor: LiQertProcessor) { return function (text: string) { try { processor.configuration = FileManager.JSONParse(text); return true; } catch (e) { alert(error); console.log(e); return false; } }; }(this), function (processor: LiQertProcessor) { return function () { if (processor.LoadLikerts()) return functor(); alert(error) }; }(this));
	}

	CleanUp() {
		this.state = "";
		this.storage.CleanUp();
		this.randomStatements = new Array<number>();
		this.randomLikerts = new Array<number>();
		this.cardsMovements = new Map<string, number>();
		this.cardsDistances = new Map<string, number>();
		this.cardsForInterview = { id: "dynamics", value: new Set<string>() };
	}

	CleanForNextLikert() {
		this.state = "presentation";
		this.randomStatements = new Array<number>();
		this.cardsMovements = new Map<string, number>();
		this.cardsDistances = new Map<string, number>();
		this.cardsForInterview = { id: "dynamics", value: new Set<string>() };
	}

	GenerateLikertList() {
		for (let key: number = 0; key < this.configuration.likerts.length; key++) 
			this.randomLikerts.push(key);
		if (this.configuration.randomLikert)
			ShuffleList(this.randomLikerts);
		this.currentLikertSize = 0;
		this.LoadStatements(this.randomLikerts[this.currentLikertSize]);
	}

	GenerateStatementsList() {
		for (let key: number = 0; key < this.statements.length; key++) {
			this.randomStatements.push(key);
			this.cardsMovements[this.statements[key].id] = 0;
			this.cardsDistances[this.statements[key].id] = 0;
		}
		ShuffleList(this.randomStatements);
	}

	GetStatement(id): Statement {
		for (let iterator: number = 0; iterator < this.statements.length; iterator++)
			if (this.statements[iterator].id === id)
				return this.statements[iterator];
		return null;
	}

	NextLikert(): boolean {
		if (this.currentLikertSize + 1 >= this.randomLikerts.length)
			return false;
		this.currentLikertSize++;
		this.currentLikertId = this.randomLikerts[this.currentLikertSize];
		this.LoadStatements(this.randomLikerts[this.currentLikertId]);
		return true;
    }

	GenerateJSON(): string {
		let json: string = '{"setup":{ "window":{"x":' + window.innerWidth + ',"y":' + window.innerHeight + '},"userAgent":\"' + navigator.userAgent + '\","vendor":\"' + navigator.vendor + '\"},';
		json += '"likertSequence": [';
		for (let key in this.randomLikerts) {
			json += this.randomLikerts[key];
			if (Number.parseInt(key) < this.randomLikerts.length - 1)
				json += ',';
		}
		json += '],';
		json += this.storage.GenerateQSortState() + ',';
		json += '"postdata":[';
		json += this.storage.GetJSONFromStore("interview");
		json += "," + this.storage.GetJSONFromStore("exogen");
		json += ']'
		if (this.configuration.saveTrace)
			json += ',' + this.storage.GenerateTrace();
		json += '}';
		return json;
	}

	StoreQSortValues(dom: LiQertGenerator): boolean {
		let tab = [];
		for (let key in this.configuration.likerts[this.currentLikertId].liQert)
			tab[tab.length] = Number.parseInt(key);
		tab.sort(function (a, b) { return b - a; });
		for (let iterator in tab) {
			let object: Sort = { id: "", value: new Set<string>() };
			object.id = "tab_" + ([tab[iterator]]).toString();
			object.value = dom.GetChildsIds(document.getElementById("tab_" + (tab[iterator]).toString()), null, 'tab_');
			if (object.value.size === 1 && object.value.has("tab_"))
				object.value = new Set<string>();
			this.storage.liQert[this.currentLikertId].push(object);
		}
		return true;
	}

	StoreTextInterview(dom: LiQertGenerator) {
		if (!this.storage.isActive)
			return;
		let list = document.getElementsByTagName("textarea");
		for (let iterator = 0; iterator < list.length; iterator++) {
			let object: Trace = { likert: this.currentLikertId, id: list[iterator].name, value: list[iterator].value, extra: dom.GetParentWithId(list[iterator]).id };
			this.storage.AppendToStorage("interview", JSON.stringify(object));
		}
	}

	StoreExoResult() {
		if (!this.storage.isActive)
			return true;
		let list: HTMLCollectionOf<HTMLInputElement> = document.getElementsByTagName("input");
		for (let iterator = 0; iterator < list.length; iterator++) {
			let object: Trace = { id: "", value: null };
			switch (list[iterator].type) {
				case "range":
				case "text":
				case "number":
					object.id = list[iterator].id;
					object.value = list[iterator].value;
					break;
				case "radio":
					object.id = list[iterator].name;
					let firstRound: boolean = true;
					while (iterator < list.length && list[iterator].type === "radio" && (firstRound || !iterator || list[iterator].parentNode === list[iterator - 1].parentNode)) {
						firstRound = false;
						if (list[iterator].checked)
							object.value = list[iterator].value;
						iterator++;
					}
					iterator--;
					break;
				case "checkbox":
					object.id = list[iterator].name;
					object.value = new Array<string>();
					while (iterator < list.length && list[iterator].type === "checkbox") {
						if (list[iterator].checked)
							object.value.push(list[iterator].value);
						iterator++;
					}
					iterator--;
					break;
				default:
				case "button":
					continue;
			}
			if (list[iterator]["required"] && (!object.value || object.value === "" || object.value.length == 0)) {
				return false;
			}
			if ("id" in object)
				this.storage.AppendToStorage("exogen", JSON.stringify(object));
		}
		return true;
	}

	StoreNewOrder(element: string, dom: LiQertGenerator) {
		let orderedList: Set<string>;
		orderedList = dom.GetChildsIds(document.getElementById(element), null, 'tab_');
		this.storage.StoreEvent("drop", "elementslist", element, Array.from(orderedList));
	}

	StoreNextStepEvent() {
		switch (this.state)
		{
			case "presentation":
				this.state = "likert_presentation";
				break
			case "likert_presentation":
				this.state = "likert";
				break
			case "likert":
				this.state = "likert_validation";
				break
			case "likert_validation":
				this.state = "likert_interview_presentation";
				break
			case "likert_interview_presentation":
				this.state = "likert_interview";
				break
			case "likert_interview":
				this.state = "exogen";
				break
			case "exogen":
				this.state = "end";
				break
			case "end":
				this.state = "end";
				break
			default:
				this.state = "presentation";
				break
		}
		this.storage.StoreNextStepEvent(this.state, "check");
	}

	GetStatementForInterview() {
		let tabMouv: ArrayIfPair_N_S = new Array<IPair_N_S>();
		let tabDist: ArrayIfPair_N_S = new Array<IPair_N_S>();
		let mask: number = 0;
		if (this.configuration.isDynamicMovements) {
			for (let iterator in this.cardsMovements)
				tabMouv.push({ key: this.cardsMovements[iterator], value: iterator });
			tabMouv.sort(function (a: IPair_N_S, b: IPair_N_S) { return b.key - a.key; });
			mask += 1;
		}
		if (this.configuration.isDynamicDistance) {
			for (let iterator in this.cardsDistances)
				tabDist.push({ key: this.cardsDistances[iterator], value: iterator });
			tabDist.sort(function (a: IPair_N_S, b: IPair_N_S) { return b.key - a.key; });
			mask += 2;
		}
		let iteratorMouv: number = 0, iteratorDist: number = 0;
		switch (mask) {
			case 1:
				while (this.cardsForInterview.value.size < this.configuration.dynamicStatementNumber && iteratorMouv < tabMouv.length && tabMouv[iteratorMouv].key)
					this.cardsForInterview.value.add(tabMouv[iteratorMouv++].value);
				break;
			case 2:
				while (this.cardsForInterview.value.size < this.configuration.dynamicStatementNumber && iteratorDist < tabDist.length && tabDist[iteratorDist].key)
					this.cardsForInterview.value.add(tabDist[iteratorDist++].value);
				break;
			case 3:
				while (this.cardsForInterview.value.size < this.configuration.dynamicStatementNumber) {
					if ((tabDist.length <= iteratorDist && tabMouv.length <= iteratorMouv) || (!tabMouv[iteratorMouv].key && !tabDist[iteratorDist].key))
						break;
					if (tabDist.length > iteratorDist && tabMouv.length <= iteratorMouv) {
						if (!tabDist[iteratorDist].key)
							break;
						this.cardsForInterview.value.add(tabDist[iteratorDist++].value);
					}
					else if (tabDist.length <= iteratorDist && tabMouv.length > iteratorMouv) {
						if (!tabMouv[iteratorMouv].key)
							break;
						this.cardsForInterview.value.add(tabMouv[iteratorMouv++].value);
					}
					else if (tabDist[iteratorDist].key < tabMouv[iteratorMouv].key)
						this.cardsForInterview.value.add(tabMouv[iteratorMouv++].value);
					else
						this.cardsForInterview.value.add(tabDist[iteratorDist++].value);
				}
				break;
		}
	}
}
