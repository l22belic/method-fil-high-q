/*
*   Copyright 2019 Aur�lien Milliat <aurelien.milliat@gmail.com>
*   Licensed under the Apache License, Version 2.0 (the "License");
*   you may not use this file except in compliance with the License.
*   You may obtain a copy of the License at
*
*       http://www.apache.org/licenses/LICENSE-2.0
*
*  Unless required by applicable law or agreed to in writing, software
*  distributed under the License is distributed on an "AS IS" BASIS,
*  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
*  See the License for the specific language governing permissions and
*  limitations under the License.
*
*/

import { ThreeStates, Sort, QSort } from "../Global/types"

/**
 * Timming in millisec 
 */

var referenceTime = Date.now();

export function ResetReferenceTime(offset?: number) {
    referenceTime = Date.now();
    if (offset)
        referenceTime -= offset;
}

function GetMillisecSinceRefTime(): number {
    let time = Date.now() - referenceTime;
    //If here something goes wrong, reinit the timer.
    if (time < 0) {
        referenceTime = Date.now();
        return 0;
    }
    return time;
}

/**
 * Basic interface & type
 */

interface ITrace {
    likert?: number;
    id?: any;
    t?: number;
    value?: any;
    extra?: any;
}

export type Trace = ITrace;

/**
 * Storage
 */

export class LiQertStorage {
    threeStatesResults: ThreeStates;
    liQert: Array<QSort>;
    liQertStatements: Array<Array<number>>;
    isActive: boolean;

    constructor(active: boolean) {
        this.isActive = active;
    }

    PrapareLikerts(size: number) {
        this.liQert = new Array<QSort>(size);
        for (let iterator: number = 0; iterator < size; iterator++)
            this.liQert[iterator] = new Array<Sort>();
        this.liQertStatements = new Array<Array<number>>(size);
        for (let iterator: number = 0; iterator < size; iterator++)
            this.liQertStatements[iterator] = new Array<number>();
    }

    CleanUp() {
        this.threeStatesResults = { agree: new Set<string>(), neutral: new Set<string>(), disagree: new Set<string>() };
        this.liQert = new Array<QSort>();
        this.ClearStorage();
    }

    ClearStorage() {
        window.sessionStorage.clear();
    }

    CleanStorage(name: string) {
        window.sessionStorage.removeItem(name);
    }

    AppendToStorage(name: string, data: string) {
        let old: string = window.sessionStorage.getItem(name);
        if (old === null || old === "")
            window.sessionStorage.setItem(name, data);
        else
            window.sessionStorage.setItem(name, old + "," + data);
    }

    StoreItem(name: string, object: Trace) {
        if (!this.isActive)
            return;
        object.t = GetMillisecSinceRefTime();
        this.AppendToStorage(name, JSON.stringify(object));
    }

    GetJSONFromStore(name: string): string {
        let json: string = '{"name":"' + name + '","data":[';
        json += window.sessionStorage.getItem(name);
        json += ']}';
        return json;
    }

    CleanStorageFormTraces() {
        this.CleanStorage("steps");
        this.CleanStorage("interview");
        this.CleanStorage("exogen");
        this.CleanStorage("focus");
        this.CleanStorage("change");
        this.CleanStorage("range");
        this.CleanStorage("keypress");
        this.CleanStorage("mousemove");
        this.CleanStorage("mouseclick");
        this.CleanStorage("scrolling");
        this.CleanStorage("zooming");
        this.CleanStorage("media");
        this.CleanStorage("drag");
        this.CleanStorage("drop");
        this.CleanStorage("errors");
        this.CleanStorage("draggablecontainer");
    }

    StoreDraggableEvent(id: string, cardid: string) {
        if (!this.isActive)
            return;
        this.StoreItem('draggablecontainer', { id: id, value: cardid });
    }

    StoreNextStepEvent(step: string, extra: string) {
        if (!this.isActive)
            return;
        let object: Trace = { id: step };
        if (extra)
            object.extra = extra;
        this.StoreItem('steps', object);
    }
    /*StoreOnChangeRadioEvent(event: Event)
    {
        if(isReplay)
            return;
        let object={};
        object.id=event.currentTarget.parentNode.id;
        object.ty="r";
        if(event.currentTarget.checked)
            object.v=event.currentTarget.value;
        this.StoreItem('change',object);
    }

    StoreOnChangeCheckboxEvent(event: Event)
    {
        if(isReplay)
            return;
        let object={};
        object.id=event.currentTarget.parentNode.id;
        object.ty="cb";
        object.v=event.currentTarget.value;
        object.ic=event.currentTarget.checked;
        this.StoreItem('change',object);
    }

    StoreFocusEvent(event: Event)
    {
        if(isReplay)
            return;
        let object={};
        object.id=event.currentTarget.id;
        this.StoreItem('focus',object);
    }

     StoreRangeEvent(event)
     {
        if(isReplay)
            return;
        let object={};
        object.id=event.currentTarget.id;
        object.v=event.currentTarget.value;
        this.StoreItem('range',object);
     }

     StoreKeyEvent(event)
     {
        let object={};
        object.id=event.currentTarget.id;
        object.ty=event.type;
        object.kc=event.code;
        object.ak=event.altKey;
        object.ck=event.ctrlKey;
        object.sk=event.shiftKey;
        this.StoreItem('keypress',object);
     }
     */
    StoreScrollingData(event: Event) {
        if (!this.isActive)
            return;
        let object = {
            x0: document.scrollingElement.scrollLeft,
            y0: document.scrollingElement.scrollTop,
            w: document.scrollingElement.scrollWidth,
            h: document.scrollingElement.scrollHeight
        };
        let trace: Trace = { value: object };
        this.StoreItem('scrolling', trace);
    }

    StoreMousePositionData(event: MouseEvent) {
        if (!this.isActive)
            return;
        this.StoreItem('mousemove', { value: { x: event.clientX, y: event.clientY } });
    }

    StoreMouseClickData(event: MouseEvent, element_id?: string) {
        if (!this.isActive)
            return;
        if (!element_id) {
            let node: HTMLElement = <HTMLElement>event.target;
            this.StoreItem('mouseclick', { id: node.id, value: { x: event.clientX, y: event.clientY }, extra: event.button });
        }
        else
            this.StoreItem('mouseclick', { id: element_id, value: { x: event.clientX, y: event.clientY }, extra: event.button });
    }

    StoreEvent(event: string, eventType: string, element_id: string, extra?: any) {
        if (!this.isActive)
            return;
        if (extra)
            this.StoreItem(event, { id: element_id, value: eventType, extra: extra });
        else
            this.StoreItem(event, { id: element_id, value: eventType });
    }

    GenerateQSortState(): string {
        if (!this.isActive)
            return '';
        let json: string = '"liqert":[';
        for (let iterator: number = 0; iterator < this.liQert.length; iterator++) {
            json += '{ "sequence":' + iterator + ',';
            json += '"statements": [';
            for (let key in this.liQertStatements[iterator]) {
                json += this.liQertStatements[iterator][key];
                if (Number.parseInt(key) < this.liQertStatements[iterator].length - 1)
                    json += ',';
            }
            json += "],\"sort\":[";
            for (let iterator2: number = 0; iterator2 < this.liQert[iterator].length; iterator2++) {
                json += '{"id": "' + this.liQert[iterator][iterator2].id + '","data":[';
                let iterator3: number = 0;
                for (let key of this.liQert[iterator][iterator2].value.keys()) {
                    json += '"' + key + '"';
                    if (iterator3 < this.liQert[iterator][iterator2].value.size - 1)
                        json += ',';
                    iterator3++;
                }
                json += ']}';
                if (iterator2 < this.liQert[iterator].length - 1)
                    json += ',';
            }
            json += ']}';
            if (iterator < this.liQert.length - 1)
                json += ',';
        }
        json += ']';
        return json;
    }

    /* For extended version
     *static GenerateResultPreview()
      {
          let json = '{"statements":[';
          for (let key in randoms) {
              json += randoms[key];
              if (key < randoms.length - 1)
                  json += ',';
          }
          json += '], ';
          if (state > 3 && configuration.threestate)
              json += Storage.GenerateThreeState() + ',';
          if (state > 5)
              json += Storage.GenerateQSortState() + ',';
          json += Storage.GetJSONFromStore("steps") + ',';
          json += Storage.GetJSONFromStore("mousemove") + ',';
          json += Storage.GetJSONFromStore("mouseclick") + ',';
          json += Storage.GetJSONFromStore("drag") + ',';
          json += Storage.GetJSONFromStore("drop") + ',';
          json += Storage.GetJSONFromStore("draggablecontainer");
          json += '}';
          return json;
      }*/

    GenerateTrace(): string {
        let json: string = '"traces":[';
        json += this.GetJSONFromStore("steps") + ',';
        json += this.GetJSONFromStore("range") + ',';
        json += this.GetJSONFromStore("focus") + ',';
        json += this.GetJSONFromStore("change") + ',';
        json += this.GetJSONFromStore("keypress") + ',';
        json += this.GetJSONFromStore("mousemove") + ',';
        json += this.GetJSONFromStore("mouseclick") + ',';
        json += this.GetJSONFromStore("scrolling") + ',';
        json += this.GetJSONFromStore("zooming") + ',';
        json += this.GetJSONFromStore("drag") + ',';
        json += this.GetJSONFromStore("drop") + ',';
        json += this.GetJSONFromStore("draggablecontainer") + ',';
        json += this.GetJSONFromStore("media") + ',';
        json += this.GetJSONFromStore("errors");
        json += ']';
        return json;
    }
}
