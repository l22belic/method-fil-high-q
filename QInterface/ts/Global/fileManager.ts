import JSZip = require("../Global/jszip");

/**
 * FileManager
 */

export class FileManager
{
	static isCompression: boolean;

	static ReadFile(file: File, functor: Function)
	{
		let fileReader: FileReader = new FileReader();
		fileReader.onload=function(event){functor(event.target.result);};
		fileReader.readAsText(file);
	}

	static XMLParse(text: string): Document
	{
		let parser: DOMParser = new DOMParser();
        return parser.parseFromString(text,"text/xml");
    }

    static JSONParse(text: string): any
	{
        return JSON.parse(text);
	}

	static FetchTimeout(url: string, body: any, timeout: number)
	{
		return Promise.race([
			fetch(url, body),
			new Promise((_, reject) =>
				setTimeout(() => reject(new Error('timeout')), timeout)
			)
		]);
	}

	static LoadFile(file: string, parseFunction: Function, onSuccess: Function) 
    {
		fetch(file,{method: "GET"})
		.then(res=> res.text())
			.then(function (data) {
			if(parseFunction(data)&&onSuccess)
				onSuccess();
		});
	}

	static UploadFiles(uploadAddress: string, files: FileList, onDone: Function) 
	{
		for (let iterator in files)
			FileManager.UploadFile(uploadAddress, files[iterator], onDone);
	}

	static UploadFile(uploadAddress: string, file: File, onDone: Function) 
	{
		let formData: FormData = new FormData();
		formData.append('file', file);

		fetch(uploadAddress, {
		method: 'POST',
		body: formData
		})
			.then(function (res) { onDone(res); });
	}

	static UploadBlob(uploadAddress: string, file: Blob, onDone: Function) {
		let formData: FormData = new FormData();
		formData.append('file', file);
		fetch(uploadAddress, {
			method: 'POST',
			body: formData
		}).then(function (res) { onDone(res); }).catch(function (res) { onDone(res); });;
	}

	static GenerateFile(filename: string, text: any) 
	{
		let pom: HTMLAnchorElement = document.createElement('a');
		pom.setAttribute('href', 'data:json/plain;charset=utf-8,' + encodeURIComponent(text));
		pom.setAttribute('download', filename);
		if (document.createEvent) 
		{
			let event: MouseEvent = document.createEvent('MouseEvents');
			event.initEvent('click', true, true);
			pom.dispatchEvent(event);
		}
		else 
		{
			pom.click();
		}
	}

	static SubmitData(uploadAddress: string, data: string, onDone: Function) 
	{
		let form: FormData = new FormData();
		const controller = new AbortController();
		const signal = controller.signal;
		if (FileManager.isCompression) {
			let zipper: JSZip = new JSZip();

			zipper.file("response.json",data);
			zipper.generateAsync({ type: "blob", compression: "DEFLATE" })
				.then(function (blob) {
					FileManager.UploadBlob(uploadAddress, blob, onDone);
				});
		}
		else {
			form.append("json", data);
			FileManager.FetchTimeout(uploadAddress, { method: "POST", body: form }, 3000).then(function (res) { onDone(res); }).catch(function (res) { onDone(res); });
			//fetch(uploadAddress,
			//	{
			//		method: "POST",
			//		body: form
			//	})
			//	.then(function (res) { onDone(res); });
		}
	}
}