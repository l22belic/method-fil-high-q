/*
*   Copyright 2019 Aurélien Milliat <aurelien.milliat@gmail.com>
*   Licensed under the Apache License, Version 2.0 (the "License");
*   you may not use this file except in compliance with the License.
*   You may obtain a copy of the License at
*
*       http://www.apache.org/licenses/LICENSE-2.0
*
*  Unless required by applicable law or agreed to in writing, software
*  distributed under the License is distributed on an "AS IS" BASIS,
*  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
*  See the License for the specific language governing permissions and
*  limitations under the License.
*
*/

var configuration;
var study=null;
var traces={};


/**
 * Local Storage 
 */
 
class AnalysisHistory extends Historic
{
	
	static GetLastStatement(id)
	{
		return Historic.FindLastEvent(currentHistoricalIndex-1,"statement",id);
	}

	static GetLastQSort()
	{
		return Historic.FindLastEvent(currentHistoricalIndex-1,"qsort");
	}

	static GetLastThreeStates(id)
	{
		return Historic.FindLastEvent(currentHistoricalIndex-1,"threestates",id);
	}

	static GetLastExoQuizz()
	{
		return Historic.FindLastEvent(currentHistoricalIndex-1,"exoquizz");
	}

	static GetLastWallOfText(id)
	{
		return Historic.FindLastEvent(currentHistoricalIndex-1,"texts",id);
	}

	static GetLastInterview()
	{
		return Historic.FindLastEvent(currentHistoricalIndex-1,"interview");
	}

	static GetLastHeadPos()
	{
		return Historic.FindLastEvent(currentHistoricalIndex-1,"isCrescent");
	}

	static GetLastStatAlign()
	{
		return Historic.FindLastEvent(currentHistoricalIndex-1,"statAlign");
	}

	static GetLastActiveInterview()
	{
		return Historic.FindLastEvent(currentHistoricalIndex-1,"Interview");
	}

	static GetLastActiveExogen()
	{
		return Historic.FindLastEvent(currentHistoricalIndex-1,"Exogen");
	}

	static GetLastIsDeck(id)
	{
		return Historic.FindLastEvent(currentHistoricalIndex-1,"threeStatesDeck",id);
	}
}

/**
 *	Processor 
 */

class Processor
{
	static Undo()
	{
		if(!currentHistoricalIndex)
			return;
		currentHistoricalIndex--;
		let prevEvent;
		switch(historicalEvents[currentHistoricalIndex].what)
		{
			case 'create':
				switch(historicalEvents[currentHistoricalIndex].type)
				{
					case "statement":
						QAnalysisGenerator.DeleteCard(historicalEvents[currentHistoricalIndex].target.id);
						break;
					case "threestates":
						threestates=[]
						QAnalysisGenerator.DeleteThreeStates();
						break;
					case "qsort":
						qsort={};
						QAnalysisGenerator.DeleteQStates();
						break;
					case "texts":
						texts={};
						QAnalysisGenerator.DeleteWallOfTexts();
						break;
				}
				break
			case 'set':
				switch(historicalEvents[currentHistoricalIndex].type)
				{
					case "statement":
						prevEvent=AdminHistory.GetLastStatement(historicalEvents[currentHistoricalIndex].target.id);
						if(!prevEvent || prevEvent.what==="create")
							Processor.ResetStatementObject(historicalEvents[currentHistoricalIndex].target.id);
						else if(!prevEvent || prevEvent.what==="delete")
							return; // if happend then historic is corrupted
						else
							statements[historicalEvents[currentHistoricalIndex].target.id]=prevEvent.target;
						QAnalysisGenerator.ReloadCard(historicalEvents[currentHistoricalIndex].target.id);
						break;
					case "threestates":
						prevEvent=AdminHistory.GetLastThreeStates();
						if(!prevEvent)
						{
							threestates=[];
							return QAnalysisGenerator.DeleteThreeStates();
						}
						threestates=prevEvent.target;
						QAnalysisGenerator.GenerateThreeStates(threestates,QAnalysisGenerator.GetThreeStatesCheckFunctor(),QAnalysisGenerator.GetThreeStatesInsertToTitleCellFunctor(),QAnalysisGenerator.GetThreeStatesInsertFunctor(),threestates);
						break;
					case "qsort":
						prevEvent=AdminHistory.GetLastQSort();
						if(!prevEvent)
						{
							qsort={};
							return QAnalysisGenerator.DeleteQStates();
						}
						qsort=prevEvent.target;
						QAnalysisGenerator.GenerateQStates(isCrescent,statAlign,qsort,QAnalysisGenerator.GetQSortCheckFunctor(),QAnalysisGenerator.GetQSortInsertToTitleCellFunctor(),QAnalysisGenerator.GetQSortInsertFunctor());
						QAnalysisGenerator.SetQSortInputValue();
						break;
					case "isCrescent":
						prevEvent=AdminHistory.GetLastHeadPos();
						if(!prevEvent)
							isCrescent=0;
						else
							isCrescent=prevEvent.target;
						QAnalysisGenerator.GenerateQStates(isCrescent,statAlign,qsort,QAnalysisGenerator.GetQSortCheckFunctor(),QAnalysisGenerator.GetQSortInsertToTitleCellFunctor(),QAnalysisGenerator.GetQSortInsertFunctor());
						QAnalysisGenerator.SetRadioToValue("headerPosition",isCrescent);
						break;
					case "statAlign":
						prevEvent=AdminHistory.GetLastStatAlign();
						if(!prevEvent)
							statAlign=0;
						else
							statAlign=prevEvent.target;
						QAnalysisGenerator.GenerateQStates(isCrescent,statAlign,qsort,QAnalysisGenerator.GetQSortCheckFunctor(),QAnalysisGenerator.GetQSortInsertToTitleCellFunctor(),QAnalysisGenerator.GetQSortInsertFunctor());
						QAnalysisGenerator.SetRadioToValue("alignementType",statAlign);
						break;
					case "interview":
						prevEvent=AdminHistory.GetLastInterview();
						if(prevEvent)
						{
							interviewType=prevEvent.target.type;
							interviewNumber=prevEvent.target.value;
						}
						else
						{
							interviewType=0;
							interviewNumber=0;
						}
						QAnalysisGenerator.SetInterview();
						break;
					case "exoquizz":
						prevEvent=AdminHistory.GetLastExoQuizz();
						if(!prevEvent)
						{
							exoquizz={};
							return QAnalysisGenerator.DeleteExoView();
						}
						exoquizz=prevEvent.target;
						QAnalysisGenerator.GenerateExoView();
						break;
					case "texts":
						prevEvent=AdminHistory.GetLastWallOfText(historicalEvents[currentHistoricalIndex].target.id);
						if(!prevEvent)
							return QAnalysisGenerator.SetTextValue(historicalEvents[currentHistoricalIndex].target.id,"");
						QAnalysisGenerator.SetTextValue(prevEvent.target.id,prevEvent.target.value);
						break;
					case "Interview":
						prevEvent=AdminHistory.GetLastActiveInterview();
						if(prevEvent)
							isActive.Interview=prevEvent.target;
						else
							isActive.Interview=true;
						QAnalysisGenerator.ShowHideInterview();
						break;
					case "Exogen":
						prevEvent=AdminHistory.GetLastActiveExogen();
						if(prevEvent)
							isActive.Exogen=prevEvent.target;
						else
							isActive.Exogen=true;
						QAnalysisGenerator.ShowHideExogen();
					case "threeStatesDeck":
						prevEvent=AdminHistory.GetLastIsDeck();
						if(prevEvent)
							isThreeStatesDeck=prevEvent.target;
						else
							isThreeStatesDeck=0;
						QAnalysisGenerator.UpdateDeckView(isThreeStatesDeck);
						QAnalysisGenerator.SetRadioToValue("threeStatesDeck",isThreeStatesDeck);
						break;
					default:
						prevEvent=document.getElementById(historicalEvents[currentHistoricalIndex].type);
						if(prevEvent)
							prevEvent.value=historicalEvents[currentHistoricalIndex].target;
				}
				break;
			case 'delete':
				switch(historicalEvents[currentHistoricalIndex].type)
				{
					case "statement":
						statements[historicalEvents[currentHistoricalIndex].target.id]=historicalEvents[currentHistoricalIndex].target;
						QAnalysisGenerator.GenerateCard(historicalEvents[currentHistoricalIndex].target);
						break;
					case "threestates":
						threestates=historicalEvents[currentHistoricalIndex].target;
						QAnalysisGenerator.GenerateThreeStates(threestates,QAnalysisGenerator.GetThreeStatesCheckFunctor(),QAnalysisGenerator.GetThreeStatesInsertToTitleCellFunctor(),QAnalysisGenerator.GetThreeStatesInsertFunctor(),threestates);
						break;
					case "qsort":
						qsort=historicalEvents[currentHistoricalIndex].target;
						QAnalysisGenerator.GenerateQStates(isCrescent,statAlign,qsort,QAnalysisGenerator.GetQSortCheckFunctor(),QAnalysisGenerator.GetQSortInsertToTitleCellFunctor(),QAnalysisGenerator.GetQSortInsertFunctor());
						QAnalysisGenerator.SetQSortInputValue();
						break;
					case "exoquizz":
						exoquizz=historicalEvents[currentHistoricalIndex].target;
						QAnalysisGenerator.GenerateExoView();
						break;
					case "texts":
						texts=historicalEvents[currentHistoricalIndex].target;
						QAnalysisGenerator.GenerateWallOfTexts();
						break;
				}
				break;
			case 'reset':
				switch(historicalEvents[currentHistoricalIndex].type)
				{
					case "statement":
						statements=historicalEvents[currentHistoricalIndex].target;
						QAnalysisGenerator.GenerateCards();
				}
				break;
		}
	}

	static Redo()
	{
		if(historicalEvents.length<currentHistoricalIndex+1)
			return;
		switch(historicalEvents[currentHistoricalIndex].what)
		{
			case 'create':
				switch(historicalEvents[currentHistoricalIndex].type)
				{
					case "statement":
						statements[historicalEvents[currentHistoricalIndex].target.id]=historicalEvents[currentHistoricalIndex].target;
						QAnalysisGenerator.GenerateCard(historicalEvents[currentHistoricalIndex].target);
						break;
					case "threestates":
						threestates=historicalEvents[currentHistoricalIndex].target;
						QAnalysisGenerator.GenerateThreeStates(threestates,QAnalysisGenerator.GetThreeStatesCheckFunctor(),QAnalysisGenerator.GetThreeStatesInsertToTitleCellFunctor(),QAnalysisGenerator.GetThreeStatesInsertFunctor(),threestates);
						break;
					case "qsort":
						qsort=historicalEvents[currentHistoricalIndex].target;
						QAnalysisGenerator.GenerateQStates(isCrescent,statAlign,qsort,QAnalysisGenerator.GetQSortCheckFunctor(),QAnalysisGenerator.GetQSortInsertToTitleCellFunctor(),QAnalysisGenerator.GetQSortInsertFunctor());
						QAnalysisGenerator.SetQSortInputValue();
						break;
					case "texts":
						texts=historicalEvents[currentHistoricalIndex].target;
						QAnalysisGenerator.GenerateWallOfTexts();
						break;
				}
				break;
			case 'set':
				switch(historicalEvents[currentHistoricalIndex].type)
				{
					case "statement":
						statements[historicalEvents[currentHistoricalIndex].target.id]=historicalEvents[currentHistoricalIndex].target;
						QAnalysisGenerator.ReloadCard(historicalEvents[currentHistoricalIndex].target.id);
						break;
					case "threestates":
						threestates=historicalEvents[currentHistoricalIndex].target;
						QAnalysisGenerator.GenerateThreeStates(threestates,QAnalysisGenerator.GetThreeStatesCheckFunctor(),QAnalysisGenerator.GetThreeStatesInsertToTitleCellFunctor(),QAnalysisGenerator.GetThreeStatesInsertFunctor(),threestates);
						break;
					case "qsort":
						qsort=historicalEvents[currentHistoricalIndex].target;
						QAnalysisGenerator.GenerateQStates(isCrescent,statAlign,qsort,QAnalysisGenerator.GetQSortCheckFunctor(),QAnalysisGenerator.GetQSortInsertToTitleCellFunctor(),QAnalysisGenerator.GetQSortInsertFunctor());
						QAnalysisGenerator.SetQSortInputValue();
						break;
					case "isCrescent":
						isCrescent=historicalEvents[currentHistoricalIndex].target;
						QAnalysisGenerator.GenerateQStates(isCrescent,statAlign,qsort,QAnalysisGenerator.GetQSortCheckFunctor(),QAnalysisGenerator.GetQSortInsertToTitleCellFunctor(),QAnalysisGenerator.GetQSortInsertFunctor());
						QAnalysisGenerator.SetRadioToValue("headerPosition",isCrescent);
						break;
					case "statAlign":
						statAlign=historicalEvents[currentHistoricalIndex].target;
						QAnalysisGenerator.GenerateQStates(isCrescent,statAlign,qsort,QAnalysisGenerator.GetQSortCheckFunctor(),QAnalysisGenerator.GetQSortInsertToTitleCellFunctor(),QAnalysisGenerator.GetQSortInsertFunctor());
						QAnalysisGenerator.SetRadioToValue("alignementType",statAlign);
						break;
					case "interview":
						interviewType=historicalEvents[currentHistoricalIndex].target.type;
						interviewNumber=historicalEvents[currentHistoricalIndex].target.value;
						QAnalysisGenerator.SetInterviewState();
						break;
					case "exoquizz":
						exoquizz=historicalEvents[currentHistoricalIndex].target;
						QAnalysisGenerator.GenerateExoView();
						break;
					case "texts":
						QAnalysisGenerator.SetTextValue(historicalEvents[currentHistoricalIndex].target.id,historicalEvents[currentHistoricalIndex].target.value);
						break;
					case "Interview":
						isActive.Interview=historicalEvents[currentHistoricalIndex].target;
						QAnalysisGenerator.ShowHideInterview();
						break;
					case "Exogen":
						isActive.Exogen=historicalEvents[currentHistoricalIndex].target;
						QAnalysisGenerator.ShowHideExogen();
						break;
					case "threeStatesDeck":
						isThreeStatesDeck=historicalEvents[currentHistoricalIndex].target;
						QAnalysisGenerator.UpdateDeckView(isThreeStatesDeck);
						QAnalysisGenerator.SetRadioToValue("threeStatesDeck",isThreeStatesDeck);
						break;
				}
				break;
			case 'delete':
				switch(historicalEvents[currentHistoricalIndex].type)
				{
					case "statement":
						QAnalysisGenerator.DeleteCard(historicalEvents[currentHistoricalIndex].target.id);
						break;
					case "threestates":
						threestates=[]
						QAnalysisGenerator.DeleteThreeStates();
						break;
					case "qsort":
						qsort={}
						QAnalysisGenerator.DeleteQStates();
						break;
					case "exoquizz":
						exoquizz={};
						QAnalysisGenerator.DeleteExoView();
						break;
					case "texts":
						QAnalysisGenerator.DeleteWallOfTexts();
						break;
				}
				break;
			case 'reset':
				switch(historicalEvents[currentHistoricalIndex].type)
				{
					case "statement":
						statements={};
						QAnalysisGenerator.CleanNode("pickingZone");
						break;
				}
				break;
			default:
				return;
		}
		currentHistoricalIndex++;
	}

	static SaveStudy(autosave)
	{
		let json=null;//Processor.GenerateConfig(autoSave)
		if(!json)
			return false;
		return AnalysisHistory.SaveInStorage(json);
	}

	static CheckLoadState(sendAlert)
	{
		if(!study)
		{
			if(sendAlert)
				alert(configuration.studyFileNeeded);
			return false;
		}
		if(!Object.keys(traces).length)
		{
			if(sendAlert)
				alert(configuration.tracesFilesNeeded);
			return false;
		}
		return true;
	}

	static ProcessConfigFile(file) 
	{
		FileManager.ReadFile(file,function(text){try{study=FileManager.JSONParse(text);if(study.serverurlsubmit){if(Processor.CheckLoadState(false))document.getElementById("processDiv").style.display="";document.getElementById("studydropZone").style.display="none";return true;}study=null;throw configuration.notaStudyFile;}catch(e){alert(configuration.notaStudyFile);console.log(e);return false;}});
	}

	static ProcessTraceFiles(files) 
	{
		for(let iterator=0; iterator<Object.keys(files).length; iterator++)
			FileManager.ReadFile(files[iterator],function(text){try{let trace=FileManager.JSONParse(text);if(trace.postdata){traces[iterator]=trace;if(Processor.CheckLoadState(false))document.getElementById("processDiv").style.display="";document.getElementById("tracesdropZone").style.display="none";return true;};throw configuration.notaTraceFile;}catch(e){alert(files[iterator].name+": "+configuration.notaTraceFile);console.log(files[iterator].name+": "+configuration.notaTraceFile);return false;}});
	}

	static GenerateInterview()
	{
		let csv="ind_id;class;statement_id";
		for(let iterator=0; iterator<Object.keys(traces).length; iterator++)
		{
			traces[iterator].postdata
		}
	}
}

/**
 * IHM
 */ 
class QAnalysisGenerator extends DOMGenerator 
{

	static GenerateGeneralPage()
	{	
		let main=QAnalysisGenerator.GetMain();
		QAnalysisGenerator.CleanMain();
		let subdiv=document.createElement("div");
		let table=subdiv.appendChild(document.createElement("table"));
		table.style.width="100%";
		let row=table.insertRow();
		let cell=row.insertCell();
		cell.style.width="50%";
		QAnalysisGenerator.GenerateDropZone("studydropZone",configuration.loadConfigurationFile,cell,"*",true,null,function(files){Processor.ProcessConfigFile(files[0]);});
		cell=row.insertCell();
		cell.style.width="50%";
		QAnalysisGenerator.GenerateDropZone("tracesdropZone",configuration.loadResultsFiles,cell,"*",true,null,function(files){Processor.ProcessTraceFiles(files);});
		main.appendChild(QAnalysisGenerator.GenSubTitle("1."+configuration.loadStudyFiles,subdiv,configuration.loadStudyFilesDesc));
		
		subdiv=document.createElement("div");
		table=subdiv.appendChild(document.createElement("table"));
		table.style.width="100%";
		row=table.insertRow();
		cell=row.insertCell();
		cell.style.width="33%";
		let button=cell.appendChild(document.createElement("button"));
		button.appendChild(document.createTextNode(configuration.downloadPQMethodFile));
		button.onclick=function(event){Processor.GeneratePQMethodFile()};
		cell=row.insertCell();
		cell.style.width="33%";
		button=cell.appendChild(document.createElement("button"));
		button.appendChild(document.createTextNode(configuration.downloadExoDataFile));
		button.onclick=function(event){Processor.GeneratePQMethodFile()};
		cell=row.insertCell();
		cell.style.width="33%";
		button=cell.appendChild(document.createElement("button"));
		button.appendChild(document.createTextNode(configuration.downloadInterviewFile));
		button.onclick=function(event){Processor.GenerateInterviewFile()};
		main.appendChild(QAnalysisGenerator.GenSubTitle("2."+configuration.exportProcess,subdiv,null));
		subdiv.id="processDiv";
		subdiv.style.display="none";
		
		/*let table=document.createElement("table");
		table.style.width="100%";
		let row=table.insertRow();
		let cell=row.insertCell();
		cell.style.width="45%";
		QAnalysisGenerator.GenerateDropZone("statsdropZone",configuration.statementDropFileText,cell,"*",true,null,function(files){Processor.ProcessFileList(files);});
		cell=row.insertCell();
		cell.style.width="45%";
		let button=cell.appendChild(document.createElement("button"));
		button.appendChild(document.createTextNode(configuration.addStatementText));
		button.onclick=function(event){let object=QAnalysisGenerator.AddStatement(statType);AdminHistory.StoreInHistoric("statement","create",object)};
		let radio=cell.appendChild(document.createElement("div"));
		radio.id="statementType";
		for(let iterator=0; iterator<configuration.statementTypes.length; iterator++)
		{
			let rInput=document.createElement("input");
			rInput.value=configuration.statementTypes[iterator].value;
			rInput.type="radio";
			rInput.name="statementType";
			rInput.id=configuration.statementTypes[iterator].text;
			rInput.onchange=function(event){statType=Number.parseInt(event.currentTarget.value);};
			if(iterator===0)
				rInput.checked=true;
			radio.appendChild(rInput);
			let label=document.createElement("label");
			label.innerHTML=configuration.statementTypes[iterator].text;
			label.htmlFor=rInput.id;
			radio.appendChild(label);
		}
		cell=row.insertCell();
		cell.style.width="10%";
		button=cell.appendChild(document.createElement("button"));
		button.appendChild(document.createTextNode(configuration.downloadStaFile));
		button.onclick=function(event){Processor.GenerateStatementsFile()};
		cell.appendChild(document.createElement("br"));
		cell.appendChild(document.createElement("br"));
		button=cell.appendChild(document.createElement("button"));
		button.appendChild(document.createTextNode(configuration.resetText));
		button.onclick=function(event){AdminHistory.StoreInHistoric("statement","reset",statements);statements={};QAnalysisGenerator.CleanNode("pickingZone");};
		subdiv=document.createElement("div");
		subdiv.appendChild(table);
		let div=subdiv.appendChild(document.createElement("div"));
		div.id="pickingZone";
		main.appendChild(QAnalysisGenerator.GenSubTitle("1."+configuration.statementsTitle,subdiv,configuration.statementsTitleDesc));
		
		table=document.createElement("table");
		table.style.width="100%";
		row=table.insertRow();
		cell=row.insertCell();
		cell.style.width="45%";
		button=cell.appendChild(document.createElement("button"));
		button.appendChild(document.createTextNode(configuration.threeStateText));
		button.onclick=function(event){if(!threestates.length){AdminHistory.StoreInHistoric("threestates","create",threestates);Processor.GenerateThreeStates();QAnalysisGenerator.GenerateThreeStates(threestates,QAnalysisGenerator.GetThreeStatesCheckFunctor(),QAnalysisGenerator.GetThreeStatesInsertToTitleCellFunctor(),QAnalysisGenerator.GetThreeStatesInsertFunctor(),threestates);}else{AdminHistory.StoreInHistoric("threestates","delete",threestates);threestates=[];QAnalysisGenerator.DeleteThreeStates()}};
		radio=cell.appendChild(document.createElement("div"));
		radio.appendChild(document.createTextNode(configuration.threeStateIsDeckText));
		radio.id="threeStatesDeck";
		for(let iterator=0; iterator<configuration.threeStateIsDeck.length; iterator++)
		{
			let rInput=document.createElement("input");
			rInput.value=configuration.threeStateIsDeck[iterator].value;
			rInput.type="radio";
			rInput.name="threeStatesDeck";
			rInput.id=configuration.threeStateIsDeck[iterator].text;
			rInput.onchange=function(event){isThreeStatesDeck=Number.parseInt(event.currentTarget.value);AdminHistory.StoreInHistoric("threeStatesDeck","set",isThreeStatesDeck);QAnalysisGenerator.UpdateDeckView(isThreeStatesDeck);};
			if(iterator===0)
				rInput.checked=true;
			radio.appendChild(rInput);
			let label=document.createElement("label");
			label.innerHTML=configuration.threeStateIsDeck[iterator].text;
			label.htmlFor=rInput.id;
			radio.appendChild(label);
		}
		cell=row.insertCell();
		cell.style.width="45%";
		button=cell.appendChild(document.createElement("button"));
		button.appendChild(document.createTextNode(configuration.generateQSort));
		button.onclick=function(event){if(!Object.keys(qsort).length){QAnalysisGenerator.QTabPlacement(document.getElementById("qsortmin").value,document.getElementById("qsortmax").value);QAnalysisGenerator.GenerateQStates(isCrescent,statAlign,qsort,QAnalysisGenerator.GetQSortCheckFunctor(),QAnalysisGenerator.GetQSortInsertToTitleCellFunctor(),QAnalysisGenerator.GetQSortInsertFunctor());AdminHistory.StoreInHistoric("qsort","create",qsort);}else if(document.getElementById("qstates")){AdminHistory.StoreInHistoric("qsort","delete",qsort);qsort={};QAnalysisGenerator.DeleteQStates();}}
		let inputGroup=cell.appendChild(document.createElement("div"));
		inputGroup.appendChild(document.createTextNode(configuration.qSortMinValue+" : "));
		let input=inputGroup.appendChild(document.createElement("input"));
		input.type="number";
		input.id="qsortmin";
		input.value=-3;
		input.onchange=function(event){AdminHistory.StoreInHistoric("qsortmin","set",event.target.value);if(event.target.value>0)event.target.value=-event.target.value;document.getElementById("qsortmax").value=-event.target.value;QAnalysisGenerator.QTabPlacement(document.getElementById("qsortmin").value,document.getElementById("qsortmax").value);QAnalysisGenerator.GenerateQStates(QAnalysisGenerator.GetQSortCheckFunctor(),QAnalysisGenerator.GetQSortInsertToTitleCellFunctor(),QAnalysisGenerator.GetQSortInsertFunctor());AdminHistory.StoreInHistoric("qsort","set",qsort);};
		inputGroup=cell.appendChild(document.createElement("div"));
		inputGroup.appendChild(document.createTextNode(configuration.qSortMaxValue+" : "));
		input=inputGroup.appendChild(document.createElement("input"));
		input.onchange=function(event){QAnalysisGenerator.QTabPlacement(document.getElementById("qsortmin").value,document.getElementById("qsortmax").value);QAnalysisGenerator.GenerateQStates(isCrescent,statAlign,qsort,QAnalysisGenerator.GetQSortCheckFunctor(),QAnalysisGenerator.GetQSortInsertToTitleCellFunctor(),QAnalysisGenerator.GetQSortInsertFunctor());AdminHistory.StoreInHistoric("qsort","set",qsort);};
		input.type="number";
		input.id="qsortmax";
		input.value=3;
		radio=cell.appendChild(document.createElement("div"));
		radio.appendChild(document.createTextNode(configuration.headerStyleText));
		radio.id="headerStyle";
		for(let iterator=0; iterator<configuration.headerStyle.length; iterator++)
		{
			let rInput=document.createElement("input");
			rInput.value=configuration.headerStyle[iterator].value;
			rInput.type="radio";
			rInput.name="headerStyle";
			rInput.id=configuration.headerStyle[iterator].text;
			rInput.onchange=function(event){isCrescent=Number.parseInt(event.currentTarget.value);AdminHistory.StoreInHistoric("isCrescent","set",isCrescent);QAnalysisGenerator.GenerateQStates(isCrescent,statAlign,qsort,QAnalysisGenerator.GetQSortCheckFunctor(),QAnalysisGenerator.GetQSortInsertToTitleCellFunctor(),QAnalysisGenerator.GetQSortInsertFunctor());};
			if(iterator===0)
				rInput.checked=true;
			radio.appendChild(rInput);
			let label=document.createElement("label");
			label.innerHTML=configuration.headerStyle[iterator].text;
			label.htmlFor=rInput.id;
			radio.appendChild(label);
		}
		radio=cell.appendChild(document.createElement("div"));
		radio.appendChild(document.createTextNode(configuration.statementsAlignementText));
		radio.id="alignementType";
		for(let iterator=0; iterator<configuration.statementsAlignement.length; iterator++)
		{
			let rInput=document.createElement("input");
			rInput.value=configuration.statementsAlignement[iterator].value;
			rInput.type="radio";
			rInput.name="alignementType";
			rInput.id=configuration.statementsAlignement[iterator].text;
			rInput.onchange=function(event){statAlign=Number.parseInt(event.currentTarget.value);AdminHistory.StoreInHistoric("statAlign","set",statAlign);QAnalysisGenerator.GenerateQStates(isCrescent,statAlign,qsort,QAnalysisGenerator.GetQSortCheckFunctor(),QAnalysisGenerator.GetQSortInsertToTitleCellFunctor(),QAnalysisGenerator.GetQSortInsertFunctor());
						};
			if(iterator===0)
				rInput.checked=true;
			radio.appendChild(rInput);
			let label=document.createElement("label");
			label.innerHTML=configuration.statementsAlignement[iterator].text;
			label.htmlFor=rInput.id;
			radio.appendChild(label);
		}
		cell=row.insertCell();
		cell.style.width="10%";
		button=cell.appendChild(document.createElement('button'))
		button.appendChild(document.createTextNode(configuration.resetText));
		button.onclick=function(event){AdminHistory.StoreInHistoric("threestates","delete",threestates);AdminHistory.StoreInHistoric("qsort","delete",qsort);qsort={};threestates=[];QAnalysisGenerator.CleanNode("simulator");};	
		subdiv=document.createElement("div");
		subdiv.appendChild(table);
		div=subdiv.appendChild(document.createElement("div"));
		div.id="simulator";
		main.appendChild(QAnalysisGenerator.GenSubTitle("2."+configuration.phasesTitle,subdiv,configuration.phasesTitleDesc));
	
		table=document.createElement("table");
		table.style.width="100%";
		row=table.insertRow();
		cell=row.insertCell();
		cell.style.width="45%";	
		div=cell.appendChild(document.createElement("div"));
		div.appendChild(document.createTextNode(configuration.interviewCalcTypeText));
		radio=cell.appendChild(document.createElement("div"));
		radio.id="interviewType";
		for(let iterator=0; iterator<configuration.interviewCalcType.length; iterator++)
		{
			let rInput=document.createElement("input");
			rInput.value=configuration.interviewCalcType[iterator].value;
			rInput.type="radio";
			rInput.name="interviewType";
			rInput.id=configuration.interviewCalcType[iterator].text;
			if(iterator===0)
			{
				rInput.onchange=function(event){document.getElementById("interviewInput").disabled=true;interviewType=Number.parseInt(event.currentTarget.value);Processor.SetInterviewState();};
				rInput.checked=true;
			}
			else
				rInput.onchange=function(event){document.getElementById("interviewInput").disabled=false;interviewType=Number.parseInt(event.currentTarget.value);Processor.SetInterviewState();};
			radio.appendChild(rInput);
			let label=document.createElement("label");
			label.innerHTML=configuration.interviewCalcType[iterator].text;
			label.htmlFor=rInput.id;
			radio.appendChild(label);
		}
		cell=row.insertCell();
		cell.style.width="45%";
		div=cell.appendChild(document.createElement("div"));
		div.appendChild(document.createTextNode(configuration.interviewNumberOfStatementText));
		input=div.appendChild(document.createElement("input"))
		input.type="number";
		input.id="interviewInput";
		input.disabled=true;
		input.onchange=function(event){interviewNumber=Number.parseInt(event.target.value);};
		input.onblur=function(){Processor.SetInterviewState();};
		cell=row.insertCell();
		cell.style.width="10%";
		button=cell.appendChild(document.createElement("button"));
		button.appendChild(document.createTextNode(configuration.resetText));
		button.onclick=function(event){interviewType=0;interviewNumber=0;QAnalysisGenerator.SetInterview();Processor.SetInterviewState();};
		subdiv=document.createElement("div");
		subdiv.appendChild(table);
		div=subdiv.appendChild(document.createElement("div"));
		div.id="interviewDiv";
		let form=div.appendChild(document.createElement("table"));
		form.id="interviewTable";
		main.appendChild(QAnalysisGenerator.GenSubTitle("3."+configuration.interviewTitle,subdiv,configuration.interviewTitleDesc,"Interview"));

		table=document.createElement("table");
		table.style.width="100%";
		row=table.insertRow();
		cell=row.insertCell();
		cell.style.width="90%";
		button=cell.appendChild(document.createElement("button"));
		button.appendChild(document.createTextNode(configuration.addExoQuizzText));
		button.onclick=function(event){QAnalysisGenerator.AddExoQuizz(exoType);AdminHistory.StoreInHistoric("exoquizz","set",exoquizz);};
		radio=cell.appendChild(document.createElement("div"));
		radio.id="exoType";
		for(let iterator=0; iterator<configuration.exoQuizzTypes.length; iterator++)
		{
			let rInput=document.createElement("input");
			rInput.value=configuration.exoQuizzTypes[iterator].value;
			rInput.type="radio";
			rInput.name="exoType";
			rInput.id=configuration.exoQuizzTypes[iterator].text;
			rInput.onchange=function(event){exoType=Number.parseInt(event.currentTarget.value);};
			if(iterator===0)
				rInput.checked=true;
			radio.appendChild(rInput);
			let label=document.createElement("label");
			label.innerHTML=configuration.exoQuizzTypes[iterator].text;
			label.htmlFor=rInput.id;
			radio.appendChild(label);
		}
		cell=row.insertCell();
		cell.style.width="10%";
		button=cell.appendChild(document.createElement("button"));
		button.appendChild(document.createTextNode(configuration.resetText));
		button.onclick=function(event){AdminHistory.StoreInHistoric("exoquizz","delete",exoquizz);exoquizz={};QAnalysisGenerator.DeleteExoView();};
		subdiv=document.createElement("div");
		subdiv.appendChild(table);
		div=subdiv.appendChild(document.createElement("div"));
		div.id="exoDiv";
		form=div.appendChild(document.createElement("table"));
		form.id="exoTable";
		main.appendChild(QAnalysisGenerator.GenSubTitle("4."+configuration.exogenTitle,subdiv,configuration.exogenTitleDesc,"Exogen"));
		
		table=document.createElement("table");
		table.style.width="100%";
		row=table.insertRow();
		cell=row.insertCell();
		cell.style.width="90%";
		button=cell.appendChild(document.createElement("button"));
		button.appendChild(document.createTextNode(configuration.generateTextsList));
		button.onclick=function(event){let table=document.getElementById("textsTable");if(table){if(table.style.display==="")table.style.display="none";else table.style.display="";}else{AdminHistory.StoreInHistoric("texts",'create',texts);QAnalysisGenerator.GenerateWallOfTexts();}};
		cell=row.insertCell();
		cell.style.width="10%";
		button=cell.appendChild(document.createElement("button"));
		button.appendChild(document.createTextNode(configuration.resetText));
		button.onclick=function(event){AdminHistory.StoreInHistoric("texts",'delete',texts);texts={};QAnalysisGenerator.DeleteWallOfTexts();};
		subdiv=document.createElement("div");
		subdiv.appendChild(table);
		div=subdiv.appendChild(document.createElement("div"));
		div.id="texts";
		main.appendChild(QAnalysisGenerator.GenSubTitle("5."+configuration.textsTitle,subdiv,configuration.textsTitleDesc));
		
		table=document.createElement("table");
		table.style.width="100%";
		row=table.insertRow();
		cell=row.insertCell();
		cell.style.width="100%";
		button=cell.appendChild(document.createElement("button"));
		button.appendChild(document.createTextNode(configuration.generateConfigurationFile));
		button.onclick=function(event){Processor.GenerateConfigFile()};
		cell.appendChild(document.createElement("br"));
		cell.appendChild(document.createElement("br"));
		button=cell.appendChild(document.createElement("button"));
		button.appendChild(document.createTextNode(configuration.saveDataText));
		button.onclick=function(event){Processor.SaveConfig(false)};
		cell.appendChild(document.createElement("br"));
		cell.appendChild(document.createElement("br"));
		button=cell.appendChild(document.createElement("button"));
		button.appendChild(document.createTextNode(configuration.loadDataText));
		button.onclick=function(event){Processor.LoadConfig(false)};
		subdiv=document.createElement("div");
		subdiv.appendChild(table);
		main.appendChild(QAnalysisGenerator.GenSubTitle("6."+configuration.generalTitle,subdiv,configuration.generalTitleDesc));*/
	}
}

function start()
{
    Historic.StorageName="QAnalysis";
    QDatabase.Initialise("QAnalysis");
    FileManager.LoadFile('analysis',function(text){try{configuration=FileManager.JSONParse(text); return true;}catch(e){console.log(e); return false;}},function(){let olderData=AnalysisHistory.GetSavedData();TextEditor.InitialiseEditor(configuration.textEditorPreviewText,textEditorDefaultSizes,textEditorDefaultColors,textEditorDefaultColors);QAnalysisGenerator.GenerateGeneralPage();if(olderData && olderData!=="undefined"){let conf=confirm(configuration.reloadOlderDataText);if(conf)Processor.ProcessConfig(olderData);else AnalysisHistory.ClearStorage();}autoSave();});

}

function checkKeyPress(event)
{
	if(!('keyCode' in event)||!event.ctrlKey)
		return;
	let done=true;
	switch(event.keyCode)
	{
		case 90: //ctrl+z UNDO
			Processor.Undo();
			break;
		case 89: //ctrl+y REDO
			Processor.Redo();
			break;
		case 80: //ctrl+P PRINT
			Processor.GenerateConfigFile();
			break;
		case 83: //ctrl+s SAVE
			Processor.SaveConfig(false);
			break;
		case 70: //ctrl+F LOAD
			Processor.LoadConfig();
			break;
		default:
			done=false;
	}
	if(done)
	{
		event.preventDefault();
		event.stopPropagation();
	}
}

function autoSave()
{
	let id=setInterval(function(){if(!Processor.SaveStudy(true)){clearInterval(id);}},3000000); //every 5 minutes 
}