import { Tab, Tabs, Statement, Statements } from "../Global/types"
import { EStatementAlignement, getAlignementType, getAlignementTypeText, DOMGenerator } from "../Global/domGenerator"
import { QAdminHistoric } from "./qAdminHistoric"
import { QAdminGenerator } from "./qAdminGenerator"
import { HistoricEvent } from "../Global/historic"
import { FileManager } from "../Global/fileManager"

/**
 * Basic interface & type
 */

interface IText
{
	id: string;
	value: string;
	default?: string;
	step?: string;
}

export type Text = IText;
export type Texts = Array<Text>;

interface IActive
{
	interview: boolean;
	exogen: boolean;
}

type Active = IActive;

interface IInterviewType
{
	type: number;
	number: number;
}

export type InterviewType = IInterviewType;

interface IParameterValue
{
	value: any;
	name: string;
}

export type ParameterValue = IParameterValue;

interface IExogen
{
	id: string;
	type: string;
	text?: string;
	values?: Array<any>;
	parameters?: Set<ParameterValue>;
}

export type Exogen = IExogen;

interface IOption
{
	value: string;
	label: string;
}

export type Option = IOption;

/**
 *	Processor 
 */

export class QAdminProcessor
{
	historic: QAdminHistoric;
	threeStates: Array<Tab>;
	qSort: Tabs;
	texts: Texts;
	statements: Statements;
	isPresentation: boolean;
	isCrescent: boolean;
	isSendButtonsAtExogen: boolean;
	statAlign: EStatementAlignement;
	interviewType: number;
	interviewNumber: number;
	statementType: number;
	isActive: Active;
	isThreeStatesDeck: boolean;
	globalCounter: number;
	exoCounter: number;
	exoQuizz: Array<Exogen>;
	exoType: number;
	configuration: any;  //TO DO: probably need a spec
	configurationFromFile: any;  //TO DO: probably need a spec
	mediacontents = {};
	mediafiles = {};
	idAutosave: number;

	constructor()
	{
		this.globalCounter=0;
		this.exoCounter=0;
		this.isCrescent=true;
		this.isThreeStatesDeck = false;
		this.threeStates = new Array<Tab>();
		this.qSort = new Array<Tab>();
		this.texts = new Array<Text>();
		this.statements = new Array<Statement>();
		this.isActive = <Active>{ interview: true, exogen: true };
		this.exoQuizz = new Array<Exogen>();
		this.isCrescent = true;
		this.statAlign = EStatementAlignement.Top;
		this.interviewType = 0;
		this.interviewNumber = 0;
		this.statementType = 0;
		this.isThreeStatesDeck = false;
		this.exoType = 0;
		this.isSendButtonsAtExogen = false;
		this.isPresentation = true;
	}

	AutoSave()
	{
		setInterval(function (processor: QAdminProcessor, idAutosave: number) { return function () { if (!processor.SaveConfig(true)) { clearInterval(idAutosave); } }; }(this, this.idAutosave), 3000000); //every 5 minutes 
	}

	Undo(dom: QAdminGenerator)
	{
		if (!this.historic.currentHistoricalIndex)
			return;
		this.historic.currentHistoricalIndex--;
		let prevEvent: HistoricEvent;
		switch (this.historic.historicalEvents[this.historic.currentHistoricalIndex].what)
		{
			case 'create':
				switch (this.historic.historicalEvents[this.historic.currentHistoricalIndex].type)
				{
					case "statement":
						dom.DeleteCard(this.historic.historicalEvents[this.historic.currentHistoricalIndex].target.id);
						break;
					case "threeStates":
						this.threeStates = new Array<Tab>();
						dom.DeleteThreeStates();
						break;
					case "qsort":
						this.qSort = new Array<Tab>();
						dom.DeleteQSort();
						break;
					case "texts":
						this.texts = new Array<Text>();
						dom.DeleteWallOfTexts();
						break;
				}
				break
			case 'set':
				switch (this.historic.historicalEvents[this.historic.currentHistoricalIndex].type)
				{
					case "statement":
						prevEvent = this.historic.GetLastStatement(this.historic.historicalEvents[this.historic.currentHistoricalIndex].target.id);
						if (!prevEvent || prevEvent.what === "create")
							this.ResetStatementObject(this.historic.historicalEvents[this.historic.currentHistoricalIndex].target.id);
						else if (!prevEvent || prevEvent.what === "delete")
							return; // if happend then historic is corrupted
						else
							this.statements[this.historic.historicalEvents[this.historic.currentHistoricalIndex].target.id] = prevEvent.target;
						dom.ReloadCard(this.historic.historicalEvents[this.historic.currentHistoricalIndex].target.id);
						break;
					case "threeStates":
						prevEvent = this.historic.GetLastThreeStates();
						if (!prevEvent)
						{
							this.threeStates = new Array<Tab>();
							return dom.DeleteThreeStates();
						}
						this.threeStates = <Array<Tab>>prevEvent.target;
						dom.GenerateThreeStates(this.threeStates, dom.GetThreeStatesCheckFunctor(), dom.GetThreeStatesInsertToTitleCellFunctor(), dom.GetThreeStatesInsertFunctor());
						break;
					case "qsort":
						prevEvent = this.historic.GetLastQSort();
						if (!prevEvent)
						{
							this.qSort = new Array<Tab>();
							return dom.DeleteQSort();
						}
						this.qSort = <Tabs>prevEvent.target;
						dom.GenerateQSort(this.isCrescent, this.statAlign, this.qSort, this.statements.length, dom.GetQSortCheckFunctor(), dom.GetQSortInsertToTitleCellFunctor(), dom.GetQSortInsertFunctor());
						dom.SetQSortInputValue();
						break;
					case "isCrescent":
						prevEvent = this.historic.GetLastHeadPos();
						if (!prevEvent)
							this.isCrescent = false;
						else
							this.isCrescent = <boolean>prevEvent.target;
						dom.GenerateQSort(this.isCrescent, this.statAlign, this.qSort, this.statements.length, dom.GetQSortCheckFunctor(), dom.GetQSortInsertToTitleCellFunctor(), dom.GetQSortInsertFunctor());
						dom.SetRadioToValue("headerPosition", this.isCrescent ? 1 : 0);
						break;
					case "statAlign":
						prevEvent = this.historic.GetLastStatAlign();
						if (!prevEvent)
							this.statAlign = EStatementAlignement.Top;
						else
							this.statAlign = prevEvent.target;
						dom.GenerateQSort(this.isCrescent, this.statAlign, this.qSort, this.statements.length, dom.GetQSortCheckFunctor(), dom.GetQSortInsertToTitleCellFunctor(), dom.GetQSortInsertFunctor());
						dom.SetRadioToValue("alignementType", this.statAlign);
						break;
					case "interview":
						prevEvent = this.historic.GetLastInterview();
						if (prevEvent)
						{
							this.interviewType = prevEvent.target.type;
							this.interviewNumber = prevEvent.target.value;
						}
						else
						{
							this.interviewType = 0;
							this.interviewNumber = 0;
						}
						dom.SetInterview();
						break;
					case "exoquizz":
						prevEvent = this.historic.GetLastExoQuizz();
						if (!prevEvent)
						{
							this.exoQuizz = new Array<Exogen>();
							return dom.DeleteExoView();
						}
						this.exoQuizz = prevEvent.target;
						dom.GenerateExoView();
						break;
					case "texts":
						prevEvent = this.historic.GetLastWallOfText(this.historic.historicalEvents[this.historic.currentHistoricalIndex].target.id);
						if (!prevEvent)
							return dom.SetTextValue(this.historic.historicalEvents[this.historic.currentHistoricalIndex].target.id, "");
						dom.SetTextValue(prevEvent.target.id, prevEvent.target.value);
						break;
					case "activeInterview":
						prevEvent = this.historic.GetLastActiveInterview();
						if (prevEvent)
							this.isActive.interview = prevEvent.target;
						else
							this.isActive.interview = true;
						dom.ShowHideInterview();
						break;
					case "activeExogen":
						prevEvent = this.historic.GetLastActiveExogen();
						if (prevEvent)
							this.isActive.exogen = prevEvent.target;
						else
							this.isActive.exogen = true;
						dom.ShowHideExogen();
					case "threeStatesDeck":
						prevEvent = this.historic.GetLastIsDeck();
						if (prevEvent)
							this.isThreeStatesDeck = prevEvent.target;
						else
							this.isThreeStatesDeck = false;
						dom.UpdateDeckView(this.isThreeStatesDeck);
						dom.SetRadioToValue("threeStatesDeck", this.isThreeStatesDeck?1:0);
						break;
					case "showStatementFirst":
						prevEvent = this.historic.GetLastShowAllStatementsFirst();
						if (prevEvent)
							dom.SetCheckboxToValue("showStatementFirst", prevEvent.target);
						break;
					case "isSendButtonsAtExogen":
						prevEvent = this.historic.GetLastIsSendButtonsAtExogen();
						if (prevEvent)
							dom.SetCheckboxToValue("isSendButtonsAtExogen", prevEvent.target);
						this.isSendButtonsAtExogen = prevEvent.target;
						break; 
				}
				break;
			case 'delete':
				switch (this.historic.historicalEvents[this.historic.currentHistoricalIndex].type)
				{
					case "statement":
						this.statements[this.historic.historicalEvents[this.historic.currentHistoricalIndex].target.id] = this.historic.historicalEvents[this.historic.currentHistoricalIndex].target;
						dom.GenerateCard(this.historic.historicalEvents[this.historic.currentHistoricalIndex].target);
						break;
					case "threeStates ":
						this.threeStates = this.historic.historicalEvents[this.historic.currentHistoricalIndex].target;
						dom.GenerateThreeStates(this.threeStates, dom.GetThreeStatesCheckFunctor(), dom.GetThreeStatesInsertToTitleCellFunctor(), dom.GetThreeStatesInsertFunctor());
						break;
					case "qsort":
						this.qSort = this.historic.historicalEvents[this.historic.currentHistoricalIndex].target;
						dom.GenerateQSort(this.isCrescent, this.statAlign, this.qSort, this.statements.length, dom.GetQSortCheckFunctor(), dom.GetQSortInsertToTitleCellFunctor(), dom.GetQSortInsertFunctor());
						dom.SetQSortInputValue();
						break;
					case "exoquizz":
						this.exoQuizz = this.historic.historicalEvents[this.historic.currentHistoricalIndex].target;
						dom.GenerateExoView();
						break;
					case "texts":
						this.texts = this.historic.historicalEvents[this.historic.currentHistoricalIndex].target;
						dom.GenerateWallOfTexts();
						break;
				}
				break;
			case 'reset':
				switch (this.historic.historicalEvents[this.historic.currentHistoricalIndex].type) {
					case "statement":
						this.statements = this.historic.historicalEvents[this.historic.currentHistoricalIndex].target;
						dom.GenerateCards();
				}
				break;
		}
	}

	Redo(dom: QAdminGenerator)
	{
		if (this.historic.historicalEvents.length < this.historic.currentHistoricalIndex + 1)
			return;
		switch (this.historic.historicalEvents[this.historic.currentHistoricalIndex].what)
		{
			case 'create':
				switch (this.historic.historicalEvents[this.historic.currentHistoricalIndex].type)
				{
					case "statement":
						this.statements[this.historic.historicalEvents[this.historic.currentHistoricalIndex].target.id] = this.historic.historicalEvents[this.historic.currentHistoricalIndex].target;
						dom.GenerateCard(this.historic.historicalEvents[this.historic.currentHistoricalIndex].target);
						break;
					case "threeStates":
						this.threeStates = this.historic.historicalEvents[this.historic.currentHistoricalIndex].target;
						dom.GenerateThreeStates(this.threeStates , dom.GetThreeStatesCheckFunctor(), dom.GetThreeStatesInsertToTitleCellFunctor(), dom.GetThreeStatesInsertFunctor());
						break;
					case "qsort":
						this.qSort = <Tabs>this.historic.historicalEvents[this.historic.currentHistoricalIndex].target;
						dom.GenerateQSort(this.isCrescent, this.statAlign, this.qSort, this.statements.length, dom.GetQSortCheckFunctor(), dom.GetQSortInsertToTitleCellFunctor(), dom.GetQSortInsertFunctor());
						dom.SetQSortInputValue();
						break;
					case "texts":
						this.texts = this.historic.historicalEvents[this.historic.currentHistoricalIndex].target;
						dom.GenerateWallOfTexts();
						break;
				}
				break;
			case 'set':
				switch (this.historic.historicalEvents[this.historic.currentHistoricalIndex].type)
				{
					case "statement":
						this.statements[this.historic.historicalEvents[this.historic.currentHistoricalIndex].target.id] = this.historic.historicalEvents[this.historic.currentHistoricalIndex].target;
						dom.ReloadCard(this.historic.historicalEvents[this.historic.currentHistoricalIndex].target.id);
						break;
					case "threeStates":
						this.threeStates = this.historic.historicalEvents[this.historic.currentHistoricalIndex].target;
						dom.GenerateThreeStates(this.threeStates , dom.GetThreeStatesCheckFunctor(), dom.GetThreeStatesInsertToTitleCellFunctor(), dom.GetThreeStatesInsertFunctor());
						break;
					case "qsort":
						this.qSort = <Tabs>this.historic.historicalEvents[this.historic.currentHistoricalIndex].target;
						dom.GenerateQSort(this.isCrescent, this.statAlign, this.qSort, this.statements.length, dom.GetQSortCheckFunctor(), dom.GetQSortInsertToTitleCellFunctor(), dom.GetQSortInsertFunctor());
						dom.SetQSortInputValue();
						break;
					case "isCrescent":
						this.isCrescent = this.historic.historicalEvents[this.historic.currentHistoricalIndex].target;
						dom.GenerateQSort(this.isCrescent, this.statAlign, this.qSort, this.statements.length, dom.GetQSortCheckFunctor(), dom.GetQSortInsertToTitleCellFunctor(), dom.GetQSortInsertFunctor());
						dom.SetRadioToValue("headerPosition", this.isCrescent?1:0);
						break;
					case "statAlign":
						this.statAlign = this.historic.historicalEvents[this.historic.currentHistoricalIndex].target;
						dom.GenerateQSort(this.isCrescent, this.statAlign, this.qSort, this.statements.length, dom.GetQSortCheckFunctor(), dom.GetQSortInsertToTitleCellFunctor(), dom.GetQSortInsertFunctor());
						dom.SetRadioToValue("alignementType", this.statAlign);
						break;
					case "interview":
						this.interviewType = this.historic.historicalEvents[this.historic.currentHistoricalIndex].target.type;
						this.interviewNumber = this.historic.historicalEvents[this.historic.currentHistoricalIndex].target.value;
						this.SetInterviewState();
						break;
					case "exoQuizz":
						this.exoQuizz = this.historic.historicalEvents[this.historic.currentHistoricalIndex].target;
						dom.GenerateExoView();
						break;
					case "texts":
						dom.SetTextValue(this.historic.historicalEvents[this.historic.currentHistoricalIndex].target.id, this.historic.historicalEvents[this.historic.currentHistoricalIndex].target.value);
						break;
					case "activeInterview":
						this.isActive.interview = this.historic.historicalEvents[this.historic.currentHistoricalIndex].target;
						dom.ShowHideInterview();
						break;
					case "activeExogen":
						this.isActive.exogen = this.historic.historicalEvents[this.historic.currentHistoricalIndex].target;
						dom.ShowHideExogen();
						break;
					case "threeStatesDeck":
						this.isThreeStatesDeck = this.historic.historicalEvents[this.historic.currentHistoricalIndex].target;
						dom.UpdateDeckView(this.isThreeStatesDeck);
						dom.SetRadioToValue("threeStatesDeck", this.isThreeStatesDeck?1:0);
						break; 
					case "showStatementFirst":
						dom.SetCheckboxToValue("showStatementFirst", this.historic.historicalEvents[this.historic.currentHistoricalIndex].target);
						break;
					case "isSendButtonsAtExogen":
						dom.SetCheckboxToValue("isSendButtonsAtExogen", this.historic.historicalEvents[this.historic.currentHistoricalIndex].target);
						this.isSendButtonsAtExogen = this.historic.historicalEvents[this.historic.currentHistoricalIndex].target;
						break;
				}
				break;
			case 'delete':
				switch (this.historic.historicalEvents[this.historic.currentHistoricalIndex].type)
				{
					case "statement":
						dom.DeleteCard(this.historic.historicalEvents[this.historic.currentHistoricalIndex].target.id);
						break;
					case "threeStates ":
						this.threeStates = new Array<Tab>();
						dom.DeleteThreeStates();
						break;
					case "qsort":
						this.qSort = new Array<Tab>(); 
						dom.DeleteQSort();
						break;
					case "exoQuizz":
						this.exoQuizz = new Array<Exogen>();
						dom.DeleteExoView();
						break;
					case "texts":
						dom.DeleteWallOfTexts();
						break;
				}
				break;
			case 'reset':
				switch (this.historic.historicalEvents[this.historic.currentHistoricalIndex].type)
				{
					case "statement":
						this.statements = new Array<Statement>();
						dom.CleanNodeByName("pickingZone");
						break;
				}
				break;
			default:
				return;
		}
		this.historic.currentHistoricalIndex++;
	}

	GenerateMediaFullPath(file: File): string
	{
		if (file.name)
			return 'data/' + file.name;
		return '';
	}

	SetTextStatement(id: number, statement: string)
	{
		this.statements[id].value = statement;
		this.historic.StoreInHistoric('statement', 'set', this.statements[id]);
	}

	SetImageStatement(id: number, file: File, description?: string)
	{
		this.statements[id].value = this.GenerateMediaFullPath(file);
		if (description)
			this.statements[id].description = description;
		this.historic.StoreInHistoric('statement', 'set', this.statements[id]);
	}

	SetMediaStatement(id: number, file: File, type: number, description?: string)
	{
		let name: string = this.GenerateMediaFullPath(file);
		this.statements[id].value = name;
		if (description)
		{
			if (type === 2)
				this.statements[id].illustration = description;
			else
				this.statements[id].description = description;
		}
		this.historic.StoreInHistoric('statement', 'set', this.statements[id]);
	}

	SetIllustration(id: number, file: File)
	{
		this.statements[id].illustration = this.GenerateMediaFullPath(file);
		this.historic.StoreInHistoric('statement', 'set', this.statements[id]);
	}

	SetAudioStatement(id: number, file: File, type: number, description?: string)
	{
		switch (type) {
			case 0:
			case 1:
				this.SetMediaStatement(id, file, 1, description);
				break;
			case 2:
				this.SetMediaStatement(id, file, 2, description);
				break;
			default:
		}
	}

	SetVideoStatement(id: number, file: File)
	{
		this.SetMediaStatement(id, file, 0);
	}

	SetStatementDescription(id: number, description?: string)
	{
		if (description)
			this.statements[id].description = description;
		this.historic.StoreInHistoric('statement', 'set', this.statements[id]);
	}

	SetThreeStates()
	{
		this.historic.StoreInHistoric('threeStates ', 'set', this.threeStates );
	}

	SetQSort()
	{
		this.historic.StoreInHistoric('qsort', 'set', this.qSort);
	}

	SetExoQuizz()
	{
		this.historic.StoreInHistoric('exoQuizz', 'set', this.exoQuizz);
	}

	SetInterviewState()
	{
		this.historic.StoreInHistoric('interview', 'set', <InterviewType>{ type: this.interviewType, number: this.interviewNumber });
	}

	GetTextValue(id: string): Text
	{
		for (let iterator of this.texts)
			if (iterator.id === id)
				return iterator;
		return null;
	}

	SetTextValue(id: string, value: string)
	{
		for (let iterator of this.texts)
		{
			if (iterator.id === id)
			{ 
				iterator.value = value;
				return;
			}
		}
		this.texts.push(<Text>{ id: id, value: value });
	}

	SetText(id: string, value: string)
	{
		this.SetTextValue(id, value);
		this.historic.StoreInHistoric("texts", 'set', <Text>{ id: id, value: value });
	}

	ProcessTXT(content: string, dom: QAdminGenerator)
	{
		let array = content.split("\n");
		for (let iterator = 0; iterator < array.length; iterator++)
			if (array[iterator].length > 3)
				dom.GenerateCard(this.GenerateStatementObject(0, array[iterator]));
	}

	ResetStatementObject(id: number)
	{
		let object: Statement = this.statements[id];
		switch (object.type) {
			case "text":
				object.value = this.configuration.defaultStatementText;
				break;
			case "img":
				object.value = "";
				if (object.description)
					delete object.description;
				break;
			case "audio":
				object.value = "";
				if (object.description)
					delete object.description;
				if (object.illustration)
					object.illustration = "";
				break;
			case "video":
				object.value = "";
				break;
			default:
				return;
		}
	}

	GenerateStatementObject(type: number, value?: string, description?: string): Statement
	{
		let object: Statement = { id: this.globalCounter,type:"",value:""};
		switch (type)
		{
			case 0:
				object.type = "text";
				object.value = value ? value : this.configuration.defaultStatementText;
				break;
			case 1:
				object.type = "img";
				object.value = value ? value : "";
				if (description)
					object.description = description;
				break;
			case 2:
				object.type = "audio";
				object.value = value ? value : "";
				if (description)
					object.description = description;
				break;
			case 3:
				object.type = "video";
				object.value = value ? value : "";
				break;
			case 4:
				object.type = "audio";
				object.value = value ? value : "";
				object.illustration = description ? description : "";
				break;
			default:
				return;
		}
		this.statements[this.globalCounter] = object;
		this.globalCounter++;
		return object;
	}

	GenerateThreeStates()
	{
		if (this.threeStates.length)
			return;
		this.threeStates = this.configuration.threeStates;
	}

	ProcessCSV(content: string, delimiter: string, dom: QAdminGenerator)
	{
		let array = this.ParseCSV(content, delimiter);
		let stat: Statement;
		for (let iterator = 0; iterator < array.length; iterator++) {
			switch (array[iterator].type) {
				case "text":
					stat = this.GenerateStatementObject(0, array[iterator].statement);
					break;
				case "img":
					stat = this.GenerateStatementObject(1, array[iterator].statement, array[iterator].description);
					break;
				case "video":
					stat = this.GenerateStatementObject(3, array[iterator].statement, array[iterator].description);
					break;
				case "audio":
					if ('illustration' in array[iterator])
						stat = this.GenerateStatementObject(4, array[iterator].statement, array[iterator].illustration);
					else
						stat = this.GenerateStatementObject(2, array[iterator].statement, array[iterator].description);
					break;
			}
			dom.GenerateCard(stat);
		}
	}

	ParseCSV(content: string, delimiter: string)
	{
		try {
			let array = content.split("\n");
			let head = array[0].split(delimiter);
			let objects = [];
			for (let iterator = 1; iterator < array.length; iterator++) {
				var object = {};
				let values = array[iterator].split(delimiter);
				for (let headIterator = 0; headIterator < head.length; headIterator++)
					object[head[headIterator]] = values[headIterator];
				objects.push(object);
			}
			return objects;
		}
		catch (event) {
			alert(event);
			return null;
		}
	}

	GetStatement(id: number): Statement
	{
		if (id in this.statements)
			return this.statements[id];
		return null;
	}

	DeleteStatement(id: number)
	{
		if (id in this.statements) {
			this.historic.StoreInHistoric("statement", "delete", this.statements[id]);
			delete this.statements[id];
		}
	}

	GetExoQuizz(id: string): Exogen
	{
		if (id in this.exoQuizz)
			return this.exoQuizz[id];
		return null;
	}

	GetExoQuizzParameter(quizz: Exogen, id: string): ParameterValue
	{
		if (!quizz.parameters)
			return null;
		for (let param of quizz.parameters)
			if (param.name === id)
				return param;
		return null;
	}

	DeleteExoQuizz(id: number)
	{
		delete this.exoQuizz[id];
		this.ProcessExotable(document.getElementById("exoTable"), id);
	}

	ProcessConfig(dom: QAdminGenerator, data: string)
	{
		try {
			this.configurationFromFile = FileManager.JSONParse(data);
			this.statements = this.configurationFromFile.statements;

			if (this.configurationFromFile.threeStates && this.configurationFromFile.threeStates.length)
			{
				this.threeStates  = this.configurationFromFile.threeStates;
				dom.GenerateThreeStates(this.threeStates, dom.GetThreeStatesCheckFunctor(), dom.GetThreeStatesInsertToTitleCellFunctor(), dom.GetThreeStatesInsertFunctor());
			}
			if (this.configurationFromFile.dynamicStatementNumber)
			{
				this.interviewNumber = this.configurationFromFile.dynamicStatementNumber;
				if (this.interviewNumber != 0)
						this.interviewType = this.configurationFromFile.dynamicType;
			}
			dom.SetInterview();
			this.qSort = this.configurationFromFile.qSortConfiguration.qSort;
			this.isCrescent = this.configurationFromFile.qSortConfiguration.isCrescent === "true";
			this.statAlign = getAlignementType(this.configurationFromFile.qSortConfiguration.statementsAlignement);
			dom.GenerateQSort(this.isCrescent, this.statAlign, this.qSort, this.statements.length, dom.GetQSortCheckFunctor(), dom.GetQSortInsertToTitleCellFunctor(), dom.GetQSortInsertFunctor());
			for (let iterator of this.configuration.texts)
			{
				if (this.configurationFromFile[iterator.id])
					this.SetTextValue(iterator.id, this.configurationFromFile[iterator.id]);
				else if (iterator.default)
					this.SetTextValue(iterator.id, iterator.default);
			}
			dom.GenerateWallOfTexts();
			dom.GenerateCards();
			if (this.configurationFromFile.exoQuizz)
			{ 
				let tempExoQuizz = this.configurationFromFile.exoQuizz;
				for (let exoJson of tempExoQuizz)
				{
					let exogen: Exogen = { id: exoJson.id, type: exoJson.type, text: exoJson.text };
					if (exoJson.values && exoJson.values.length)
						exogen.values = exoJson.values;
					if (exoJson.parameters)
					{
						exogen.parameters = new Set<ParameterValue>();
						for (let value of Object.entries(exoJson.parameters))
						{
							let obj: any = <any>value[1];
							exogen.parameters.add({ name: obj.name, value: obj.value });
						}
					}
					this.exoQuizz.push(exogen);
				}
				dom.GenerateExoView();
			}
			if (this.configurationFromFile.showAllStatementsFirst)
				dom.SetCheckboxToValue("showStatementFirst", this.configurationFromFile.showAllStatementsFirst);

			if (this.configurationFromFile.isTraceActive)
				dom.SetCheckboxToValue("isTraceActive", this.configurationFromFile.isTraceActive);
		}
		catch (error) {
			console.log(error);
			alert(this.configuration.wrongConfigurationFile);
		}
	}

	ProcessConfigFile(file: File, dom: QAdminGenerator)
	{
		FileManager.ReadFile(file, function (dom: QAdminGenerator) { return function (text) { dom.processor.ProcessConfig(dom, text); }; }(dom));
	}

	ProcessFileList(list: any, dom: QAdminGenerator)
	{
		if (typeof list === "object")
		{
			for (let iterator = 0; iterator < list.length; iterator++)
				this.ProcessFileList(list[iterator], dom);
		}
		else (typeof list === "string") 
		{
			let fileType: string = list.name.substring(list.name.lastIndexOf(".") + 1).toLowerCase();
			switch (fileType) {
				case "sta":
				case "txt":
					let txtReader: FileReader = new FileReader();
					txtReader.onload = function (dom: QAdminGenerator) { return function (event: any) { dom.processor.ProcessTXT(event.target.result, dom); }; }(dom);
					txtReader.readAsText(list);
					break;
				case "csv":
					let csvReader: FileReader  = new FileReader();
					csvReader.onload = function (dom: QAdminGenerator) { return function (event: any) { dom.processor.ProcessCSV(event.target.result, ";", dom); };}(dom);
					csvReader.readAsText(list);
					break;
				case "png":
				case "bmp":
				case "jpg":
				case "jpeg":
					let imgReader: FileReader = new FileReader();
					imgReader.onload = function (dom: QAdminGenerator, file: File, id: number) { return function (event: any) { dom.processor.mediacontents[dom.processor.GenerateMediaFullPath(file)] = event.target.result; dom.processor.SetImageStatement(id, file); dom.ReloadCard(id); }; }(dom, list, this.globalCounter);
					dom.GenerateEmptyCard(this.globalCounter);
					imgReader.readAsDataURL(list);
					break;
				case "mp3":
					let audioReader: FileReader = new FileReader();
					audioReader.onload = function (dom: QAdminGenerator, file: File, id: number) { return function (event: any) { dom.processor.mediacontents[dom.processor.GenerateMediaFullPath(file)] = event.target.result; dom.processor.SetAudioStatement(id, file, 0); dom.ReloadCard(id) }; }(dom, list, this.globalCounter);
					dom.GenerateEmptyCard(this.globalCounter);
					audioReader.readAsDataURL(list);
					break;
				case "mov":
				case "mp4":
					let videoReader: FileReader  = new FileReader();
					videoReader.onload = function (dom: QAdminGenerator, file: File, id: number) { return function (event: any) { dom.processor.mediacontents[dom.processor.GenerateMediaFullPath(file)] = event.target.result; dom.processor.SetVideoStatement(id, file); dom.ReloadCard(id) }; }(dom, list, this.globalCounter);
					dom.GenerateEmptyCard(this.globalCounter);
					videoReader.readAsDataURL(list);
					break;
			}
		}
	}

	ProcessImageFile(dom: QAdminGenerator, file: File, id: number)
	{
		let reader: FileReader = new FileReader();
		if (this.statements[id].type === "img")
			reader.onload = function (dom: QAdminGenerator, node: number, file: File) { return function (event: any) { dom.processor.mediacontents[dom.processor.GenerateMediaFullPath(file)] = event.target.result; dom.processor.SetImageStatement(node, file); dom.ReloadCard(node); }; }(dom, id, file);
		else
			reader.onload = function (dom: QAdminGenerator, node: number, file: File) { return function (event: any) { dom.processor.mediacontents[dom.processor.GenerateMediaFullPath(file)] = event.target.result; dom.processor.SetIllustration(node, file); dom.ReloadCard(node); }; }(dom, id, file);
		reader.readAsDataURL(file);
	}

	ProcessAudioFile(dom: QAdminGenerator, file: File, id: number)
	{
		let reader: FileReader  = new FileReader();
		reader.onload = function (dom: QAdminGenerator, node: number, file: File) { return function (event: any) { dom.processor.mediacontents[dom.processor.GenerateMediaFullPath(file)] = event.target.result; dom.processor.SetAudioStatement(node, file, 0); dom.ReloadCard(node); }; }(dom, id, file);
		reader.readAsDataURL(file);
	}

	ProcessVideoFile(dom: QAdminGenerator, file: File, id: number)
	{
		let reader: FileReader  = new FileReader();
		reader.onload = function (dom: QAdminGenerator, node: number, file: File) { return function (event: any) { dom.processor.mediacontents[dom.processor.GenerateMediaFullPath(file)] = event.target.result; dom.processor.SetVideoStatement(node, file); dom.ReloadCard(node); }; }(dom, id, file);
		reader.readAsDataURL(file);
	}

	ProcessExotable(table, joker)
	{
		this.exoQuizz = new Array<Exogen>();
		for (let row = 0; row < table.rows.length; row++)
		{
			if (row === Number.parseInt(joker))
				continue;
			let object: Exogen = { id: "-1", type: "none" };
			for (let element in table.rows[row].cells[0].childNodes)
				if (table.rows[row].cells[0].childNodes[element].id)
					object[table.rows[row].cells[0].childNodes[element].id] = table.rows[row].cells[0].childNodes[element].childNodes[1].value;
			for (let element in table.rows[row].cells[1].childNodes)
			{
				if (table.rows[row].cells[1].childNodes[element].id)
				{
					let value: ParameterValue = { name:"", value:"" };
					switch (table.rows[row].cells[1].childNodes[element].id)
					{
						case "values":
							for (let iterator = 0; iterator < table.rows[row].cells[1].childNodes[element].childNodes.length; iterator++)
							{
								if (!object.values)
									object.values = [];
								object.values.push(table.rows[row].cells[1].childNodes[element].childNodes[iterator].value);
							}
							break;
						case "isRequired":
							value.name = table.rows[row].cells[1].childNodes[element].id;
							value.value = table.rows[row].cells[1].childNodes[element].childNodes[1].checked;
							if (!object.parameters)
								object.parameters = new Set<ParameterValue>();
							object.parameters.add(value);
							break;
						case "min":
						case "max":
						case "step":
						case "max-length":
							value.name = table.rows[row].cells[1].childNodes[element].id;
							value.value = table.rows[row].cells[1].childNodes[element].childNodes[1].value;
							if (!object.parameters)
								object.parameters = new Set<ParameterValue>();
							object.parameters.add(value);
							break;
						default:
							for (let iterator = 0; iterator < table.rows[row].cells[1].childNodes[element].childNodes.length; iterator++)
							{
								let elem = table.rows[row].cells[1].childNodes[element].childNodes[iterator];
								value.name = elem.data;
								value.value = elem.data;
								if (!object.parameters)
									object.parameters = new Set<ParameterValue>();
								object.parameters.add(value);
							}
							break;
					}
				}
			}
			this.exoQuizz.push(object);
		}
	}

	CleanStatementsMap(): void
	{
		if (!this.statements.length)
			return;
		let count: number = 0;
		let newStatements = new Array<Statement>();
		for (let key in this.statements)
		{
			let statement: Statement = this.statements[key];
			statement.id = count++;
			newStatements.push(statement);
		}
		this.statements = newStatements;
		this.globalCounter = this.statements.length;
	}

	GenerateStatementsFile(): boolean
	{
		this.CleanStatementsMap();
		if (!this.statements.length)
			return false;
		let staFile: string = '';
		for (let key in this.statements)
		{
			staFile += key + "." + this.statements[key].value 
			switch (this.statements[key].type)
			{
				case "audio":
					if ('illustration' in this.statements[key])
					{
						staFile += " - " + this.statements[key].illustration;
						break;
					}
				case "img":
					if ('descirption' in this.statements[key])
						staFile += " - " + this.statements[key].description;
					break;
			}
			staFile +=  "\n";
		}
		FileManager.GenerateFile("statements.sta", staFile);
		return true;
	}

	GenerateConfig(autoSave?: boolean): string
	{
		let sum: number = 0;
		let qSortLength: number = 0;
		for (let sort in this.qSort)
		{
			sum += this.qSort[sort].limit;
			qSortLength++;
		}
		if (!qSortLength) {
			if (!autoSave)
				alert(this.configuration.needQsort);
		}
		if (sum && this.statements.length !== sum)
		{
			alert(sum + "!=" + this.statements.length);
			qSortLength = 0;
			if (!autoSave)
				alert(this.configuration.needEqualStatementsNumber);
		}
		if (!this.texts.length)
		{
			if (!autoSave)
				alert(this.configuration.needTexts)
		}
		let json: string = '{ "serverUrlSubmit":"submit.php","opacityAnimationInterval": 100,"opacityAnimationDuration": 1000,"scalingAnimationInterval": 10,"scalingAnimationDuration": 0,"scalingAnimationMaxValue": 3,"scalingAnimationMinValue": 0.65,"dragAndDropClone":true';
		sum = 0;
		if (qSortLength > 0) {
			json += ',"qSortConfiguration":{"qSort":{'
			for (let key in this.qSort) {
				json += '"' + key + '":' + JSON.stringify(this.qSort[key]);
				if (sum < qSortLength - 1)
					json += ",";
				sum++;
			}
			json += '},"statementsAlignement":' + getAlignementTypeText(this.statAlign);
			json += ',"isCrescent":"';
			if (this.isCrescent)
				json += 'true"}';
			else
				json += 'false"}';
		}
		json += ',"statements":[';
		this.CleanStatementsMap();
		for (let iterator in this.statements)
		{
			json += JSON.stringify(this.statements[iterator]);
			if (Number.parseInt(iterator) < this.statements.length-1)
				json += ',';
		}
		json += '],';
		json += '"showStatementsFirst":';
		if ((<any>document.getElementById("showStatementFirst")).checked)
			json += 'true';
		else
			json += 'false';
		json += ',"isTraceActive":';
		if ((<any>document.getElementById("isTraceActive")).checked)
			json += 'true';
		else
			json += 'false';
		if (this.threeStates.length)
		{
			json += ',"isThreeStatesDeck":' + this.isThreeStatesDeck;
			json += ',"threeStates":[';
			for (let iterator in this.threeStates)
			{
				json += '{ "type": "' + this.threeStates[iterator].type + '", "title":"' + this.threeStates[iterator].title + '"}';
				if (Number.parseInt(iterator) < this.threeStates.length - 1)
					json +=",";
			}
			json += ']';
		}
		for (let text of this.texts)
			json += ',"' + text.id + '":"' + text.value.replace(/\"/g, "\\\"").replace(/\n/g, "<br>").replace(/\r/g, "") + '"';
		if (this.isActive.interview)
		{
			json += ',"isInterviewActive":"true"';
			json += ',"dynamicType":' + this.interviewType;
			json += ',"dynamicStatementNumber":' + this.interviewNumber;
		}
		else
			json += ',"isInterviewActive":"false"';
		let maxitem = this.exoQuizz.length -1;
		if (this.isActive.exogen && maxitem !== -1)
		{
			json += ',"isExogenActive":true';
			json += ',"isSendButtonsAtExogen":' + this.isSendButtonsAtExogen;
			json += ',"exoQuizz":[';
			let iterator = 0;
			for (let exo of this.exoQuizz)
			{
				iterator++;
				json += '{ "id":"' + exo.id + '","type":"' + exo.type + '", "text":"' + exo.text + '"';
				if ('values' in exo && exo.values.length)
					json += ',"values":' + JSON.stringify(exo.values);
				json += ',"parameters":[';
				sum = 0;
				if ('parameters' in exo)
				{ 
					for (let parameter of exo.parameters)
					{
						json += '{"name":"' + parameter.name + '","value":"' + parameter.value + '"}';
						if (sum < exo.parameters.size-1)
							json += ",";
						sum++;
					}
				}
				json += "]}";
				if (iterator <= maxitem)
					json += ",";
			}
			json += "]";
		}
		else
			json += ',"isExogenActive":"false"';
		json += "}";
		return json;
	}

	GenerateConfigFile(): boolean
	{
		/*if(!Object.keys(this.statements).length||Object.keys(this.texts).length!==this.configuration.texts.length)
		{
			alert(configuration.needAllTexts);
			return false;
		}*/
		let json: string = this.GenerateConfig()
		if (!json)
			return false;
		FileManager.GenerateFile("config", json);
		return true;
	}

	SaveConfig(autoSave): boolean
	{
		let json: string = this.GenerateConfig(autoSave)
		if (!json)
			return false;
		return this.historic.SaveInStorage(json);
	}

	LoadConfig(dom: QAdminGenerator)
	{
		let olderData: string = this.historic.GetSavedData();
		if (!olderData || olderData === "undefined") {
			alert(this.configuration.noDataSaved);
			return;
		}
		this.ProcessConfig(dom, olderData);
	}
}