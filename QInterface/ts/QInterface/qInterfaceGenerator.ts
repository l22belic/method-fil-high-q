import { DOMGenerator, getAlignementType } from "../Global/domGenerator"
import { QInterfaceProcessor } from "./qInterfaceProcessor"
import { DragAndDropManager } from "../Global/ddManager"
import { ResetReferenceTime, QInterfaceStorage } from "./qInterfaceStorage";
import { FileManager } from "../Global/fileManager"
import { Sort, Statement, Tab } from "../Global/types"

/**
 * Basic interface & type
 */

interface IBaseCardValue
{
	w: number;
	h: number;
}

type BaseCardValue = IBaseCardValue;

/*
function lambdaStart(manager: any)
{
	return function () {
		ResetReferenceTime();
		manager.processor.storage.ClearStorage();
		manager.processor.StoreNextStepEvent();

		if (manager.processor.configuration.presentationText) {
			manager.GenerateStepPage(this.processor.configuration.presentationText, this.processor.configuration.ne, function (manager: QInterfaceGenerator) {
				return function () {
					manager.GenerateCards();
					manager.ChangeCardsScale(1.0);
					manager.GenerateZoomSlider();
					manager.processor.StoreNextStepEvent(); manager.GenerateThreeStates(manager.processor.configuration.threeStates, manager.GetThreeStatesCheckFunctor(), manager.GetThreeStatesInsertToTitleCellFunctor(), manager.GetThreeStatesInsertFunctor());
				};
			}(manager));


		}
		else {
			manager.GenerateCards();
			manager.ChangeCardsScale(1.0);
			manager.GenerateZoomSlider();
			if (!manager.processor.configuration.showStatementsFirst) {
				if (manager.processor.configuration.threeStates) {
					manager.processor.StoreNextStepEvent();
					manager.GenerateStepPage(manager.processor.configuration.threeStatesText,
						manager.processor.configuration.startButton,
						function (manager: QInterfaceGenerator) { return function () { manager.processor.StoreNextStepEvent(); manager.GenerateThreeStates(manager.processor.configuration.threeStates, manager.GetThreeStatesCheckFunctor(), manager.GetThreeStatesInsertToTitleCellFunctor(), manager.GetThreeStatesInsertFunctor()); }; }(manager), ["pickingZone"]);
				}
				else {
					manager.processor.StoreNextStepEvent();
					manager.GenerateStepPage(manager.processor.configuration.qSortText,
						manager.processor.configuration.startButton,
						function (manager: QInterfaceGenerator) { return function () { manager.processor.StoreNextStepEvent(); manager.GenerateQSort(manager.processor.configuration.qSortConfiguration.isCrescent, getAlignementType(manager.processor.configuration.qSortConfiguration.statementsAlignement), manager.processor.configuration.qSortConfiguration.qSort, manager.processor.configuration.statements.length, manager.GetQSortCheckFunctor("pickingZone"), manager.GetQSortInsertToTitleCellFunctor(), manager.GetQSortInsertFunctor()); }; }(manager), ["pickingZone"]);
				}
			}
		}

	};
} (this), ['sig'], true);
}*/

/**
 * IHM
 */

export class QInterfaceGenerator extends DOMGenerator 
{
	processor: QInterfaceProcessor;
	dragAndDropManager: DragAndDropManager;
	baseCardValue: BaseCardValue;
	timeFactor: number;
	scaleZoom: number;
	scaledNode: HTMLElement;

	constructor()
	{
		super();
		this.baseCardValue = {w: 0, h: 0};
		this.timeFactor = 1;
		this.scaleZoom = 1;
    }

	AppendChildList(nodeid: string, list: Array<string>, animation: boolean)
	{
		let container: HTMLElement = document.getElementById(nodeid);
		if(container)
		{
			for(let iterator:number=0; iterator<list.length;iterator++)
			{
				let card=document.getElementById(list[iterator]);
				//card["location"]=nodeid;
				container.appendChild(card);
				if(animation)
					this.CountCardMovement(list[iterator],nodeid);
			}
		}
	}

	GetNewOrder(elementid: string)
	{
		this.processor.StoreNewOrder(elementid, this);
	}

	RegisterParents(parentID: string, grandParentID: string, node: HTMLElement)
	{
		node["stateScale"]=1;
		node["originState"]=parentID;
		node["location"]=grandParentID;
	}

	CheckState()
	{
		switch (this.processor.state)
		{
			case 3:
				return this.CheckPickingZoneEmpty();
			case 4:
				return this.CheckPickingZoneEmptyFinnish();
			case 6:
				return this.Check3StatesEmpty();
			case 7:
				return this.Check3StatesEmptyFinnish();
			case 1:
				return;
		}
		this.processor.storage.StoreEvent("errors", "StateValue", this.processor.state.toString());
		let zone: HTMLElement = document.getElementById("pickingZone");
		if (zone) {
			let agree: HTMLElement = document.getElementById("agree");
			let disagree: HTMLElement = document.getElementById("disagree");
			let neutral: HTMLElement = document.getElementById("neutral");
			if (agree.childNodes.length === 0 && disagree.childNodes.length === 0 && neutral.childNodes.length === 0) {
				this.processor.state = 4;
				return this.CheckPickingZoneEmptyFinnish();
			}
			else {
				this.processor.state = 3;
				return this.CheckPickingZoneEmpty();
			}
		}
		else
		{
			let agree: HTMLElement = document.getElementById("agree");
			let disagree: HTMLElement = document.getElementById("disagree");
			let neutral: HTMLElement = document.getElementById("neutral");
			if (agree.childNodes.length === 0 && disagree.childNodes.length === 0 && neutral.childNodes.length === 0) {
				this.processor.state = 7;
				return this.Check3StatesEmptyFinnish();
			}
			else {
				this.processor.state = 6;
				return this.Check3StatesEmpty();
			}
		}
	}

	CheckPickingZoneEmpty() 
	{
		let zone: HTMLElement = document.getElementById("pickingZone");
		if(this.processor.configuration.isThreeStatesDeck)
			this.UpdateDeckView(true);
		if(zone&&zone.childElementCount==0) 
		{
			this.processor.StoreNextStepEvent();
			if (this.processor.configuration.threeStates)
				this.AddContinueButton(
					function (manager: QInterfaceGenerator) {
						return function () {
							manager.processor.StoreThreeStatesValues(manager);
							manager.GenerateStepPage(manager.processor.configuration.qSortText, manager.processor.configuration.continueButton,
								function (manager: QInterfaceGenerator) {
									return function () {
										manager.processor.StoreNextStepEvent();
										manager.ChangeCardsScale(1);
										manager.GenerateQSort(manager.processor.configuration.qSortConfiguration.isCrescent,
											getAlignementType(manager.processor.configuration.qSortConfiguration.statementsAlignement),
											manager.processor.configuration.qSortConfiguration.qSort,
											manager.processor.configuration.statements.length,
											manager.GetQSortCheckFunctor("threeStates"),
											manager.GetQSortInsertToTitleCellFunctor(),
											manager.GetQSortInsertFunctor());
										manager.dragAndDropManager.Start();
									};
								}(manager), ["threeStates"]);
							manager.processor.StoreNextStepEvent();
						};
					}(this));
			else
				this.AddContinueButton(
					function (manager: QInterfaceGenerator) {
						return function () {
							manager.processor.StoreQSortValues(manager);
							manager.processor.StoreNextStepEvent();
							manager.GenerateStepPage(manager.processor.configuration.quizzText, manager.processor.configuration.continueButton,
								function (manager: QInterfaceGenerator) {
									return function () {
										manager.dragAndDropManager.Stop();
										manager.GeneratePostInterview();
										manager.processor.StoreNextStepEvent();
									};
								}(manager));
						};
					}(this));
		}
		return true;
	}

	CheckPickingZoneEmptyFinnish() 
	{
		let zone: HTMLElement = document.getElementById("pickingZone");
		let continueButton: HTMLButtonElement = <HTMLButtonElement>document.getElementById("button");
		if(zone&&zone.childElementCount==0) 
			continueButton.disabled=false;
		else
			continueButton.disabled=true;
		return true;
	}

	Check3StatesEmpty()
	{
		let agree: HTMLElement=document.getElementById("agree");
		let disagree: HTMLElement=document.getElementById("disagree");
		let neutral: HTMLElement=document.getElementById("neutral");
		if(agree&&disagree&&neutral&&agree.childNodes.length==0&&disagree.childNodes.length==0&&neutral.childNodes.length==0) 
		{
			this.processor.StoreNextStepEvent();
			this.AddContinueButton(function (manager: QInterfaceGenerator) {
				return function () {
					manager.processor.StoreQSortValues(manager);
					manager.processor.StoreNextStepEvent();
					manager.dragAndDropManager.Stop();
					manager.GenerateStepPage(manager.processor.configuration.quizzText, manager.processor.configuration.continueButton,
						function (manager: QInterfaceGenerator) {
							return function () {
								manager.GeneratePostInterview();
							};
						}(manager));
				};
			}(this));
			return true;
		}
		return true;
	}

	Check3StatesEmptyFinnish()
	{
		let agree: HTMLElement=document.getElementById("agree");
		let disagree: HTMLElement=document.getElementById("disagree");
		let neutral: HTMLElement=document.getElementById("neutral");
		let continueButton: HTMLButtonElement = <HTMLButtonElement>document.getElementById("button");
		if(agree&&agree.childElementCount==0&&disagree&&disagree.childElementCount==0&&neutral&&neutral.childElementCount==0) 
			continueButton.disabled=false;
		else
			continueButton.disabled=true;
		return true;
	}

	HideZoom(set: boolean)
	{
		let zoom: HTMLElement=document.getElementById("zoomcontainer");
		if(zoom)
		{
			if(set)
				zoom.style.display="none";
			else
				zoom.style.display="initial";
		}
	}

	AddContinueButton(fonctor: Function)
	{
		let button: HTMLButtonElement = document.createElement("button");
		button.id = "button";
		button.className = "nextStepButton";
		button.appendChild(document.createTextNode(this.processor.configuration.continueButton));
		button.onclick=function(){fonctor();};
		let main: HTMLElement=this.GetMain();
		//main.insertBefore(button,main.firstChild);
		main.appendChild(button);
	}

	GenerateStepPage(contentpage: string, buttontext: string, functor: Function, jokers?: Array<string>, stillDisplay?: boolean)
	{
		this.HideZoom(true);
		this.CleanMain(jokers, stillDisplay);
		let div: HTMLDivElement=document.createElement("div");
		div.className="presdiv";
		let text: HTMLDivElement=document.createElement("div");
		text.className="prestext noselect";
		text.innerHTML=contentpage;
		/*if(replayScale)
			text.style.fontSize=(replayScale*1.3)+"em";*/
		div.appendChild(text);
		let button: HTMLButtonElement =document.createElement("button");
		button.id="button";
		button.appendChild(document.createTextNode(buttontext));
		button.className = "noselect nextStepButton";
		button.onclick = function (dom: QInterfaceGenerator) { return function () { dom.HideZoom(false); functor(); }; }(this);
		div.appendChild(button);
		this.GetMain().appendChild(div);
	}

	GenerateZoomSlider()
	{
		let div: HTMLDivElement=document.createElement("div");
		div.appendChild(document.createTextNode(this.processor.configuration.zoomTitle));
		div.id="zoomcontainer";
		div.className="slidecontainer noselect";
		let slider = <any>document.createElement("input"); //TO DO better...
		slider.type = "range";
		slider.min=50;
		slider.max=200;
		slider.value=100;
		slider.className="slider";
		slider.id = "zoom";
		slider.oninput = function (manager: QInterfaceGenerator) {
			return function (event: any) {
				manager.UnscaleElement();
				manager.processor.storage.StoreEvent("zooming", "zoom", event.currentTarget.value);
				manager.ChangeCardsScale(event.currentTarget.value / 100.0); }; }(this);
		div.appendChild(slider);
		document.body.appendChild(div);
	}

	GeneratePostInterviewTable(labelText: string, list: Sort, id: string): HTMLElement
	{
		let div: HTMLDivElement=document.createElement("div");
		let label: HTMLDivElement=div.appendChild(document.createElement("div"));
		label.innerHTML=labelText;
		label.className = "tabletitle ";
		let table: HTMLTableElement = div.appendChild(document.createElement("table"));
		table.id = id;
		table.style.maxWidth = "99.5%";
		table.style.maxHeight = "20%";
		table.align="center";
		table.className = "nestable noselect";
		let iterator: number = 0;
		for (let value of list.value)
		{
			let row: HTMLTableRowElement = table.insertRow(iterator);
			let li: HTMLDivElement = document.createElement('div');
			li.appendChild(this.GenerateStatement(this.processor.GetStatement(parseInt(value))));
			li.className = "nested-item ";
			row.insertCell(0).appendChild(li);
			row.cells[0].className = "nestable grey";
			row.cells[0].width = "25%";
			let input: HTMLTextAreaElement = document.createElement("textarea");
			input.id = "text_" + value;
			input.name = value;
			/* For extend version
			input.onkeydown = function (storage: QInterfaceStorage) { return function (event) { storage.StoreKeyEvent(event) }; }(this.processor.storage);
			input.onkeyup = function (storage: QInterfaceStorage) { return function (event) { storage.StoreKeyEvent(event) }; }(this.processor.storage);
			input.onfocus = = function (storage: QInterfaceStorage) { return function (event) { storage.StoreFocusEvent(event) }; }(this.processor.storage);*/
			row.insertCell(1).appendChild(input);
			iterator++;
		}
		return div;
	}

	GeneratePostInterview()
	{
		let main: HTMLElement=this.GetMain();
		document.body.removeChild(document.getElementById("zoomcontainer"));
		this.CleanMain();
		main.appendChild(document.createElement("br"));
		if (this.processor.storage.qSort[this.processor.storage.qSort.length-1].value.size)
		{
			main.appendChild(this.GeneratePostInterviewTable(this.processor.configuration.disagreePostText, this.processor.storage.qSort[this.processor.storage.qSort.length-1],"disagree"));
			main.appendChild(document.createElement("br"));
		}
		if (this.processor.storage.qSort[0].value.size)
		{
			main.appendChild(this.GeneratePostInterviewTable(this.processor.configuration.agreePostText, this.processor.storage.qSort[0],"agree"));
			main.appendChild(document.createElement("br"));
		}
		this.processor.GetStatementForInterview();
		if (this.processor.cardsForInterview.value.size !== 0)
		{
			main.appendChild(this.GeneratePostInterviewTable(this.processor.configuration.dynamicPostText, this.processor.cardsForInterview,"dynamic"));
			main.appendChild(document.createElement("br"));
		}
		let button: HTMLButtonElement=document.createElement("button");
		button.appendChild(document.createTextNode(this.processor.configuration.continueButton));
		button.className = "noselect nextStepButton";
		button.id = "button";
		button.onclick = function (manager: QInterfaceGenerator) { return function () { manager.processor.StoreTextInterview(manager); manager.processor.StoreNextStepEvent(); if (manager.processor.configuration.exoQuizz) manager.GenerateExoQuizz(); else manager.GenerateFinnishPage(); }; }(this);
		main.appendChild(button);
		//this.ChangeCardsScale(this.processor.configuration.scalingAnimationMinValue);
	}

	GenerateFinnishPage()
	{
		let main: HTMLElement=this.GetMain();
		this.CleanMain();
		let div: HTMLDivElement=document.createElement("div");
		div.appendChild(document.createElement("br"));
		div.appendChild(document.createElement("br"));
		div.id="finish";
		let text: HTMLDivElement=document.createElement("div");
		text.innerHTML = this.processor.configuration.finishText;
		text.className="noselect";
		div.appendChild(text);
		div.appendChild(document.createElement("br"));
		div.appendChild(document.createElement("br"));
		div.appendChild(document.createElement("br"));
		div.appendChild(document.createElement("br"));
		div.appendChild(document.createElement("br"));

		this.GenerateNotSendSendButtons(div, null);
		main.appendChild(div);
	}

	GenerateNotSendSendButtons(div: HTMLElement, functor: Function)
	{
		let table: HTMLTableElement = div.appendChild(document.createElement("table"));
		let row: HTMLTableRowElement = table.insertRow();
		let cell: HTMLTableCellElement = row.insertCell();
		cell.style.width = "50%";
		cell.style.textAlign = "center";
		cell = row.insertCell();
		let button: HTMLButtonElement = cell.appendChild(document.createElement("button"));
		//button.id = "button";
		//button.className = "noselect";
		//button.appendChild(document.createTextNode(this.processor.configuration.notSendButton));
		//button.onclick = function (manager: QInterfaceGenerator) { return function () { manager.CleanMain(); manager.processor.GenerateStatementsList(); manager.GenerateStartPage(); }; }(this);
		//button.style.paddingRight = "20px;"
		//cell = row.insertCell();
		//cell.style.width = "50%";
		//cell.style.textAlign = "center";
		//button = cell.appendChild(document.createElement("button"));
		button.id = "button";
		button.className = "noselect";
		button.appendChild(document.createTextNode(this.processor.configuration.sendButton));
		button.onclick = function (manager: QInterfaceGenerator, functor: Function) {
			return function () {
				if (!functor || functor())
				FileManager.SubmitData(manager.processor.configuration.serverUrlSubmit, manager.processor.GenerateJSON(), function (res) {
					if (res.status != 200 && confirm(manager.processor.configuration.networkErrorText))
						FileManager.GenerateFile("data.json", manager.processor.GenerateJSON());
					else if (manager.processor.configuration.submissionOkText)
						alert(manager.processor.configuration.submissionOkText);
					else { manager.CleanMain(); manager.processor.GenerateStatementsList(); manager.GenerateStartPage(); }
				});
			};
		}(this, functor);
		button.style.paddingLeft = "20px;"
		button.style.transform = "scale(1.5)"; 
	}

	/*FindMissingQStateDiv(parentNode: HTMLElement): HTMLElement
	{
		for(let iterator=0;iterator<parentNode['childLimit'];iterator++)
		{
			if(!document.getElementById(div.id+'_'+iterator));
				return div.id+'_'+iterator;
		}
		return null;
	}

	AddQStateDiv(elementid: string)
	{
		let name=elementid.substring(0,elementid.lastIndexOf("_"));
		let div = <HTMLElement>document.getElementById(name);
		if(!div)
			div=document.getElementById(elementid);
		if(!div)
			return;
		let count = div.childElementCount;
		let firstChild = <HTMLElement>div.firstChild;
		if(firstChild.childElementCount>1)
			count+=firstChild.childElementCount-1;
		if(firstChild["childLimit"]>count)
			document.getElementById(elementid).appendChild(this.FindMissingQStateDiv(div));
	}
	
	static ChangeDropLocation(cardid,containerid)
	{
		if(state!=6&&state!=7)
			return;
		let container=QInterfaceGenerator.GetParentWithId(document.getElementById(containerid));
		let parentContainer=QInterfaceGenerator.GetParentWithId(container);
		let card=document.getElementById(cardid);
		let tempvalue=0;
		if(parentContainer.id!="qSort")
		{
			if(!("location"in card) || !card["location"] || card["location"]==="threeStates")
				return;
			QInterfaceGenerator.AddQStateDiv(card["location"]);
			card["location"]="threeStates";
		}
		else
		{
			if("location" in card && card["location"] && card["location"]!="threeStates")
				QInterfaceGenerator.AddQStateDiv(card["location"]);
			if(card.parentNode!=container.firstChild)
				container.firstChild.appendChild(document.getElementById(cardid));
			if((container.firstChild["childLimit"]+1)<(container.firstChild.childElementCount+container.childElementCount))
				container.removeChild(container.childNodes[container.childElementCount-1]);
			card["location"]=containerid;
		}
		QInterfaceGenerator.CheckMissingDrop();
	}*/

	CheckMissingDrop()
	{
		let divs: HTMLCollectionOf<Element> = document.getElementsByClassName("toCheck");
		for(let iterator:number=divs.length-1; iterator>0; iterator--)
		{
			if(divs[iterator].firstChild)
			{
				let parentContainer: HTMLElement=this.GetParentWithId(<HTMLElement>divs[iterator].firstChild);
				parentContainer.firstChild.appendChild(divs[iterator].firstChild);
				parentContainer.removeChild(divs[iterator]);
			}
		}
	}

	SetContainerId(cardid: string, containerid: string)
	{
		let card: HTMLElement=document.getElementById(cardid);
		card["location"]=containerid;
	}

	GetQSortCheckFunctor(joker: string): Function
	{
		return function (manager: QInterfaceGenerator) { return function () { if (document.getElementById("qSort") != null) return; if (joker) manager.CleanMain([joker]); if (document.getElementById("disagree") != null) { document.getElementById("disagree")["isFrozen"] = true; document.getElementById("agree")["isFrozen"] = true; document.getElementById("neutral")["isFrozen"] = true; } }; }(this);
	}

	GetQSortInsertToTitleCellFunctor(): Function
	{
		return function (qConfig: Array<Tab>, node: HTMLElement, statementsTarget: HTMLElement, iterator: number, tab: Array<number>) { node.innerHTML = qConfig[tab[iterator]].title; };
	}

	GetQSortInsertFunctor(): Function
	{
		return function (manager: QInterfaceGenerator) {
			return function (node) {
				let main: HTMLElement = manager.GetMain();
				main.insertBefore(node, main.firstChild);
				manager.OpacityAnimation(node, 0.0, 1.0, manager.processor.configuration.opacityAnimationInterval, manager.processor.configuration.opacityAnimationDuration);
				let name:string;
				if (manager.processor.configuration.threeStates) {
					name = "threeStates";
					document.getElementById("threeStates").style.display = null;
				}
				else {
					name = "pickingZone";
					document.getElementById("pickingZone").style.display = null;
					if (manager.processor.configuration.isThreeStatesDeck)
						manager.UpdateDeckView(true); 
				}
				manager.OpacityAnimation(document.getElementById(name), 0.0, 1.0, manager.processor.configuration.opacityAnimationInterval, manager.processor.configuration.opacityAnimationDuration);
				manager.dragAndDropManager.Start();
			};
		}(this);
	}

	GetThreeStatesCheckFunctor(): Function
	{
		return function (manager: QInterfaceGenerator) { return function () { manager.CleanMain(["pickingZone"]); if (manager.processor.configuration.isThreeStatesDeck) manager.UpdateDeckView(true); }; }(this);
	}

	GetThreeStatesInsertToTitleCellFunctor(): Function {
		return function (threeStatesConfig: Array<Tab>, text: HTMLElement, iterator: number) { text.className = "tabletitle"; text.innerHTML = threeStatesConfig[iterator].title; };
	}

	GetThreeStatesInsertFunctor(): Function
	{
		return function (manager: QInterfaceGenerator) { return function (node) { document.getElementById("pickingZone").style.display = null; let main = manager.GetMain(); main.insertBefore(node, main.firstChild); manager.OpacityAnimation(main, 0.0, 1.0, manager.processor.configuration.opacityAnimationInterval, manager.processor.configuration.opacityAnimationDuration); manager.dragAndDropManager.Start(); }; }(this);
	}

	SetMediaEvent(player: HTMLMediaElement)
	{
		//isMedia=true;
		//if('onwebkitfullscreenchange' in player)
		//	player.onwebkitfullscreenchange = (function (id: string){return function(){let media=document.getElementById(id);Storage.StoreEvent("media","fullscreen",id,media.mozFullScreen);};})(player.id);
		player.onpause = function (id: string, storage: QInterfaceStorage) { return function () { let media: HTMLMediaElement = <HTMLMediaElement>document.getElementById(id); storage.StoreEvent("media", "pause", id, media.currentTime); }; }(player.id, this.processor.storage);
		player.onplay = function (id: string, storage: QInterfaceStorage) { return function () { let media: HTMLMediaElement = <HTMLMediaElement>document.getElementById(id); storage.StoreEvent("media", "play", id, media.currentTime); }; }(player.id, this.processor.storage);
		player.onended = function (id: string, storage: QInterfaceStorage) { return function () { storage.StoreEvent("media", "end", id); }; }(player.id, this.processor.storage);
		player.onseeking = function (id: string, storage: QInterfaceStorage) { return function () { let media: HTMLMediaElement = <HTMLMediaElement>document.getElementById(id); storage.StoreEvent("media", "seeking", id, media.currentTime); }; }(player.id, this.processor.storage);
		player.onvolumechange = function (id: string, storage: QInterfaceStorage) { return function () { let media: HTMLMediaElement = <HTMLMediaElement>document.getElementById(id); storage.StoreEvent("media", "volumechange", id, media.volume); }; }(player.id, this.processor.storage);
		//Behavior correction for chromium.
		//player.onmousedown=function(event){event.stopPropagation();};
	}

	GenerateStatement(statement: Statement): HTMLElement
	{
		if(!statement)
			return;
		if(statement.type==="text")
		{
			let text: HTMLElement=document.createElement("div");
			text.innerHTML=statement.value;
			return text;
		}
		if(statement.type==="image") 
		{
			let image: HTMLImageElement = document.createElement("img");
			image.setAttribute("src", /*document.baseURI +*/ statement.value);
			image.oncontextmenu = function (manager: QInterfaceGenerator) { return function (event) { event.preventDefault(); event.stopPropagation(); manager.ScaleElement(event.target.parentNode); }; }(this);
			if (statement.description)
			{
				let div: HTMLDivElement = document.createElement("div");
				div.appendChild(image);
				div.appendChild(document.createTextNode(statement.description));
				return div;
			}
			return image;
		}
		if(statement.type==="audio") 
		{
			let audio: HTMLAudioElement = new Audio(/*document.baseURI +*/ statement.value);
			audio.id='audio-player-'+statement.id;
			audio.controls=true;
			//audio.controlsList="nodownload";
			this.SetMediaEvent(audio);
			if (statement.description)
			{
				let div: HTMLDivElement=document.createElement("div");
				div.appendChild(audio);
				div.appendChild(document.createTextNode(statement.description));
				return div;
			}
			if (statement.illustration)
			{
				let div: HTMLDivElement = document.createElement("div");
				div.appendChild(audio);
				let image: HTMLImageElement = document.createElement("img");
				div.appendChild(image);
				image.setAttribute("src", document.baseURI + statement.illustration);
				return div;
			}
			return audio;
		}
		if(statement.type==="video") 
		{
			let video: HTMLMediaElement=document.createElement("video");
			video.id='video-player-'+statement.id;
			video.controls=true;
			video.src=statement.value;
			//video.controlsList="nodownload";
			this.SetMediaEvent(video);
			return video;
		}
		alert(this.processor.configuration.unknowstatementype);
		return null;
	}

	GenerateCards() 
	{
		let main: HTMLElement = this.GetMain();
		this.CleanMain();
		let button: HTMLButtonElement=document.createElement("button");
		button.id="button";
		button.className="noselect nextStepButton";
		button.appendChild(document.createTextNode(this.processor.configuration.continueButton));
		if (this.processor.configuration.threeStates)
			button.onclick = function (manager: QInterfaceGenerator) {
				return function () {
					manager.processor.StoreNextStepEvent(); manager.GenerateStepPage(manager.processor.configuration.threeStatesText, manager.processor.configuration.startButton,
						function (manager: QInterfaceGenerator) { return function () { manager.processor.StoreNextStepEvent(); manager.GenerateThreeStates(manager.processor.configuration.threeStates, manager.GetThreeStatesCheckFunctor(), manager.GetThreeStatesInsertToTitleCellFunctor(), manager.GetThreeStatesInsertFunctor()); }; }(manager), ["pickingZone"]);
				};
			}(this);
		else
			button.onclick = function (manager: QInterfaceGenerator) {
				return function () {
					manager.processor.StoreNextStepEvent(); manager.GenerateStepPage(manager.processor.configuration.qSortText, manager.processor.configuration.startButton,
						function (manager: QInterfaceGenerator) {
							return function () {
								manager.processor.StoreNextStepEvent();
								manager.GenerateQSort(manager.processor.configuration.qSortConfiguration.isCrescent, getAlignementType(manager.processor.configuration.qSortConfiguration.statementsAlignement), manager.processor.configuration.qSortConfiguration.qSort, manager.processor.configuration.statements.length, manager.GetQSortCheckFunctor("pickingZone"), manager.GetQSortInsertToTitleCellFunctor(), manager.GetQSortInsertFunctor());
							};
						}(manager), ["pickingZone"]);
				};
			}(this);
		let pickingZone: HTMLElement=document.createElement("div");
		pickingZone.id="pickingZone";
		pickingZone.className="nestable nest-container";
		for (let key in this.processor.randoms)
		{
			let li: HTMLDivElement=document.createElement('div');
			li.appendChild(this.GenerateStatement(this.processor.statements[this.processor.randoms[key]]));
			li.className="nested-item noselect";
			li["location"]="pickingZone";
			li["baseFontSize"]=1.2;
			li.oncontextmenu = function (manager: QInterfaceGenerator) {
				return function (event: Event) { let node = <HTMLElement>(event.target); if (!node.classList.contains("nested-item")) return; event.preventDefault(); event.stopPropagation(); manager.ScaleElement(node); };
			}(this);
			li.id = this.processor.statements[this.processor.randoms[key]].id.toString();
			/*if(replayScale)
			{
				li.style.width=(replayScale*320)+"px";
				li.style.height=(replayScale*190)+"px";
				li.style.fontSize=(replayScale*1.2)+"em";
				li.style.padding=(replayScale*10)+"px";
				li.style.borderRadius=(replayScale*25)+"px";
			}*/
			pickingZone.appendChild(li);
		}
		main.appendChild(pickingZone);
		main.appendChild(button);

		this.dragAndDropManager.Start();
		this.OpacityAnimation(pickingZone, 0.0, 1.0, this.processor.configuration.opacityAnimationInterval, this.processor.configuration.opacityAnimationDuration);
	}

	ChangeCardScale(card: HTMLElement, scale: number, isGlobalScale: boolean)
	{	
		if(!card["baseWidth"])
			card["baseWidth"] = card.scrollWidth > 0 ? card.scrollWidth : this.baseCardValue.w;
		else if(!this.baseCardValue.w)
			this.baseCardValue.w=card["baseWidth"];
		if(!card["baseHeight"])
			card["baseHeight"] = card.scrollHeight > 0 ? card.scrollHeight : this.baseCardValue.h;
		else if (!this.baseCardValue.h)
			this.baseCardValue.h=card["baseHeight"];
		if(!card["baseFontSize"])
			card["baseFontSize"]=parseFloat(card.style.fontSize);
		if (!card["baseScale"] || isGlobalScale)
			card["baseScale"] = scale;
		if (!isGlobalScale && scale || !card["stateScale"])
			card["stateScale"] = scale;

		let localScale: number = this.scaleZoom * card["stateScale"];
		card.style.width = Math.ceil(card["baseWidth"] * localScale * card["baseScale"])+"px";
		card.style.height = Math.ceil(card["baseHeight"] * localScale * card["baseScale"])+"px";
		card.style.fontSize = card["baseFontSize"] * localScale * card["baseScale"]+"em";
	}

	CountCardMovement(cardid: string, containerid: string)
	{
		let card: HTMLElement = <HTMLElement>document.getElementById(cardid);
		let container: HTMLElement;
		if(!containerid)
			container=<HTMLElement>document.getElementById(cardid).parentNode;
		else
			container = <HTMLElement>document.getElementById(containerid);
		let parentContainer: HTMLElement = this.GetParentWithId(<HTMLElement>container.parentNode);
		let location: HTMLElement = null;
		if(parentContainer.id=="body")
			parentContainer=container;
		if(!card)
			return;
		let realstate: number = this.processor.state;
		let origin: string = "pickingZone";
		if (document.getElementById("qSort"))
		{
			if (this.processor.configuration.threeStates)
				origin = "threeStates";
			realstate = 6;
		}
		else if (document.getElementById("threeStates"))
			realstate = 3;
		else
			return;
		let threeStatesMap = { agree: 0, neutral: 1, disagree: 2 }; // Do Better
		switch(realstate)
		{
			case 3:
			case 4:
				if(container.id!==origin)
				{
					if (!card["container"])
						card["container"] = container.id;
					else
					{
						if (this.processor.configuration.dynamicType & 0x01 && card["container"] in threeStatesMap && container.id in threeStatesMap)
						{
							let val1 = threeStatesMap[card["container"]];
							let val2 = threeStatesMap[container.id]
							this.processor.cardsDistances[cardid] += Math.abs(val1 - val2);
						}
						if (this.processor.configuration.dynamicType & 0x02 && card["container"]!==container.id)
							this.processor.cardsMovements[cardid]++;
						card["container"] = container.id;
					}
				}
				if(container.id==="pickingZone")
				{
					if(card["location"]==="pickingZone")
						return;
					card["location"]="pickingZone";
					break;
				}
				else if(parentContainer.id==="threeStates" && "location" in card && card["location"] && this.GetParentWithId(document.getElementById(card["location"])).id==="threeStates")
					return;
				card["location"]=parentContainer.id;
				break;
			case 6:
			case 7:
				if(container.id!==origin)
				{
					if (!card["container"] || card["container"] in threeStatesMap)
						card["container"]=container.id;
					else
					{
						if (this.processor.configuration.dynamicType & 0x01)
							this.processor.cardsDistances[cardid]+=Math.abs(Number.parseInt(card["container"].substring(4))-Number.parseInt(container.id.substring(4)));
						if (this.processor.configuration.dynamicType & 0x02 && card["container"]!==container.id)
							this.processor.cardsMovements[cardid]++;
						card["container"]=container.id;
					}
				}
				if(((parentContainer && parentContainer.id==="qSort")||(container && container.id==="qSort")) && "location" in card && card["location"])
				{
					
					location=this.GetParentWithId(<HTMLElement>document.getElementById(card["location"]).parentNode);
					if(location.id==="qSort")
						return;
				}
				else if((parentContainer && parentContainer.id===origin)||(container && container.id===origin))
				{
					if(card["location"]===origin)
						return;
					break;
				}
				break;
			default:
				return;
		}
	}

	ChangeCardsScale(scale: number)
	{
		this.scaleZoom = scale;
		let cards: HTMLCollectionOf<Element> = document.getElementsByClassName("nested-item");
		for (let iterator: number = 0; iterator < cards.length; iterator++)
			this.ChangeCardScale(<HTMLElement>cards[iterator], scale, true);
	}

	SetParameters(node: HTMLElement,parameters: any)
	{
		let isRequired: boolean = false;
		for (let iterator: number=0; iterator<parameters.length; iterator++)
		{
			node[parameters[iterator].name]=parameters[iterator].value;
			if(parameters[iterator].name==="required"&&parameters[iterator].value)
				isRequired=true;
		}
		return isRequired;
	}

	GenerateExoQuizz() {
		let main: HTMLElement = this.GetMain();
		this.CleanMain();
		let form: HTMLTableElement = document.createElement("table");
		form.className = "tableExo";
		for (let iterator = 0; iterator < this.processor.configuration.exoQuizz.length; iterator++) {
			let row: HTMLTableRowElement = form.insertRow(iterator);
			row.className = "trExo";
			row.insertCell(0).appendChild(document.createTextNode(this.processor.configuration.exoQuizz[iterator].text));
			row.cells[0].className = "tdExo";
			let div: HTMLTableCellElement = row.insertCell(1);
			//TO DO locally
			let input: HTMLInputElement, label: HTMLLabelElement, radio: HTMLDivElement, checkbox: HTMLDivElement, range: HTMLDivElement, datalist: HTMLDataListElement, option: HTMLOptionElement, asterix: boolean;
			switch (this.processor.configuration.exoQuizz[iterator].type) {
				case "text":
					input = document.createElement("input");
					input.id = this.processor.configuration.exoQuizz[iterator].id;
					input.type = "text";
					/*input.onkeydown = function (storage: QInterfaceStorage) {return function (event: KeyboardEvent) { storage.StoreKeyEvent(event) };}(this.processor.storage);
					input.onkeyup = function (storage: QInterfaceStorage) { return function (event: KeyboardEvent) { storage.StoreKeyEvent(event) }; }(this.processor.storage);
					input.onfocus = function (storage: QInterfaceStorage) { return function (event: FocusEvent) { storage.StoreFocusEvent(event) }; }(this.processor.storage);*/
					if (this.SetParameters(input, this.processor.configuration.exoQuizz[iterator].parameters))
						form.rows[iterator].cells[0].innerHTML += "(*)";
					div.appendChild(input);
					break;
				case "number":
					input = document.createElement("input");
					input.id = this.processor.configuration.exoQuizz[iterator].id;
					input.type = "number";
					/*input.onkeydown = function (storage: QInterfaceStorage) {return function (event: KeyboardEvent) { storage.StoreKeyEvent(event) };}(this.processor.storage);
					input.onkeyup = function (storage: QInterfaceStorage) { return function (event: KeyboardEvent) { storage.StoreKeyEvent(event) }; }(this.processor.storage);
					input.onfocus = function (storage: QInterfaceStorage) { return function (event: FocusEvent) { storage.StoreFocusEvent(event) }; }(this.processor.storage);*/
					if (this.SetParameters(input, this.processor.configuration.exoQuizz[iterator].parameters))
						form.rows[iterator].cells[0].innerHTML += "(*)";
					div.appendChild(input);
					break;
				case "radio":
					radio = document.createElement("div");
					radio.id = this.processor.configuration.exoQuizz[iterator].id;
					asterix = false;
					for (let iterator2 = 0; iterator2 < this.processor.configuration.exoQuizz[iterator].values.length; iterator2++) {
						input = document.createElement("input");
						input.value = this.processor.configuration.exoQuizz[iterator].values[iterator2];
						input.type = "radio";
						input.name = this.processor.configuration.exoQuizz[iterator].id;
						input.id = this.processor.configuration.exoQuizz[iterator].id + "_" + iterator2;
						//input.onchange = function (storage: QInterfaceStorage) { return function (event: Event) { storage.StoreOnChangeRadioEvent(event) }; }(this.processor.storage);
						radio.appendChild(input);
						label = document.createElement("label");
						label.innerHTML = this.processor.configuration.exoQuizz[iterator].values[iterator2];
						label.htmlFor = input.id;
						if (this.SetParameters(input, this.processor.configuration.exoQuizz[iterator].parameters))
							asterix = true;
						radio.appendChild(label);
					}
					if (asterix)
						form.rows[iterator].cells[0].innerHTML += "(*)";
					div.appendChild(radio);
					break;
				case "checkbox":
					checkbox = document.createElement("div");
					asterix = false;
					checkbox.id = this.processor.configuration.exoQuizz[iterator].id;
					for (let iterator2 = 0; iterator2 < this.processor.configuration.exoQuizz[iterator].values.length; iterator2++) {
						input = document.createElement("input");
						input.value = this.processor.configuration.exoQuizz[iterator].values[iterator2];
						input.type = "checkbox";
						input.name = this.processor.configuration.exoQuizz[iterator].id;
						input.id = this.processor.configuration.exoQuizz[iterator].id + "_" + this.processor.configuration.exoQuizz[iterator].values[iterator2];
						//input.onchange = function (storage: QInterfaceStorage) { return function (event: Event) { storage.StoreOnChangeCheckboxEvent(event) }; }(this.processor.storage);
						checkbox.appendChild(input);
						label = document.createElement("label");
						label.innerHTML = this.processor.configuration.exoQuizz[iterator].values[iterator2];
						label.htmlFor = input.id;
						checkbox.appendChild(label);
						if (this.SetParameters(input, this.processor.configuration.exoQuizz[iterator].parameters))
							asterix = true;
					}
					if (asterix)
						form.rows[iterator].cells[0].innerHTML += "(*)";
					div.appendChild(checkbox);
					break;
				case "range":
					range = document.createElement("div");
					range.id = this.processor.configuration.exoQuizz[iterator].id;
					input = document.createElement("input");
					input.type = "range";
					input.className = "rangeExo";
					input.id = this.processor.configuration.exoQuizz[iterator].id;
					if (this.processor.configuration.exoQuizz[iterator].value && this.processor.configuration.exoQuizz[iterator].value.length) {
						let datalistId: string = "datalist_" + this.processor.configuration.exoQuizz[iterator].id;
						input.setAttribute("list", datalistId);
						datalist = document.createElement("datalist");
						datalist.id = datalistId;
						for (let iterator2 = 0; iterator2 < this.processor.configuration.exoQuizz[iterator].value.length; iterator2++) {
							option = document.createElement("option");
							option.value = this.processor.configuration.exoQuizz[iterator].values[iterator2].value;
							option.label = this.processor.configuration.exoQuizz[iterator].values[iterator2].label;
							datalist.appendChild(option);
						}
						div.appendChild(datalist);
					}
					else
						input.value = this.processor.configuration.exoQuizz[iterator].value;
					//input.oninput = function (storage: QInterfaceStorage) { return function (event: Event) { storage.StoreRangeEvent(event) }; }(this.processor.storage);
					if (this.SetParameters(input, this.processor.configuration.exoQuizz[iterator].parameters))
						form.rows[iterator].cells[0].innerHTML += "(*)";
					range.appendChild(input);
					div.appendChild(range);
					break;
			}
		}
		main.appendChild(form);
		main.appendChild(document.createElement("br"));
		if (this.processor.configuration.requiredText) {
			let requiredDiv: HTMLDivElement = document.createElement("div");
			requiredDiv.innerHTML = "(*) " + this.processor.configuration.requiredText;
			requiredDiv.style.color = "grey";
			main.appendChild(requiredDiv);
		}
		if (this.processor.configuration.isSendButtonsAtExogen) {
			this.GenerateNotSendSendButtons(main, function (manager: QInterfaceGenerator) { return function () { manager.processor.state = 11; return manager.processor.StoreExoResult();};}(this));
		}
		else
		{ 
			let submit: HTMLInputElement = document.createElement("input");
			submit.type = "button";
			submit.id = "button";
			submit.className = "noselect nextStepButton";
			submit.value = this.processor.configuration.continueButton;
			submit.onclick = function (manager: QInterfaceGenerator) { return function () { if (manager.processor.StoreExoResult()) { manager.processor.StoreNextStepEvent(); manager.GenerateFinnishPage(); } else alert(manager.processor.configuration.exoQuizzNotFull) }; }(this);
			main.appendChild(submit);
		}
	}

	OpacityAnimation(element: HTMLElement, start: number, end: number, interval: number, duration: number)
	{
		element.style.opacity = start.toString();
		let acc: number = start;
		let step: number = this.timeFactor*(end-start)*interval/duration; 
		let id = setInterval(frame, this.timeFactor*interval);
		  function frame()
		  {
				acc+=step;
				if (acc>=end) 
				{
					clearInterval(id);
					acc=end;
				}
			  element.style.opacity = acc.toString();
		  }
	}

	ScalingAnimation(element: HTMLElement, start: number, end: number, interval: number, duration: number)
	{
		  if("scaleAnimation" in element && element["scaleAnimation"]!=0)
			return;
		let acc: number = start;
		let step: number = this.timeFactor * (end - start) * interval / duration;
		let id = setInterval(frame, this.timeFactor * interval, this);
		element["scaleAnimation"] = end - start;
			function frame(manager: QInterfaceGenerator) {
				acc += step;
				if (acc >= end && step > 0 || acc <= end && step < 0) {
					element["scaleAnimation"] = 0;
					clearInterval(id);
					acc = end;
				}
				if (acc < 0.0)
					element["stateScale"] = -acc;
				else
					element["stateScale"] = acc;
				manager.ChangeCardScale(element, 0, false);
			};
	}

	/*static TranslateAnimation(element,start,end,interval,duration)
	{
		  let acc=start;
		  let stepX=timefactor*(end.x-start.x)*interval/duration;
		  let stepY=timefactor*(end.y-start.y)*interval/duration;
		  let id=setInterval(frame, timefactor*interval);
		  function frame()
		  {
				acc.x+=stepX;
				acc.y+=stepY;
				if (acc.x>=end.x&&stepX>0 || acc.x<=end.x&&stepX<0 || acc.y>=end.y&&stepY>0 || acc.y<=end.y&&stepY<0) 
				{
					clearInterval(id);
					acc=end;
				}
				element.style.transform="translate("+acc.x+"px,"+acc.y+"px)";
		  }
	}*/

	UnscaleElement()
	{
		if(!this.scaledNode)
			return;
		this.processor.storage.StoreEvent("zooming", "scale", this.scaledNode.id, "initial");
		this.scaledNode.style.transform="initial";
		if (this.scaledNode["oldStateScale"])
		{
			this.scaledNode["stateScale"] = this.scaledNode["oldStateScale"];
			this.ChangeCardScale(this.scaledNode, 0, false);
		}
		this.scaledNode=null;
	}

	GetGlobalOffsetLeft(element: HTMLElement): number
	{
		let acc: number = 0;
		while(element.offsetParent)
		{
			acc += element.offsetLeft;
			element = element.parentElement;
		}
		return acc;
	}

	GetGlobalOffsetTop(element: HTMLElement): number
	{
		let acc: number = 0;
		while(element.offsetParent)
		{
			acc += element.offsetTop;
			element = element.parentElement;
		}
		return acc;
	}

	ScaleElement(element: HTMLElement)
	{
		if (this.scaledNode && this.scaledNode===element&&!this.dragAndDropManager.IsDragging())
		{
			this.UnscaleElement();
			return;
		}
		if (this.scaledNode)
			this.UnscaleElement();
		this.scaledNode=element;
		if (this.scaledNode["stateScale"])
		{
			this.scaledNode["oldStateScale"] = this.scaledNode["stateScale"];
			this.scaledNode["stateScale"]=1;
			this.ChangeCardScale(this.scaledNode,0,false);
		}
		let width: number = (element.style.width === "" || element.offsetWidth > parseInt(element.style.width)) ? element.offsetWidth : parseInt(element.style.width);
		let scale: number = this.processor.configuration.scalingAnimationMaxValue * Math.pow(1 - width / window.innerWidth, 2);
		if (scale * width > (window.innerWidth * 0.75))
			scale = (window.innerWidth * 0.95) / width;
		this.processor.storage.StoreEvent("zooming", "scale", element.id);
		let x: number = (window.innerWidth / 2) - (this.GetGlobalOffsetLeft(element) + element.offsetWidth / 2), y: number = (window.innerHeight / 2) - (this.GetGlobalOffsetTop(element) + element.offsetHeight / 2);
		element.style.transform = "translate(" + x + "px," + y + "px) scale(" + scale +")"; 
	}

	CountRealChildsInContainer(container: HTMLElement): number
	{
		let count: number=0;
		for (let iterator: number = 0; iterator < container.childNodes.length; iterator++)
		{
			let childNode = <HTMLElement>container.childNodes[iterator];
			if (!childNode.classList.contains("clone") && childNode.classList.contains("nested-item"))
				count++;
		}	
		return count;
	}

	CheckIfScaleAnimation(container: HTMLElement, target: HTMLElement): HTMLElement {
		let smaller: boolean = false;
		let containerTable: HTMLElement = this.GetParentWithType(container, 'TABLE');
		let targetTable: HTMLElement = this.GetParentWithType(target, 'TABLE');

		if (containerTable == targetTable)
			return container;

		if (containerTable && ((containerTable.id === "qSort" && (!targetTable || targetTable.id === "threeStates")) || (containerTable.id === "threeStates" && !targetTable)))
			smaller = true;

		if (smaller)
		{
			if (this.processor.configuration.scalingAnimationDuration)
				this.ScalingAnimation(target, 1.0, this.processor.configuration.scalingAnimationMinValue, this.processor.configuration.scalingAnimationInterval, this.processor.configuration.scalingAnimationDuration);
			else
				this.ChangeCardScale(target, this.processor.configuration.scalingAnimationMinValue,false);
		}
		else
		{
			if (this.processor.configuration.scalingAnimationDuration)
				this.ScalingAnimation(target, this.processor.configuration.scalingAnimationMinValue, 1.0, this.processor.configuration.scalingAnimationInterval, this.processor.configuration.scalingAnimationDuration);
			else
				this.ChangeCardScale(target, 1,false);
		}

		return container;
	}

	IsAcceptNewChild(container: HTMLElement, target: HTMLElement, locked: boolean): HTMLElement
	{
		if(!container||container.className==="")
			return null;
		if(container.className.includes("nested-item"))
			return this.IsAcceptNewChild(<HTMLElement>container.parentNode,target,false);
		
		if(container.className.includes("nestable"))
		{
			if(container['isFrozen'] === true)
			{
				let containerTable: HTMLElement = this.GetParentWithType(container, 'TABLE');
				let targetTable: HTMLElement = this.GetParentWithType(target, 'TABLE');
 				if(targetTable === containerTable)
					return null;
				if (containerTable.id === "threeStates")
					return this.CheckIfScaleAnimation(document.getElementById(target['originState']), target);
				return this.CheckIfScaleAnimation(container, target);
			}
			if (!container['childLimit'])
				return this.CheckIfScaleAnimation(container, target);
			if (container['childLimit'] && container['childLimit']>this.CountRealChildsInContainer(container))
				return this.CheckIfScaleAnimation(container, target);
			if(locked)
				return null;
		}
		let parentNode: HTMLElement = <HTMLElement>container.parentNode;
		if (parentNode.classList.contains("nest-container"))
			return this.IsAcceptNewChild(parentNode,target,true);
		if(container.className.includes("nest-container"))
		{
			for(let iterator:number=0;iterator<container.childNodes.length;iterator++)
			{
				let childNode = <HTMLElement>container.childNodes[iterator];
				if (childNode.classList.contains("nestable"))
				{
					let res: HTMLElement  = this.IsAcceptNewChild(childNode,target,true);
					if(res)
						return this.CheckIfScaleAnimation(res, target);
				}
			}
			return null;
		}
		return null;
	}
	
	GenerateStartPage() {
		this.GetMain().classList.add("main");
		this.dragAndDropManager = new DragAndDropManager(
			this,
			'nested-item',
			'nest-container',
			function (manager: QInterfaceGenerator) {
				return function (id: string, parentid: string) { manager.UnscaleElement(); manager.processor.storage.StoreEvent("drag", "start", id, parentid); if (manager.processor.configuration.isThreeStatesDeck && manager.processor.state > 1) manager.UpdateDeckView(true); };
			}(this),
			function (manager: QInterfaceGenerator) {
				return function (id: string, parentid: string) { manager.processor.storage.StoreEvent("drag", "end", id, parentid); manager.CountCardMovement(id, parentid); if (parentid) { manager.SetContainerId(id, parentid); manager.CheckState(); manager.GetNewOrder(parentid); } if (manager.processor.configuration.isThreeStatesDeck) manager.UpdateDeckView(true); };
			}(this),
			function (manager: QInterfaceGenerator) {
				return function (id: string, cardid: string, extra: string) { manager.processor.storage.StoreDraggableEvent(id, cardid); if (manager.processor.configuration.isThreeStatesDeck) manager.UpdateDeckView(true); };
			}(this),
			function (manager: QInterfaceGenerator) {
				return function (container: HTMLElement, target: HTMLElement) { return manager.IsAcceptNewChild(container, target, false); };
			}(this), this.processor.configuration.dragAndDropClone);
		this.GenerateStepPage(this.processor.configuration.introText, this.processor.configuration.startButton, function (manager: QInterfaceGenerator) {
			return function () {
				ResetReferenceTime();
				manager.processor.storage.ClearStorage();
				manager.processor.StoreNextStepEvent();

				if (manager.processor.configuration.presentationText)
				{
					manager.GenerateStepPage(manager.processor.configuration.presentationText, manager.processor.configuration.continueButton, function (manager: QInterfaceGenerator)
					{
						return function () {
							manager.GenerateCards();
							manager.ChangeCardsScale(1.0);
							manager.GenerateZoomSlider();
						/*if (manager.processor.configuration.threeStates) 
							manager.GenerateStepPage(manager.processor.configuration.threeStatesText,
								manager.processor.configuration.startButton,
								function (manager: QInterfaceGenerator) { return function () { manager.processor.StoreNextStepEvent(); manager.GenerateThreeStates(manager.processor.configuration.threeStates, manager.GetThreeStatesCheckFunctor(), manager.GetThreeStatesInsertToTitleCellFunctor(), manager.GetThreeStatesInsertFunctor()); }; }(manager), ["pickingZone"]);
						
						else
							manager.GenerateStepPage(manager.processor.configuration.qSortText,
								manager.processor.configuration.startButton,
								function (manager: QInterfaceGenerator) { return function () { manager.processor.StoreNextStepEvent(); manager.GenerateQSort(manager.processor.configuration.qSortConfiguration.isCrescent, getAlignementType(manager.processor.configuration.qSortConfiguration.statementsAlignement), manager.processor.configuration.qSortConfiguration.qSort, manager.processor.configuration.statements.length, manager.GetQSortCheckFunctor("pickingZone"), manager.GetQSortInsertToTitleCellFunctor(), manager.GetQSortInsertFunctor()); }; }(manager), ["pickingZone"]);
						*/};	
					}(manager));
				}
				else
				{ 
					manager.GenerateCards();
					manager.ChangeCardsScale(1.0);
					manager.GenerateZoomSlider();
					if (!manager.processor.configuration.showStatementsFirst) {
						if (manager.processor.configuration.threeStates) {
							manager.processor.StoreNextStepEvent();
							manager.GenerateStepPage(manager.processor.configuration.threeStatesText,
								manager.processor.configuration.startButton,
								function (manager: QInterfaceGenerator) { return function () { manager.processor.StoreNextStepEvent(); manager.GenerateThreeStates(manager.processor.configuration.threeStates, manager.GetThreeStatesCheckFunctor(), manager.GetThreeStatesInsertToTitleCellFunctor(), manager.GetThreeStatesInsertFunctor()); }; }(manager), ["pickingZone"]);
						}
						else
						{
							manager.processor.StoreNextStepEvent();
							manager.GenerateStepPage(manager.processor.configuration.qSortText,
								manager.processor.configuration.startButton,
											function (manager: QInterfaceGenerator) {return function () {manager.processor.StoreNextStepEvent();manager.GenerateQSort(manager.processor.configuration.qSortConfiguration.isCrescent, getAlignementType(manager.processor.configuration.qSortConfiguration.statementsAlignement), manager.processor.configuration.qSortConfiguration.qSort, manager.processor.configuration.statements.length, manager.GetQSortCheckFunctor("pickingZone"), manager.GetQSortInsertToTitleCellFunctor(), manager.GetQSortInsertFunctor());};}(manager), ["pickingZone"]);
						}
					}
				}

			};
		}(this), ['sig'], true);
	}
}
