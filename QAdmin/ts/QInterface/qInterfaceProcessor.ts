import { QInterfaceGenerator } from "./qInterfaceGenerator"
import { QInterfaceStorage, Trace } from "./qInterfaceStorage"
import { FileManager } from "../Global/fileManager"
import { Sort, Statement } from "../Global/types"
import { error } from "./qInterface"

/**
 * Shuffle
 * 
 * http://jsfromhell.com/array/shuffle [v1.0]
 */

function ShuffleList(list: Array<any>): Array<any>
{
	for (let j, x, i = list.length; i; j = Math.floor(Math.random() * i), x = list[--i], list[i] = list[j], list[j] = x) { };
	return list;
}

/**
 * Basic interface & type
 */

interface IPair_N_S {
	key: number;
	value: string;
}

type ArrayIfPair_N_S = Array<IPair_N_S>; 

/**
 * QInterfaceProcessor
 */

export class QInterfaceProcessor {
	storage: QInterfaceStorage;
	configuration: any; //TO DO: probably need a spec
	statements: Array<Statement>;
	randoms: Array<number>;
	cardsMovements: Map<string, number>;
	cardsDistances: Map<string, number>;
	state: number;
	cardsForInterview: Sort;

	LoadStatement(): boolean
	{
		/* For extended version ??
		if (this.configuration.statementsFile && this.configuration.statementsType)
		{
			if (this.configuration.statementsType === "xml")
				FileManager.LoadFile(this.configuration.statementsFile, function (processor: QInterfaceProcessor) { return function parser(text: string) { try { this.statements = FileManager.XMLParse(text); return true; } catch (e) { console.log(e); return false; } }; }(this), function (processor: QInterfaceProcessor) { return function () { return processor.GenerateStatementsList(); }; }(this));
			else if (this.configuration.statementsType === "json")
				FileManager.LoadFile(this.configuration.statementsFile, function (processor: QInterfaceProcessor) { return function parser(text: string) { try { this.statements = FileManager.JSONParse(text); return true; } catch (e) { console.log(e); return false; } }; }(this), function (processor: QInterfaceProcessor) { return function () { return processor.GenerateStatementsList(); }; }(this));
			else if (this.configuration.statementsType === "csv")
				FileManager.LoadFile(this.configuration.statementsFile, function (processor: QInterfaceProcessor) { return function parser(text: string) { try { this.statements = FileManager.CSVParse(text); return true; } catch (e) { console.log(e); return false; } }; }(this), function (processor: QInterfaceProcessor) { return function () { return processor.GenerateStatementsList(); }; }(this));
			else
				alert(this.configuration.wrongstatementformatmessage);

		}
		else*/ if (this.configuration.statements) {
			this.statements = this.configuration.statements;
			this.GenerateStatementsList();
			return true;
		}
		return false;
	}

	LoadConfig(functor: Function) {
		FileManager.LoadFile('config.json', function (processor: QInterfaceProcessor) { return function (text: string) { try { processor.configuration = FileManager.JSONParse(text); return true; } catch (e) { alert(error); console.log(e); return false; } }; }(this), function (processor: QInterfaceProcessor) { return function () { if(processor.LoadStatement())return functor(); alert(error)}; }(this));
	}

	CleanUp()
	{
		this.state = 0;
		this.storage.CleanUp();
		this.randoms = new Array<number>();
		this.cardsMovements = new Map<string, number>();
		this.cardsDistances = new Map<string, number>();
		this.cardsForInterview = { id: "dynamics", value: new Set<string>() };
	}

	GenerateStatementsList()
	{
		this.CleanUp();
		for (let key: number = 0; key < this.statements.length; key++) {
			this.randoms.push(key);
			this.cardsMovements[this.statements[key].id] = 0;
			this.cardsDistances[this.statements[key].id] = 0;
		}
		ShuffleList(this.randoms);
	}

	GetStatement(id): Statement
	{
		for (let iterator: number = 0; iterator < this.statements.length; iterator++)
			if (this.statements[iterator].id === id)
				return this.statements[iterator];
		return null;
	}

	GenerateJSON(): string
	{
		let json: string = '{"setup":{ "window":{"x":' + window.innerWidth + ',"y":' + window.innerHeight + '},"userAgent":\"' + navigator.userAgent + '\","vendor":\"' + navigator.vendor + '\"},';
		json+='"statements": [';
		for (let key in this.randoms) {
			json += this.randoms[key];
			if (Number.parseInt(key) < this.randoms.length - 1)
				json += ',';
		}
		json += '],';
		if (this.state > 3 && this.configuration.threeStates)
			json += this.storage.GenerateThreeState() + ',';
		if (this.state > 5)
			json += this.storage.GenerateQSortState() + ',';
		if (this.state > 6) {
			json += '"postdata":[';
			json += this.storage.GetJSONFromStore("interview");
			if (this.state >= 10)
				json += "," + this.storage.GetJSONFromStore("exogen");
			json += ']'
		}
		if (this.configuration.saveTrace)
			json += ','+this.storage.GenerateTrace();
		json += '}';
		return json;
    }

    StoreThreeStatesValues(dom: QInterfaceGenerator)
    {
        if (!this.storage)
            return true;
        this.storage.threeStatesResults.disagree = dom.GetChildsIds(document.getElementById("disagree"), function (manager: QInterfaceGenerator) { return function (node) { manager.RegisterParents("disagree", "threeStates", node); }; }(dom));
        this.storage.threeStatesResults.neutral = dom.GetChildsIds(document.getElementById("neutral"), function (manager: QInterfaceGenerator) { return function (node) { manager.RegisterParents("neutral", "threeStates", node); }; }(dom));
        this.storage.threeStatesResults.agree = dom.GetChildsIds(document.getElementById("agree"), function (manager: QInterfaceGenerator) { return function (node) { manager.RegisterParents("agree", "threeStates", node); }; }(dom));
    }

	StoreQSortValues(dom: QInterfaceGenerator): boolean
    {
        let tab = [];
        for (let key in this.configuration.qSortConfiguration.qSort)
            tab[tab.length] = Number.parseInt(key);
        tab.sort(function (a, b) { return b - a; });
        for (let iterator in tab) {
			let object: Sort= { id: "", value: new Set<string>()};
            object.id = "tab_" + ([tab[iterator]]).toString();
            object.value = dom.GetChildsIds(document.getElementById("tab_" + (tab[iterator]).toString()), null, 'tab_');
            if (object.value.size === 1 && object.value.has("tab_"))
                object.value = new Set<string>();
            this.storage.qSort.push(object);
        }
        return true;
    }

	StoreTextInterview(dom: QInterfaceGenerator)
	{
        if (!this.storage.isActive)
            return;
        let list = document.getElementsByTagName("textarea");
        for (let iterator = 0; iterator < list.length; iterator++) {
			let object: Trace = { id: list[iterator].name, value: list[iterator].value, extra: dom.GetParentWithId(list[iterator]).id };
            this.storage.AppendToStorage("interview", JSON.stringify(object));
        }
    }

    StoreExoResult()
    {
        if (!this.storage.isActive)
			return true;
		let list: HTMLCollectionOf<HTMLInputElement> = document.getElementsByTagName("input");
        for (let iterator = 0; iterator < list.length; iterator++) {
			let object: Trace = { id: "", value: null };
            switch (list[iterator].type) {
                case "range":
                case "text":
                case "number":
                    object.id = list[iterator].id;
                    object.value = list[iterator].value;
                    break;
                case "radio":
					object.id = list[iterator].name;
					let firstRound: boolean = true;
                    while (iterator < list.length && list[iterator].type === "radio" &&  (firstRound || !iterator || list[iterator].parentNode === list[iterator - 1].parentNode)) {
                        firstRound = false;
                        if (list[iterator].checked)
							object.value = list[iterator].value;
						iterator++;
                    }
                    iterator--;
                    break;
                case "checkbox":
					object.id = list[iterator].name;
					object.value = new Array<string>();
                    while (iterator < list.length && list[iterator].type === "checkbox") {
                        if (list[iterator].checked)
							object.value.push(list[iterator].value);
                        iterator++;
                    }
                    iterator--;
                    break;
                default:
                case "button":
                    continue;
            }
			if (list[iterator]["required"] && (!object.value || object.value === "" || object.value.length == 0)) {
                return false;
            }
            if ("id" in object)
                this.storage.AppendToStorage("exogen", JSON.stringify(object));
        }
        return true;
    }

    StoreNewOrder(element: string, dom: QInterfaceGenerator)
    {
		let orderedList: Set<string>;
		if (this.state > 5)
            orderedList = dom.GetChildsIds(document.getElementById(element), null, 'tab_');
		else
            orderedList = dom.GetChildsIds(document.getElementById(element));
        this.storage.StoreEvent("drop", "elementslist", element, Array.from(orderedList));
    }

    StoreNextStepEvent()
    {
        this.state++;
        this.storage.StoreNextStepEvent(this.state, "check");
    }

	GetStatementForInterview()
	{
		let tabMouv: ArrayIfPair_N_S = new Array<IPair_N_S>();
		let tabDist: ArrayIfPair_N_S = new Array<IPair_N_S>();
		let mask: number = 0;
		if (this.configuration.dynamicType & 0x01)
		{
			for (let iterator in this.cardsMovements)
				tabMouv.push({ key: this.cardsMovements[iterator], value: iterator});
			tabMouv.sort(function (a: IPair_N_S, b: IPair_N_S) { return b.key - a.key; });
			mask += 1;
		}
		if (this.configuration.dynamicType & 0x02)
		{
			for (let iterator in this.cardsDistances)
				tabDist.push({ key: this.cardsDistances[iterator], value: iterator });
			tabDist.sort(function (a: IPair_N_S, b: IPair_N_S) { return b.key - a.key; });
			mask += 2;
		}
		let iteratorMouv :number = 0, iteratorDist :number = 0;
		switch (mask)
		{
			case 1:
				while (this.cardsForInterview.value.size < this.configuration.dynamicStatementNumber && iteratorMouv < tabMouv.length && tabMouv[iteratorMouv].key)
					this.cardsForInterview.value.add(tabMouv[iteratorMouv++].value);
				break;
			case 2:
				while (this.cardsForInterview.value.size < this.configuration.dynamicStatementNumber && iteratorDist < tabDist.length && tabDist[iteratorDist].key)
					this.cardsForInterview.value.add(tabDist[iteratorDist++].value);
				break;
			case 3:
				while (this.cardsForInterview.value.size < this.configuration.dynamicStatementNumber)
				{
					if ((tabDist.length <= iteratorDist && tabMouv.length <= iteratorMouv) || (!tabMouv[iteratorMouv].key && !tabDist[iteratorDist].key))
						break;
					if (tabDist.length > iteratorDist && tabMouv.length <= iteratorMouv) {
						if (!tabDist[iteratorDist].key)
							break;
						this.cardsForInterview.value.add(tabDist[iteratorDist++].value);
					}
					else if (tabDist.length <= iteratorDist && tabMouv.length > iteratorMouv) {
						if (!tabMouv[iteratorMouv].key)
							break;
						this.cardsForInterview.value.add(tabMouv[iteratorMouv++].value);
					}
					else if (tabDist[iteratorDist].key < tabMouv[iteratorMouv].key)
						this.cardsForInterview.value.add(tabMouv[iteratorMouv++].value);
					else
						this.cardsForInterview.value.add(tabDist[iteratorDist++].value);
				}
				break;
		}
	}
}
