interface HTMLInputEvent extends Event {
	value: HTMLInputElement & EventTarget;
}

interface ITag
{
	start: string;
	end: string;
}

export var textEditorDefaultColors: Array<string> = ["red", "green", "blue", "yellow", "orange", "purple", "pink", "cyan", "brown", "white", "black"];
export var textEditorDefaultSizes: Array<string> = ["h1", "h2", "h3", "h4", "h5"];

interface IStates
{
	bold: boolean;
	italic: boolean;
	underline: boolean;
	strike: boolean;
	list: boolean;
	format: string;
	backgroundColor: string;
	color: string;
}

export class TextEditor
{
	editor: HTMLElement;
	states: IStates;
	previousStates: IStates;
	previousCursorPosition: number;
	target: HTMLTextAreaElement;

	constructor(previewButtonText: string, formatArray: Array<string>, colorArray: Array<string>, backgroundColorArray: Array<string>)
	{
		this.states = { bold: false, italic: false, underline: false, strike: false, list: false, format: null, backgroundColor: null, color: null };
		if(formatArray)
			this.states.format="";
		
		if(backgroundColorArray)
			this.states.backgroundColor="";
		if(colorArray)
			this.states.color = "";

		this.StoreStates();
		this.editor=document.createElement("div");
		this.editor.id="textEditor";
		//TextEditor.editor.className="tooltiptext/toolbar";
		let toolbar: HTMLSpanElement = this.editor.appendChild(document.createElement("span"));
		toolbar.className="textditortoolbar";
		let button: HTMLButtonElement=toolbar.appendChild(document.createElement("button"));
		button.id="bold";
		button.className="textditortoolbarbutton";
		button.innerHTML = "<b>B</b>";
		button.onclick = function (editor: TextEditor) { return function (event: Event) { editor.states.bold = !editor.states.bold; editor.Engine() }; }(this);

		button=toolbar.appendChild(document.createElement("button"));
		button.id="underline";
		button.className="textditortoolbarbutton";
		button.innerHTML="<u>U</u>";
		button.onclick = function (editor: TextEditor) { return function (event: Event) { editor.states.underline = !editor.states.underline; editor.Engine() }; }(this);

		button=toolbar.appendChild(document.createElement("button"));
		button.id="italic";
		button.className="textditortoolbarbutton";
		button.innerHTML = "<i>I</i>";
		button.onclick = function (editor: TextEditor) { return function (event: Event) { editor.states.italic = !editor.states.italic; editor.Engine() }; }(this);

		button=toolbar.appendChild(document.createElement("button"));
		button.id="strike";
		button.className="textditortoolbarbutton";
		button.innerHTML = "<s>S</s>";
		button.onclick = function (editor: TextEditor) { return function (event: Event) { editor.states.strike = !editor.states.strike; editor.Engine() }; }(this);

		button=toolbar.appendChild(document.createElement("button"));
		button.id="list";
		button.className="textditortoolbarbutton";
		button.innerHTML="<u><b>list</b></u>";
		button.onclick = function (editor: TextEditor) { return function (event: Event) { editor.DoList() }; }(this);

		button=toolbar.appendChild(document.createElement("button"));
		button.id="left";
		button.className="textditortoolbarbutton";
		button.innerHTML = "<u><b>Left</b></u>";
		button.onclick = function (editor: TextEditor) { return function (event: Event) { editor.DoAlignementLeft() }; }(this);

		button=toolbar.appendChild(document.createElement("button"));
		button.id="center";
		button.className="textditortoolbarbutton";
		button.innerHTML = "<u><b>center</b></u>";
		button.onclick = function (editor: TextEditor) { return function (event: Event) { editor.DoAlignementCenter() }; }(this);

		button=toolbar.appendChild(document.createElement("button"));
		button.id="right";
		button.className="textditortoolbarbutton";
		button.innerHTML = "<u><b>Right</b></u>";
		button.onclick = function (editor: TextEditor) { return function (event: Event) { editor.DoAlignementRight() }; }(this);

		if(formatArray)
		{
			let select: HTMLSelectElement = toolbar.appendChild(document.createElement("select"));
			select.id="format";
			select.className="textditortoolbarselect";
			select.onchange = function (editor: TextEditor) { return function (event) { editor.states.format = event.target.value; editor.Engine() }; }(this);
			let option: HTMLOptionElement = select.appendChild(document.createElement("option"));
			option.value="";
			for(let iterator in formatArray)
			{
				option=select.appendChild(document.createElement("option"));
				option.value=formatArray[iterator];
				option.innerHTML=formatArray[iterator];
			}
		}
		if(colorArray)
		{
			let select: HTMLSelectElement =toolbar.appendChild(document.createElement("select"));
			select.id = "color";
			select.className = "textditortoolbarselect";
			select.onchange = function (editor: TextEditor) { return function (event) { editor.states.color = event.target.value; editor.Engine() }; }(this);
			let option: HTMLOptionElement=select.appendChild(document.createElement("option"));
			option.value="";
			for(let iterator in colorArray)
			{
				option=select.appendChild(document.createElement("option"));
				option.value=colorArray[iterator];
				option.style.color=colorArray[iterator];
				option.innerHTML="<b>"+colorArray[iterator]+"</b>";
			}
		}
		if(backgroundColorArray)
		{
			let select: HTMLSelectElement =toolbar.appendChild(document.createElement("select"));
			select.id="backgroundColor";
			select.className = "textditortoolbarselect";
			select.onchange = function (editor: TextEditor) { return function (event) { editor.states.backgroundColor = event.target.value; editor.Engine() }; }(this);
			let option: HTMLOptionElement=select.appendChild(document.createElement("option"));
			option.value="";
			for(let iterator in colorArray)
			{
				option=select.appendChild(document.createElement("option"));
				option.value=colorArray[iterator];
				option.style.backgroundColor=colorArray[iterator];
				option.innerHTML="<b>"+colorArray[iterator]+"</b>";
			}
		}

		let previewButton: HTMLButtonElement=toolbar.appendChild(document.createElement("button"));
		previewButton.appendChild(document.createTextNode(previewButtonText));
		previewButton.onclick = function (editor) { return function () { editor.ShowHidePreview(); }; }(this);
		let preview: HTMLDivElement=this.editor.appendChild(document.createElement("div"));
    	preview.id="previewTextArea";
    	preview.style.display="none";
    	this.editor.style.display="none";
		document.body.appendChild(this.editor);
		document.body.onkeyup = function (editor: TextEditor) { return function (event) { if (event.keyCode == 27) this.Stop(event); }; }(this);
	}

	StoreStates()
	{
		//this is disgusting...
		 this.previousStates=JSON.parse(JSON.stringify(this.states));
	}

	HidePreview()
	{
		let preview: HTMLElement = document.getElementById("previewTextArea");
		if(preview.style.display==="none")
			return false;
		preview.style.display="none";
		preview.innerHTML="";
		this.target.style.display="";
		preview.className="";
		return true;
	}

	ShowHidePreview()
	{
		let preview: HTMLElement =document.getElementById("previewTextArea");
		if(preview.style.display==="")
		{
			preview.style.display="none";
			this.target.style.display="";
			preview.className="";
			this.target.value=preview.innerHTML;
		}
		else if(this.target)
		{
			preview.style.display="";
			preview.innerHTML=this.target.value.replace(/\n/g,"<br>");
			preview.className="textEditorPreview "+this.target.className;
			this.target.style.display="none";
		}
	}

	UpdateToolbarStates()
	{
		for(let iterator in this.states)
		{
			let node: HTMLElement = document.getElementById(iterator);
			if(!node)
				continue;
			if((typeof this.states[iterator])==="string")
			{
				for(let iteratorOption in node.childNodes)
				{
					let htmlNode: HTMLOptionElement = <HTMLOptionElement>node.childNodes[iteratorOption];
					if (htmlNode.value===this.states[iterator])
					{
						htmlNode.selected=true;
						break;
					}
				}
			}
			else
			{
				if(this.states[iterator])
					node.className="pushed";
				else
					node.className="";
			}
		}
	}

	ResetStates()
	{
		let result: boolean = false;
		result = this.states.bold || result;
		result = this.states.italic || result;
		result = this.states.underline || result;
		result = this.states.strike || result;
		result = (this.states.format && this.states.format.length > 0) || result;
		result = (this.states.color && this.states.color.length > 0) || result;;
		result = (this.states.backgroundColor && this.states.backgroundColor.length > 0) || result;;
		this.states.bold=false;
		this.states.italic=false;
		this.states.underline=false;
		this.states.strike=false;
		if(this.states.format)
			this.states.format="";
		if(this.states.backgroundColor)
			this.states.backgroundColor="";
		if(this.states.color)
			this.states.color="";
		this.UpdateToolbarStates();
		return result;
	}

	Stop(event: Event)
	{
		if(!this.target)
			return;
		this.StoreStates();
		if(this.ResetStates())
		{
			this.Engine();
			event.preventDefault();
			event.stopPropagation();
			return;
		}
		if(this.HidePreview())
		{
			event.preventDefault();
			event.stopPropagation();
			return;
		}
		this.Hide()
	}

	Show(target: HTMLTextAreaElement, advancedMode?: boolean)
	{
		this.editor.style.display="";
		if(this.target)
		{
			this.HidePreview();
			this.target.onclick=null;
			this.target.onkeydown = null;
		}
		if(advancedMode){}
		if(this.target!==target)
		{
			this.ResetStates();
			this.StoreStates();
			this.target=target;
			target.parentNode.insertBefore(this.editor,target);
		}
	}

	Hide()
	{
		if(this.target)
		{
			this.editor.style.display="none";
			this.target.style.display="";
			document.body.appendChild(this.editor);
			this.target=null;
		}
	}

	CreateTag(name: string, invert: boolean)
	{
		switch(name)
		{
			case "bold":
				if(this.states[name]&&!invert)
					return "<b>";
				return "</b>";
			case "italic":
				if(this.states[name]&&!invert)
					return "<i>";
				return "</i>";
			case "underline":
				if(this.states[name]&&!invert)
					return "<u>";
				return "</u>";
			case "strike":
				if(this.states[name]&&!invert)
					return "<s>";
				return "</s>";
			case "format":
				if(this.states[name].length&&!invert)
					return "<"+this.states[name]+">";
				return "</"+this.states[name]+">";
			case "color":
				if(this.states[name].length&&!invert)
					return "<span style='color: "+this.states[name]+";'>";
				return "</span>";
			case "backgroundColor":
				if(this.states[name].length&&!invert)
					return "<span style='background-color: "+this.states[name]+";'>";
				return "</span>";
		}
	}

	CollectTag()
	{
		let tag:string="";
		for(let iterator in this.states)
		{
			if(this.states[iterator]!==this.previousStates[iterator])
				tag+=this.CreateTag(iterator, false);
		}
		this.StoreStates();
		return tag.length?tag:null;
	}

	CollectTags()
	{
		let tag: ITag={ start:"",end:""};
		for(let iterator in this.states)
		{
			if(this.states[iterator]!==this.previousStates[iterator])
				tag.start+=this.CreateTag(iterator,false);
			if(this.states[iterator]!==this.previousStates[iterator])
				tag.end+=this.CreateTag(iterator,true);
		}
		if(!tag.start.length)
			return null;
		this.StoreStates();
		return tag;
	}

	Engine()
	{
		let start: number = this.target.selectionStart;
		let end: number = this.target.selectionEnd;
		let html: string = this.target.value;
		if(start===end)
		{
			let tag=this.CollectTag();
			if(!tag)
				return;
			html=html.slice(0,start)+tag+html.slice(end,html.length);
			start+=tag.length;
			end=start;
		}
		else
		{
			let tag: ITag = this.CollectTags();
			if(!tag)
				return;
			html=html.slice(0,start)+tag.start+html.slice(start,end)+tag.end+html.slice(end,html.length);
			end+=tag.start.length+tag.end.length;	
			this.ResetStates();
			this.StoreStates();
		}
		this.UpdateToolbarStates();
		this.target.value=html;
		this.target.focus();
		this.target.selectionStart=start;
		this.target.selectionEnd=end;
	}
	
	DoList()
	{
		let start: number = this.target.selectionStart;
		let end: number = this.target.selectionEnd;
		let html: string = this.target.value;
		if(start===end)
		{
			html=html.slice(0,start)+"<ul><li></li></ul>"+html.slice(end,html.length);
			start+=9;
			end=start;
		}
		else
		{
			let list:string=html.slice(start,end).replace(/\n/g,"</li><li>");
			html=html.slice(0,start)+"<ul>"+list+"</ul>"+html.slice(end,html.length);
			end+=list.length+8;
		}
		this.target.value=html;
		this.target.focus();
		this.target.selectionStart=start;
		this.target.selectionEnd=end;
	}

	DoAlignement(type: string)
	{
		let start: number = this.target.selectionStart;
		let end: number = this.target.selectionEnd;
		let html: string = this.target.value;
		let tagStart:string="<div style='text-align:"+type+"'>";
		if(start===end)
		{
			html=html.slice(0,start)+tagStart+"</div>"+html.slice(end,html.length);
			start+=tagStart.length;
			end=start;
		}
		else
		{
			html=html.slice(0,start)+tagStart+html.slice(start,end)+"</div>"+html.slice(end,html.length);
			end+=tagStart.length+6;
		}
		this.target.value=html;
		this.target.focus();
		this.target.selectionStart=start;
		this.target.selectionEnd=end;
	}

	DoAlignementLeft()
	{
		this.DoAlignement("left");
	}

	DoAlignementCenter()
	{
		this.DoAlignement("center");
	}

	DoAlignementRight()
	{
		this.DoAlignement("right");
	}
}
