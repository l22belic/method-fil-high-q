import { Tab } from "./types"

/**
 * Basic interface, type & function
 */

interface HTMLInputEvent extends Event {
	target: HTMLInputElement & EventTarget;
}

export enum EStatementAlignement { Top, Bottom, Middle, Left, Right } 

export function getAlignementType(alignement: any): EStatementAlignement
{
	switch (alignement)
	{
		case "top":
		case 0:
			return EStatementAlignement.Top;
		case "bottom":
		case 1:
			return EStatementAlignement.Bottom;
		case "middle":
		case 2:
			return EStatementAlignement.Middle;
		case "left":
		case 3:
			return EStatementAlignement.Left;
		case "right":
		case 4:
			return EStatementAlignement.Right;
	}
}

export function getAlignementTypeText(alignement: EStatementAlignement): string
{
	switch (alignement)
	{
		case EStatementAlignement.Top:
			return '"top"' ;
		case EStatementAlignement.Bottom:
			return '"bottom"';
		case EStatementAlignement.Middle:
			return '"middle"';
		case EStatementAlignement.Left:
			return '"left"';
		case EStatementAlignement.Right:
			return '"right"';
	}
}

/**
 * DOMGenerator
 */

export class DOMGenerator 
{
	GetMain(): HTMLElement
	{
		let main=document.getElementById("main");
		if(main!==null)
			return main;
		main=document.createElement("div");
		main.id="main";
		document.body.appendChild(main);
		return main;
	}

	CleanMain(jokers?: Array<string>, stillDisplay?: boolean) 
	{
		let main: HTMLElement = this.GetMain();
		if(jokers)
		{
			let found: boolean = false;
			for (let iterator: number = 0; iterator < main.childNodes.length; iterator++)
			{
				found = false;
				let htmlchildNode: HTMLElement = <HTMLElement>main.childNodes[iterator];
				for (let iterator2: number=0; iterator2<jokers.length; iterator2++)
				{
					if (jokers[iterator2] === htmlchildNode.id)
					{
						found=true;
						break;
					}
				}
				if(!found)
				{
					main.removeChild(main.childNodes[iterator]);
					iterator--;
				}
				else if (!stillDisplay)
					htmlchildNode.style.display="none";
			}
		}
		else
		{
			while(main.firstChild)
				main.removeChild(main.firstChild);
		}
	}

	CleanNodeByName(name: string, jokers?: Array<string>) 
	{
		let main: HTMLElement=document.getElementById(name);
		if(jokers)
		{
			let found: boolean=false;
			for(let iterator:number=0; iterator<main.childNodes.length;iterator++)
			{
				found=false;
				for(let iterator2:number=0; iterator2<jokers.length; iterator2++)
				{
					let htmlchildNode = <HTMLElement>main.childNodes[iterator];
					if (jokers[iterator2] === htmlchildNode.id)
					{
						found=true;
						break;
					}
				}
				if(!found)
				{
					main.removeChild(main.childNodes[iterator]);
					iterator--;
				}
			}
		}
		else
		{
			while(main.firstChild)
				main.removeChild(main.firstChild);
		}
	}

	CleanNode(node: HTMLElement, jokers?: Array<string>)
	{
		if (jokers)
		{
			let found: boolean = false;
			for (let iterator: number = 0; iterator < node.childNodes.length; iterator++) {
				found = false;
				for (let iterator2: number = 0; iterator2 < jokers.length; iterator2++) {
					let htmlchildNode = <HTMLElement>node.childNodes[iterator];
					if (jokers[iterator2] === htmlchildNode.id) {
						found = true;
						break;
					}
				}
				if (!found) {
					node.removeChild(node.childNodes[iterator]);
					iterator--;
				}
			}
		}
		else {
			while (node.firstChild)
				node.removeChild(node.firstChild);
		}
	}

	GetParentWithId(node: HTMLElement, joker?: string, returnOnFirst?: boolean): HTMLElement
	{
		if (node.id)
		{
			if (returnOnFirst)
				return node;
			else
				node = <HTMLElement>node.parentNode;
		}
		while(!node.id || (joker && node.id.includes(joker)))
		{	
			if(!node.parentNode)
				return node;
			node = <HTMLScriptElement>node.parentNode;
		}
		return node;
	}

	GetParentWithClassName(node: HTMLElement, className: string): HTMLElement
	{
		if(node.className.includes(className))
			return node;
		while(true)
		{
			if(node.className.includes(className))
				return node;	
			if(!node.parentNode)
				return null;
			node = <HTMLElement>node.parentNode;
		}
		return null;
	}

	GetParentWithType(node: HTMLElement, type: string): HTMLElement
	{
		if (node.tagName.includes(type))
			return node;
		while (true)
		{
			if (node.tagName && node.tagName.includes(type))
				return node;
			else if (node.nodeName && node.nodeName.includes(type))
				return node;
			if (!node.parentNode)
				return null;
			node = <HTMLElement>node.parentNode;
		}
		return null;
	}

	GenSubTitle(title: string, content: HTMLElement, tooltipContent: string, isOptional?: string, buttonInnerHTML?: string, buttonFunctor?: Function): HTMLElement
	{	
		let mainContent: HTMLDivElement=document.createElement('div');
		mainContent.style.marginTop = "50px";

		let titleObj: HTMLDivElement=mainContent.appendChild(document.createElement('div'));
		Object.assign(titleObj.style,{color:"#000",marginBottom:"20px",fontVariant:"small-caps",fontWeight:"bold",fontSize:"28px",textShadow:"2px 2px 4px #aaa"});
		let titleDiv: HTMLDivElement=titleObj.appendChild(document.createElement("div"));		
		titleDiv.appendChild(document.createTextNode(title));
		if(isOptional)
		{
			titleObj.appendChild(document.createElement("br"))
			let hideButton: HTMLButtonElement=titleObj.appendChild(document.createElement("button"));
			hideButton.innerHTML = buttonInnerHTML;//configuration.optionnalPartDeactivate;
			//isActive[isOptional]=true;
			hideButton.id = "active" + isOptional;
			hideButton.onclick = function (variable) { return function (event) { buttonFunctor(event, variable);/*if(content.style.display==="none"){QAnalysisGenerator.ShowHideContent(content,"",event.target,configuration.optionnalPartDeactivate);AdminHistory.StoreInHistoric(variable,"set",true);isActive[variable]=true;}else{QAnalysisGenerator.ShowHideContent(content,"none",event.target,configuration.optionnalPartActivate);AdminHistory.StoreInHistoric(variable,"set",false);isActive[variable]=false;}/*if(document.getElementById("texts").childNodes.length)QAnalysisGenerator.GenerateWallOfTexts();*/ }; }(isOptional);
		}
		mainContent.appendChild(content);
		if(tooltipContent && tooltipContent.length>1)
		{
			titleDiv.className += "tooltip";
			let tooltip: HTMLSpanElement = titleDiv.appendChild(document.createElement("span"));
			tooltip.className="tooltiptext";
			tooltip.innerHTML=tooltipContent;
		}
		return(mainContent);
	}

	GetChildsIds(container: HTMLElement, functor?: Function, joker?: string): Set<string>
	{
		if(!container)
			return null;
		let data: Set<string>= new Set<string>();
		for(let iterator=0; iterator<container.childElementCount;iterator++) 
		{
			let htmlchildNode = <HTMLScriptElement>container.childNodes[iterator];
			if ((!htmlchildNode.id || joker && htmlchildNode.id.includes(joker)) && htmlchildNode.childElementCount > 0)
			{
				let dataSecondary: Set<string> = this.GetChildsIds(<HTMLElement>container.childNodes[iterator], functor);
				for (let currentNumber of dataSecondary)
					data.add(currentNumber);
			}
			else
			{
				if (htmlchildNode.id)
				{
					data.add(htmlchildNode.id);
					if(functor)
						functor(container.childNodes[iterator]);
				}
			}
		}
		return data;
	}

	Addvalue(begin: number, size: number, value: number, tab: Array<Tab>)
	{
		for(let iterator:number=begin;iterator<size+begin;iterator++)
			tab[iterator].limit+=value;
	}

	CheckQState(qsort: Array<Tab>, statementsNumber: number)
	{
		let sum:number=0;
		for(let iterator in qsort)
		{
			if(!qsort[iterator].limit)
				return;
			sum+=qsort[iterator].limit;
		}
		let style :string= "";
		if (sum !== statementsNumber)
			style="red";
		document.getElementById("qSort").style.backgroundColor=style;
	}

	CreateQStatediv(isLimitless: boolean, isLine: boolean): HTMLElement
	{
		let qstatediv: HTMLElement = document.createElement("div");
		if(isLimitless)
		{
			qstatediv.className = "nestable grey";
			qstatediv.style.minHeight="100px";
			if(isLine)
				qstatediv.style.minWidth="100%";
		}
		else
		{
			qstatediv['childLimit']=1;
			qstatediv.style.minHeight="30px";
			if(isLine)
				qstatediv.className = "grey nestable-line";
			else
				qstatediv.className = "grey nestable";
		}
		return qstatediv;
	}

	QTabPlacement(min: number, max: number, statementsNumber: number): Array<Tab>
	{
		let qsort: Array<Tab> = new Array<Tab>();
		let qtablesize = (max - min) + 1;
		for(let iterator:number=min;iterator<=max;iterator++)
		{
			if(qsort.length!==0&&qsort[iterator])
				qsort[iterator]={ limit: 0, title: qsort[iterator].title};
			else
				qsort[iterator] = { limit: 0, title: "" };
		}
		let result: number = statementsNumber;
		if(!result)
			return;
		let size: number=qtablesize;
		let lign: number=0;
		let iterator: number=min;
		while(true)
		{
			lign = Math.floor(result / size);
			result -= lign * size;
			if(result===lign||(result-lign)*size<size-2)
				lign--;
			this.Addvalue(iterator,size,lign,qsort);
			size-=2;
			iterator++;
			if(size===1||lign===0)
			{
				qsort[0].limit+=Math.ceil(result);
				break;
			}
			if(isNaN(lign)||isNaN(result))
			{
				let sum=1;
				for(let iterator=min;iterator<=max;iterator++)
					sum+=qsort[iterator].limit;
				qsort[0].limit += statementsNumber-sum;
				break;
			}
		}
		return qsort;
	}

	DeleteQSort()
	{
		if(document.getElementById("qSort")===null)
			return;
		document.getElementById("simulator").removeChild(document.getElementById("qSort"));
	}

	GenerateQSortByColumns(states: HTMLTableElement, sortedTab: Array<number>, qConfig: Array<Tab>, statAlign: EStatementAlignement, insertToTitleCellFunctor: Function)
	{
		let headerRow: HTMLTableRowElement;
		let dataDow: HTMLTableRowElement;
		let data: HTMLDivElement;
		switch(statAlign)
		{
			case EStatementAlignement.Top:
			case EStatementAlignement.Middle:
				headerRow=states.insertRow();
				dataDow=states.insertRow();
				break;
			case EStatementAlignement.Bottom:
				dataDow=states.insertRow();
				headerRow=states.insertRow();
				break;
		}

		for(let iterator in sortedTab)
		{
			let dataStat = document.createElement("div");
			data = document.createElement("div");
			data.className="tabletitle";
			insertToTitleCellFunctor(qConfig, data, dataStat, iterator, sortedTab, false);
			headerRow.insertCell().appendChild(data);
			dataStat.id="tab_"+(sortedTab[iterator]).toString();
			dataStat.className ="nest-container statAlign";
			if(!qConfig[sortedTab[iterator]].limit)
				dataStat.appendChild(this.CreateQStatediv(true,false));
			else
				for (let iterator2: number = 0; iterator2 < qConfig[sortedTab[iterator]].limit; iterator2++)
					dataStat.appendChild(this.CreateQStatediv(false,false));
			let cell=dataDow.insertCell();
			cell.appendChild(dataStat);
		}
	}

	GenerateQSortByLines(states: HTMLTableElement, sortedTab: Array<number>, qConfig: Array<Tab>, statAlign: EStatementAlignement, insertToTitleCellFunctor: Function)
	{
		let dataTitle: HTMLDivElement, dataCell: HTMLDivElement;
		for(let iterator in sortedTab)
		{
			dataTitle=document.createElement("div");
			dataCell=document.createElement("div");
			dataTitle.className="tabletitle";
			//specific insertion (QAdmin input vs QInterfaceText)
			insertToTitleCellFunctor(qConfig, dataTitle, dataCell, iterator, sortedTab, true);
			
			dataCell.id="tab_"+(sortedTab[iterator]).toString();
			dataCell.className ="nest-container statAlign";
			if(!qConfig[sortedTab[iterator]].limit)
				dataCell.appendChild(this.CreateQStatediv(true,true));
			else
				for(let iterator2: number=0;iterator2<qConfig[sortedTab[iterator]].limit;iterator2++)
					dataCell.appendChild(this.CreateQStatediv(false,true));

			let row: HTMLTableRowElement=states.insertRow();
			let cellTitle: HTMLTableCellElement, cellStates: HTMLTableCellElement;
			switch(statAlign)
			{
				case EStatementAlignement.Left:
					cellTitle=row.insertCell();
					cellStates=row.insertCell();
					break;
				case EStatementAlignement.Right:
					cellStates=row.insertCell();
					cellTitle=row.insertCell();
					break;
			}
			//cellStates.className="statAlign";
			cellStates.appendChild(dataCell);
			cellTitle.appendChild(dataTitle);

			/*if(dataCell.childNodes.length===1)
				dataTitle.style.minHeight="100px";
			else
				dataTitle.style.minHeight="30px";*/
			cellTitle.style.minWidth = "12.5%";
			cellStates.style.minWidth = "50%";
			cellTitle.style.width = "25%";
			cellStates.style.width = "75%";
		}
	}

	GenerateQSort(isCrescent: boolean, statAlign: EStatementAlignement, qConfig: Array<Tab>, statementsNumber: number, checkFunctor: Function, insertToTitleCellFunctor: Function, insertFunctor: Function)
	{
		//spesific check do before (aka froze 3 states in qInterface)
		checkFunctor();
		let states: HTMLTableElement = document.createElement("table");
		states.id="qSort";

		let tab: Array<number> = Array<number>();
		for(let key in qConfig)
			tab[tab.length]=Number.parseInt(key);
		if(isCrescent)
			tab.sort(function(a,b){return a-b;});
		else
			tab.sort(function(a,b){return b-a;});
		
		switch(statAlign)
		{
			case EStatementAlignement.Top:
			case EStatementAlignement.Bottom:
			case EStatementAlignement.Middle:
				this.GenerateQSortByColumns(states, tab, qConfig, statAlign, insertToTitleCellFunctor);
				break;
			case EStatementAlignement.Left:
			case EStatementAlignement.Right:
				this.GenerateQSortByLines(states, tab, qConfig, statAlign, insertToTitleCellFunctor);
				break;
		}
		insertFunctor(states);
		this.CheckQState(qConfig, statementsNumber);
		this.SetStatementsAlignement(statAlign);
	}

	SetStatementsAlignement(statAlign: EStatementAlignement)
	{
		let cells: HTMLCollectionOf<Element> = document.getElementsByClassName("statAlign");
		if(!cells||!cells.length)
			return;
		switch(statAlign)
		{
			case EStatementAlignement.Top:
				for (let iterator :number = 0; iterator < cells.length; iterator++)
				{
					let htmlNode: HTMLElement = <HTMLElement>cells[iterator];
					let parent: HTMLElement = <HTMLElement>htmlNode.parentNode;
					parent.style.verticalAlign = "top";
				}
				break;
			case EStatementAlignement.Bottom:
				for (let iterator: number = 0; iterator < cells.length; iterator++)
				{
					let htmlNode = <HTMLElement>cells[iterator];
					let parent: HTMLElement = <HTMLElement>htmlNode.parentNode;
					parent.style.verticalAlign = "bottom";
				}
				break;
			case EStatementAlignement.Middle:
				for (let iterator: number = 0; iterator < cells.length; iterator++)
				{
					let htmlNode: HTMLElement = <HTMLElement>cells[iterator];
					let parent: HTMLElement = <HTMLElement>htmlNode.parentNode;
					parent.style.verticalAlign = "middle";
				}
				break;
			case EStatementAlignement.Left:
				for (let iterator: number = 0; iterator < cells.length; iterator++)
				{
					let htmlNode: HTMLElement = <HTMLElement>cells[iterator];
					htmlNode.className += " nested-line-left";
				}
				break;
			case EStatementAlignement.Right:
				for (let iterator: number = 0; iterator < cells.length; iterator++)
				{
					let htmlNode: HTMLElement = <HTMLElement>cells[iterator];
					htmlNode.className += " nested-line-right";
				}
				break;
		}
	}

	GenerateThreeStates(threeStateConfig: Array<Tab>, checkFunctor: Function, insertToTitleCellFunctor: Function, insertThreeStatesFunctor: Function) 
	{
		if(document.getElementById("threestates")!==null)
			return;
		checkFunctor();
		let states: HTMLTableElement = document.createElement("table");
		states.id="threeStates";
		let rowHead: HTMLTableRowElement=states.insertRow();
		let rowBody: HTMLTableRowElement=states.insertRow();

		for (let iterator: number = 0; iterator < 3; iterator++)
		{
			let cell: HTMLTableCellElement=rowHead.insertCell();
			//cell.style.width="30%";
			let title: HTMLDivElement=cell.appendChild(document.createElement("div"));
			insertToTitleCellFunctor(threeStateConfig,title,iterator);

			cell=rowBody.insertCell();
			cell.style.verticalAlign="top";
			let div: HTMLDivElement=cell.appendChild(document.createElement("div"));
			div.id=threeStateConfig[iterator].type;
			div.className = "nestable grey nest-container";
			// TO DO check replay state
			/*if(isReplay)
			{
				div.style.minWidth=(replayScale*130)+'px';
				div.style.minHeight=(replayScale*90)+'px';
			}*/
		}
		insertThreeStatesFunctor(states);
	}

	GenerateDropZone(id: string, text: string, className: string, parent: HTMLElement, accept: string, isMultiple: boolean, functor: Function)
	{
		let dropZone: HTMLDivElement = parent.appendChild(document.createElement("div"));
		dropZone.appendChild(document.createTextNode(text+" : "));
		dropZone.className = className;
		dropZone.id=id;
		dropZone.onchange = function (event: HTMLInputEvent) { if (event.target) functor(event.target.files); };
		let form: HTMLFormElement = dropZone.appendChild(document.createElement("form"));
		let input: HTMLInputElement=form.appendChild(document.createElement("input"));
		input.type="file";
		input.multiple = isMultiple;
		input.accept=accept;
	}

	UpdateDeckView(isThreeStatesDeck: boolean)
	{
		let pickingZone: HTMLElement = document.getElementById("pickingZone");
		if(!pickingZone||!pickingZone.childNodes.length)
			return;
		if (isThreeStatesDeck) {
			let htmlNode: HTMLElement= <HTMLElement>pickingZone.firstChild;
			htmlNode.style.display = "";
			for (let iterator:number = 1; iterator < pickingZone.childNodes.length; iterator++)
			{
				htmlNode = <HTMLElement>pickingZone.childNodes[iterator];
				htmlNode.style.display = "none";
			}
		}
		else
		{
			for (let iterator:number = 0; iterator < pickingZone.childNodes.length; iterator++)
			{
				let htmlNode: HTMLElement= <HTMLElement>pickingZone.childNodes[iterator];
				htmlNode.style.display = "";
			}
		}
	}
}
