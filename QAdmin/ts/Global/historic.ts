/**
 * Basic interface & type
 */

interface IHistoricEvent {
	type: string;
	what: string;
	target: any;
}
export type HistoricEvent = IHistoricEvent;

/**
 * Local Storage 
 */

export class Historic {
	StorageName: string;
	historicalEvents: Array<HistoricEvent>;
	currentHistoricalIndex: number;
	maxHistoricalIndex: number;

	constructor(storageName: string)
	{
		this.StorageName = storageName;
		this.historicalEvents = new Array<HistoricEvent>();
	}

	static CheckStorage(): boolean
	{
		if (typeof (Storage) !== "undefined")
			return true;
		else
			return false;
	}

	SaveInStorage(data: string): boolean
	{
		if (Historic.CheckStorage()) {
			window.localStorage.setItem(this.StorageName, data);
			return true;
		}
		return false;
	}

	GetSavedData(): string
	{
		if (Historic.CheckStorage())
			return window.localStorage.QAdmin;
		return null;
	}

	ClearStorage()
	{
		if (Historic.CheckStorage())
			window.localStorage.removeItem(this.StorageName);
	}

	PopSomeHistoricalEvents() {
		this.historicalEvents.splice(this.currentHistoricalIndex, this.maxHistoricalIndex - this.currentHistoricalIndex);
	}

	DeepCloneObject(object: Object)
	{
		//this is disgusting...
		return JSON.parse(JSON.stringify(object));
	}

	StoreInHistoric(name: string, step: string, object: any)
	{
		if (this.currentHistoricalIndex != this.maxHistoricalIndex)
			this.PopSomeHistoricalEvents();
		let historicalEvent: HistoricEvent = { type: name, what: step, target: this.DeepCloneObject(object) };
		this.currentHistoricalIndex = this.historicalEvents.push(historicalEvent);
		this.maxHistoricalIndex = this.currentHistoricalIndex;
	}

	FindLastEvent(begin: number, type: string, id?: string): HistoricEvent
	{
		for (let iterator: number = begin; iterator >= 0; iterator--)
		{
			if (this.historicalEvents[iterator].type === type)
			{
				if (id)
				{
					if (this.historicalEvents[iterator].target.id === id)
						return this.historicalEvents[iterator];
					else
						continue;
				}
				return this.historicalEvents[iterator];
			}
		}
		return null;
	}

	FindNextEvent(begin: number, type: string, id?: string): HistoricEvent
	{
		for (let iterator: number = begin; iterator < this.maxHistoricalIndex; iterator++)
		{
			if (this.historicalEvents[iterator].type === type)
			{
				if (id)
				{
					if (this.historicalEvents[iterator].target.id === id)
						return this.historicalEvents[iterator];
					else
						continue;
				}
				return this.historicalEvents[iterator];
			}
		}
		return null;
	}
}