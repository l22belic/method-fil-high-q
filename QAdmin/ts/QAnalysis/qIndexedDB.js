

class QDatabase
{
	static database;

	static Open(dbName,upgradeFunctor)
	{
		QDatabase.database=null;
		new Promise(() => {
			const request = window.indexedDB.open(dbName);
			request.onload = () => function(db){QDatabase.database=db;}(request.result);
			request.onerror = () =>  function(error){alert(error);}(request.error);
			request.onupgradeneeded = () => function(db){QDatabase.database=db;upgradeFunctor()}(request.result);
		  });
	}

	/*
	*
	*	tables => array of {name:string,columns:columns}
	*	columns => array of {index:string,key:string(optionnal),params:object=>{unique:bool,multiEntry:bool}(optionnal)}
	*
	*/
	static InitialseTables(tables)
	{
		for(let iteratorTable=0;iteratorTable<tables.length;iteratorTable++)
			QDatabase.InitialseTable(tables[iteratorTable].name,tables[iteratorTable].columns);
	}

	static InitialseTable(tableName,columns)
	{
		if (!db.objectStoreNames.contains(tableName))
		{
			const table = QDatabase.database.createObjectStore(tableName)
			for(let iterator=0;iterator<columns.length;iterator++)
				table.createIndex(columns[iterator].index, columns[iterator].key, columns[iterator].params);
		}
	}

	static SetValue(tableName,value)
	{
		new Promise(() => {
			const transaction = db.transaction(tableName, 'readwrite');
			const table = transaction.objectStore(tableName);
			const request = table.put(value);
		  });
	}
	
}

/*

function setValue(db: IDBDatabase, storeName: string, value: any): Promise<Boolean> {
	return new Promise<Boolean>((resolve, reject) => {
		const transaction: IDBTransaction = db.transaction(storeName, 'readwrite');
		const store: IDBObjectStore = transaction.objectStore(storeName);
		const request: IDBRequest = store.put(value);
		request.onsuccess = event => {
			resolve(true);
		};
		request.onerror = (event: any) => {
			transaction.abort();
			reject(event.error);
		};
	});
}

function getValue<T>(db: IDBDatabase, storeName: string, key: any): Promise<T> {
	return new Promise<T>((resolve, reject) => {
		const transaction = db.transaction(storeName, 'readonly');
		const store = transaction.objectStore(storeName);
		const request = store.get(key);
		request.onsuccess = event => {
			resolve(request.result);
		};
		request.onerror = (event: any) => {
			reject(event.error);
		};
	});
}

function searchValues<T>(db: IDBDatabase, storeName: string, search: any): Promise<T[]> {
	const range = IDBKeyRange.lowerBound(search);
	let results = [];
	return new Promise<T[]>((resolve, reject) => {
		const transaction = db.transaction(storeName, 'readonly');
		const store = transaction.objectStore(storeName);
		const request = store.openCursor(range);
		request.onsuccess = (event:any) => {
			const cursor = event.target.result;
			if (cursor) {
				results.push(cursor.value);
				cursor.continue();
			} else {
				resolve(results);
			}
		};
		request.onerror = (event: any) => {
			reject(event.error);
		};
	});
}

IDBgetByIndex.ts
function getByIndex<T>(db: IDBDatabase, storeName: string, indexName: string, key: any): Promise<T[]> {
	let results = [];
	return new Promise<T[]>((resolve, reject) => {
		const transaction = db.transaction(storeName, 'readonly');
		const store = transaction.objectStore(storeName);
		const index = store.index(indexName);
		const request = index.openCursor(key);
		request.onsuccess = (event: any) => {
			const cursor = event.target.result;
			if (cursor) {
				results.push(cursor.value);
				cursor.continue();
			} else {
				resolve(results);
			}
		};
		request.onerror = (event: any) => {
			reject(event.error);
		};
	});
}*/