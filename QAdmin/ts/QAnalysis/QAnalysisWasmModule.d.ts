export declare class QAnalysisWasmModule
{
	constructor();
	AddFileToDatabase(stream: string): void;
	RestoreDatabase(stream: string): void;
	DumpDatabase(): string;
	GetPQMethodResult(): string;
	GetInterviews(): string;
	GetExogenData(): string;
	GetExogenStats(): string;
	GetSetupStats(): string;
	GetGraphsData(): string;
	GetQSortForUser(id: number): string;
	GetInterviewsForUsers(ids: Array<number>): string;
	GetExogenDataForUsers(ids: Array<number>): string;
	GetUserCount(): number;
}