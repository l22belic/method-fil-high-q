import { QAnalysisGenerator } from "./qAnalysisGenerator"
import { QAnalysisProcessor } from "./qAnalysisProcessor"
import { QAnalysisHistoric } from "./qAnalysisHistoric"
import { FileManager } from "../Global/fileManager"
import { TextEditor, textEditorDefaultSizes, textEditorDefaultColors } from "../Global/textEditor"

export default function start()
{
	let hmi: QAnalysisGenerator = new QAnalysisGenerator();

	hmi.processor = new QAnalysisProcessor();
	hmi.processor.historic = new QAnalysisHistoric("QAnalysis");

	document.body.onkeydown = function (dom: QAnalysisGenerator) {
		return function (event: KeyboardEvent) {
			if (!('keyCode' in event) || !event.ctrlKey)
				return;
			let done = true;
			switch (event.keyCode) {
				case 90: //ctrl+z UNDO
					dom.processor.Undo(dom);
					break;
				case 89: //ctrl+y REDO
					dom.processor.Redo(dom);
					break;
				case 80: //ctrl+P PRINT
					//dom.processor.GenerateConfigFile();
					break;
				case 83: //ctrl+s SAVE
					dom.processor.SaveConfig(false);
					break;
				case 70: //ctrl+F LOAD
					dom.processor.LoadConfig(dom);
					break;
				default:
					done = false;
			}
			if (done) {
				event.preventDefault();
				event.stopPropagation();
			}
		}
	}(hmi);

	FileManager.LoadFile('analysis',
		function (processor: QAnalysisProcessor) {
			return function (text: string) {
				try {
					processor.configuration = FileManager.JSONParse(text);
					return true;
				}
				catch (e) {
					console.log(e);
					return false;
				}
			};
		}(hmi.processor),
		function (dom: QAnalysisGenerator) {
			return function () {
				let olderData: string = dom.processor.historic.GetSavedData();
				hmi.editor = new TextEditor(dom.processor.configuration.textEditorPreviewText, textEditorDefaultSizes, textEditorDefaultColors, textEditorDefaultColors);
				hmi.GenerateGeneralPage();
				if (olderData && olderData !== "undefined") {
					let conf = confirm(dom.processor.configuration.reloadOlderDataText);
					if (conf)
						dom.processor.ProcessConfig(hmi, olderData);
					else
						dom.processor.historic.ClearStorage();
				}
				hmi.processor.AutoSave();
			}
		}(hmi));
}