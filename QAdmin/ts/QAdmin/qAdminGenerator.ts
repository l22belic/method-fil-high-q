import { DOMGenerator } from "../Global/domGenerator"
import { TextEditor } from "../Global/textEditor"
import { QAdminProcessor, Exogen, ParameterValue, Text, Option } from "../QAdmin/QAdminProcessor"
import { Tab, Statement } from "../Global/types";
import { QAdminHistoric } from "./qAdminHistoric";

const imageAcceptance = "image/*";
const audioAcceptance = "audio/*";
const videoAcceptance = "video/*";

/**
 * IHM
 */

export class QAdminGenerator extends DOMGenerator {

	processor: QAdminProcessor;
	editor: TextEditor;

	DeleteQSort()
	{
		if (document.getElementById("qSort") === null)
			return;
		document.getElementById("simulator").removeChild(document.getElementById("qSort"));
	}

	DeleteThreeStates()
	{
		if (document.getElementById("threestates") === null)
			return;
		document.getElementById("simulator").removeChild(document.getElementById("threestates"));
	}

	GetQSortCheckFunctor()
	{
		return function () { if (document.getElementById("qSort") !== null) document.getElementById("simulator").removeChild(document.getElementById("qSort")); };
	}

	GetQSortInsertToTitleCellFunctor(): Function
	{
		return function (dom: QAdminGenerator)
		{
			return function (qConfig: Array<Tab>, node: HTMLElement, statementsTarget: HTMLElement, iterator: number, tab: Tab, isLine: boolean)
			{
				node.id = (tab[iterator]).toString();
				let input: HTMLInputElement = node.appendChild(document.createElement("input"));
				input.id = "text_" + (tab[iterator]).toString();
				input.type = "text";
				if (qConfig[tab[iterator]].title === "") {
					input.value = (tab[iterator]).toString();
					qConfig[tab[iterator]].title = input.value;
				}
				else
					input.value = qConfig[tab[iterator]].title;
				input.onchange = function (sort: Tab) { return function (event: any) { sort.title = event.currentTarget.value; }; }(qConfig[iterator]);
				input.onblur = function (dom: QAdminGenerator, sort: Tab) { return function (event: any) { sort.title = event.currentTarget.value; dom.processor.SetQSort(); }; }(dom, qConfig[iterator]);
				input.onfocus = function (editor: TextEditor) { return function (event: any) { editor.Show(event.target); }; }(dom.editor);

				input = node.appendChild(document.createElement("input"));
				input.type = "number";
				input.className = "titleInput";
				input.value = qConfig[tab[iterator]].limit.toString();
				input.id = "input_" + (tab[iterator]).toString();
				input.onchange = function (dom: QAdminGenerator, sort: Tab, element: HTMLElement, id: number, isLine: boolean) {
					return function (event) { let oldLimit: number = sort.limit; sort.limit = Number.parseInt(event.currentTarget.value); if (oldLimit === 0) element.removeChild(element.firstChild); let length: number = element.childNodes.length; if (sort.limit > length) { for (let iterator: number = length; iterator < sort.limit; iterator++)element.appendChild(dom.CreateQStatediv(sort.limit === 0, isLine)); } else if (qConfig[id].limit < element.childNodes.length) { for (let iterator: number = sort.limit; iterator < length; iterator++)element.removeChild(element.firstChild); } } }(dom, qConfig[tab[iterator]], statementsTarget, tab[iterator], isLine);
				input.onfocus = function (processor: QAdminProcessor) { return function () { processor.SetQSort(); }; }(dom.processor);
			};
		}(this);
	}

	GetQSortInsertFunctor(): Function
	{
		return function (node: HTMLElement) { document.getElementById("simulator").appendChild(node); };
	}

	GetThreeStatesCheckFunctor(): Function
	{
		return function () { };
	}

	GetThreeStatesInsertToTitleCellFunctor(): Function
	{
		return function (dom: QAdminGenerator) {
			return function (threeStatesConfig: Array<Tab>, text: HTMLElement, iterator: number) {
				let inputttext: HTMLTextAreaElement = text.appendChild(document.createElement("textarea"));
				inputttext.id = threeStatesConfig[iterator].type;
				inputttext.style.width = "90%";
				inputttext.style.height = "100%";
				inputttext.style.textAlign = "center";
				inputttext.value = threeStatesConfig[iterator].title;
				inputttext.onfocus = function (editor: TextEditor) { return function (event: any) { editor.Show(event.target); }; }(dom.editor);
				inputttext.onchange = function (config: Tab) { return function (event: any) { config.title = event.target.value; }; }(threeStatesConfig[iterator]);
				inputttext.onblur = function (processor: QAdminProcessor, config: Tab) { return function (event: any) { config.title = event.target.value; processor.SetThreeStates(); }; }(dom.processor, threeStatesConfig[iterator])
				text.className = "tabletitle";
			};
		}(this)
	}

	GetThreeStatesInsertFunctor(): Function
	{
		return function (node: HTMLElement)
		{
			let qSort: HTMLElement = document.getElementById("qSort")
			if (qSort)
				document.getElementById("simulator").insertBefore(node, qSort);
			else
				document.getElementById("simulator").appendChild(node);
		};
	}

	AddStatement(type: number, value?: string, description?: string): Statement
	{
		let object = this.processor.GenerateStatementObject(type, value, description);
		this.GenerateCard(object);
		return object;
	}

	ShowHideContent(content: HTMLElement, style: string, button: HTMLButtonElement, text: string)
	{
		content.style.display = style;
		button.innerHTML = text;
	}

	ShowHideInterview(update?: boolean)
	{
		let content: HTMLElement = <HTMLElement>document.getElementById("interviewDiv").parentNode;
		let button: HTMLButtonElement = <HTMLButtonElement>document.getElementById("activeinterview");
		if (update)
			this.UpdateShowHide(content, "interview");
		if (this.processor.isActive.interview)
			this.ShowHideContent(content, "", button, this.processor.configuration.optionnalPartDeactivate);
		else
			this.ShowHideContent(content, "none", button, this.processor.configuration.optionnalPartActivate);
	}

	ShowHideExogen(update?: boolean)
	{
		let content: HTMLElement = <HTMLElement>document.getElementById("exoDiv").parentNode;
		let button: HTMLButtonElement = <HTMLButtonElement>document.getElementById("activeexogen");
		if (update)
			this.UpdateShowHide(content, "exogen");
		if (this.processor.isActive.exogen)
			this.ShowHideContent(content, "", button, this.processor.configuration.optionnalPartDeactivate);
		else
			this.ShowHideContent(content, "none", button, this.processor.configuration.optionnalPartActivate);
	}

	UpdateShowHide(content: HTMLElement, variable: string)
	{
		if (content.style.display === "none")
		{
			this.processor.historic.StoreInHistoric(variable, "set", true);
			this.processor.isActive[variable] = true;
		}
		else
		{
			this.processor.historic.StoreInHistoric(variable, "set", false);
			this.processor.isActive[variable] = false;
		}
	}

	GenerateGeneralPage()
	{
		let main: HTMLElement = this.GetMain();
		this.CleanMain();
		let subdiv: HTMLDivElement = document.createElement("div");
		this.GenerateDropZone("statsdropZone", this.processor.configuration.configurationDropFileText, "", subdiv, "*", true, function (dom: QAdminGenerator) { return function (files: FileList) { dom.processor.ProcessConfigFile(files[0], dom); }; }(this));
		main.appendChild(this.GenSubTitle("0." +  this.processor.configuration.loadExistingConfigurationFile, subdiv,  this.processor.configuration.loadExistingConfigurationFileDesc));

		let table: HTMLTableElement = document.createElement("table");
		table.style.width = "100%";
		let row: HTMLTableRowElement = table.insertRow();
		let cell: HTMLTableCellElement = row.insertCell();
		cell.style.width = "35%";
		this.GenerateDropZone("statsdropZone", this.processor.configuration.statementDropFileText, "", cell, "*", true, function (dom: QAdminGenerator) { return function (files: FileList) { dom.processor.ProcessFileList(files[0], dom);}; }(this));
		cell = row.insertCell();
		cell.style.width = "35%";
		let button: HTMLButtonElement = cell.appendChild(document.createElement("button"));
		button.appendChild(document.createTextNode(this.processor.configuration.addStatementText));
		button.onclick = function (dom: QAdminGenerator) { return function () { let object: Statement = dom.AddStatement(dom.processor.statementType); dom.processor.historic.StoreInHistoric("statement", "create", object) }; }(this);
		let radio: HTMLDivElement = cell.appendChild(document.createElement("div"));
		radio.id = "statementType";
		for (let iterator: number = 0; iterator < this.processor.configuration.statementTypes.length; iterator++)
		{
			let rInput: HTMLInputElement = document.createElement("input");
			rInput.value = this.processor.configuration.statementTypes[iterator].value;
			rInput.type = "radio";
			rInput.name = "statementType";
			rInput.id = this.processor.configuration.statementTypes[iterator].text;
			rInput.onchange = function (processor: QAdminProcessor) { return function (event: any) { processor.statementType = Number.parseInt(event.currentTarget.value); }; }(this.processor);
			if (iterator === 0)
				rInput.checked = true;
			radio.appendChild(rInput);
			let label: HTMLLabelElement = document.createElement("label");
			label.innerHTML =  this.processor.configuration.statementTypes[iterator].text;
			label.htmlFor = rInput.id;
			radio.appendChild(label);
		}
		cell = row.insertCell();
		cell.style.width = "10%";
		let checkbox: HTMLInputElement = cell.appendChild(document.createElement("input"));
		checkbox.type = "checkbox";
		checkbox.name = "showStatementFirst";
		checkbox.id = "showStatementFirst";
		checkbox.checked = true;
		checkbox.onchange = function (generator: QAdminGenerator) { return function (event: any) { generator.processor.isPresentation = event.target.checked; generator.UpdateWallOfTexts(); generator.processor.historic.StoreInHistoric("showStatementFirst", "set", event.target.checked); }; }(this);
		let label: HTMLLabelElement = document.createElement("label");
		label.innerHTML = this.processor.configuration.showAllStatementsFirst;
		label.htmlFor = "showStatementFirst";
		cell.appendChild(label);
		cell = row.insertCell();
		cell.style.width = "10%";
		button = cell.appendChild(document.createElement("button"));
		button.appendChild(document.createTextNode(this.processor.configuration.downloadStaFile));
		button.onclick = function (processor: QAdminProcessor) { return function () { processor.GenerateStatementsFile() }; }(this.processor);
		cell.appendChild(document.createElement("br"));
		cell.appendChild(document.createElement("br"));
		button = cell.appendChild(document.createElement("button"));
		button.appendChild(document.createTextNode(this.processor.configuration.resetText));
		button.onclick = function (dom: QAdminGenerator) { return function () { dom.processor.historic.StoreInHistoric("statement", "reset", dom.processor.statements); dom.processor.globalCounter = 0; dom.processor.statements = new Array<Statement>(); dom.CleanNodeByName("pickingZone"); }; }(this);
		subdiv = document.createElement("div");
		subdiv.appendChild(table);
		let div: HTMLDivElement = subdiv.appendChild(document.createElement("div"));
		div.id = "pickingZone";
		main.appendChild(this.GenSubTitle("1." +  this.processor.configuration.statementsTitle, subdiv,  this.processor.configuration.statementsTitleDesc));

		table = document.createElement("table");
		table.style.width = "100%";
		row = table.insertRow();
		cell = row.insertCell();
		cell.style.width = "45%";
		button = cell.appendChild(document.createElement("button"));
		button.appendChild(document.createTextNode(this.processor.configuration.threeStateText));
		button.onclick = function (dom: QAdminGenerator) { return function () { if (!dom.processor.threeStates.length) { dom.processor.historic.StoreInHistoric("threestates", "create", dom.processor.threeStates); dom.processor.GenerateThreeStates(); dom.GenerateThreeStates(dom.processor.threeStates, dom.GetThreeStatesCheckFunctor(), dom.GetThreeStatesInsertToTitleCellFunctor(), dom.GetThreeStatesInsertFunctor()); } else { dom.processor.historic.StoreInHistoric("threestates", "delete", dom.processor.threeStates); dom.processor.threeStates = new Array<Tab>(); dom.DeleteThreeStates() } }; }(this);
		radio = cell.appendChild(document.createElement("div"));
		radio.appendChild(document.createTextNode( this.processor.configuration.threeStateIsDeckText));
		radio.id = "threeStatesDeck";
		for (let iterator: number = 0; iterator < this.processor.configuration.threeStateIsDeck.length; iterator++) {
			let rInput = document.createElement("input");
			rInput.value =  this.processor.configuration.threeStateIsDeck[iterator].value;
			rInput.type = "radio";
			rInput.name = "threeStatesDeck";
			rInput.id = this.processor.configuration.threeStateIsDeck[iterator].text;
			rInput.onchange = function (dom: QAdminGenerator) { return function (event) { dom.processor.isThreeStatesDeck = Number.parseInt(event.currentTarget.value) > 0; dom.processor.historic.StoreInHistoric("threeStatesDeck", "set", dom.processor.isThreeStatesDeck); dom.UpdateDeckView(dom.processor.isThreeStatesDeck); }; }(this);
			if (iterator === 0)
				rInput.checked = true;
			radio.appendChild(rInput);
			let label: HTMLLabelElement = document.createElement("label");
			label.innerHTML =  this.processor.configuration.threeStateIsDeck[iterator].text;
			label.htmlFor = rInput.id;
			radio.appendChild(label);
		}
		cell = row.insertCell();
		cell.style.width = "45%";
		button = cell.appendChild(document.createElement("button"));
		button.appendChild(document.createTextNode(this.processor.configuration.generateQSort));
		button.onclick = function (dom: QAdminGenerator) { return function () { if (!dom.processor.statements.length) { alert(dom.processor.configuration.needStatements); return;} if (!Object.keys(dom.processor.qSort).length) { dom.processor.qSort = dom.QTabPlacement(Number.parseInt((<HTMLInputElement>document.getElementById("qsortmin")).value), Number.parseInt((<HTMLInputElement>document.getElementById("qsortmax")).value), dom.processor.statements.length); dom.GenerateQSort(dom.processor.isCrescent, dom.processor.statAlign, dom.processor.qSort, dom.processor.statements.length, dom.GetQSortCheckFunctor(), dom.GetQSortInsertToTitleCellFunctor(), dom.GetQSortInsertFunctor()); dom.processor.historic.StoreInHistoric("qsort", "create", dom.processor.qSort); } else if (document.getElementById("qSort")) { dom.processor.historic.StoreInHistoric("qsort", "delete", dom.processor.qSort); dom.processor.qSort = new Array<Tab>(); dom.DeleteQSort(); } };}(this);
		let inputGroup: HTMLDivElement = cell.appendChild(document.createElement("div"));
		inputGroup.appendChild(document.createTextNode(this.processor.configuration.qSortMinValue + " : "));
		let input: HTMLInputElement = inputGroup.appendChild(document.createElement("input"));
		input.type = "number";
		input.id = "qsortmin";
		input.value = (-3).toString();
		input.onchange = function (dom: QAdminGenerator) { return function (event: any) { dom.processor.historic.StoreInHistoric("qsortmin", "set", event.target.value); if (event.target.value > 0) event.target.value = -event.target.value; (<HTMLInputElement>document.getElementById("qsortmax")).value = (-Number.parseInt(event.target.value)).toString(); dom.processor.qSort = dom.QTabPlacement(Number.parseInt((<HTMLInputElement>document.getElementById("qsortmin")).value), Number.parseInt((<HTMLInputElement>document.getElementById("qsortmax")).value), dom.processor.statements.length); dom.processor.historic.StoreInHistoric("qsort", "set", dom.processor.qSort); }; }(this);
		inputGroup = cell.appendChild(document.createElement("div"));
		inputGroup.appendChild(document.createTextNode( this.processor.configuration.qSortMaxValue + " : "));
		input = inputGroup.appendChild(document.createElement("input"));
		input.onchange = function (dom: QAdminGenerator) { return function () { dom.processor.qSort=dom.QTabPlacement(Number.parseInt((<HTMLInputElement>document.getElementById("qsortmin")).value), Number.parseInt((<HTMLInputElement>document.getElementById("qsortmax")).value), dom.processor.statements.length); dom.GenerateQSort(dom.processor.isCrescent, dom.processor.statAlign, dom.processor.qSort, dom.processor.statements.length, dom.GetQSortCheckFunctor(), dom.GetQSortInsertToTitleCellFunctor(), dom.GetQSortInsertFunctor()); dom.processor.historic.StoreInHistoric("qsort", "set", dom.processor.qSort); }; }(this);
		input.type = "number";
		input.id = "qsortmax";
		input.value = (3).toString();
		radio = cell.appendChild(document.createElement("div"));
		radio.appendChild(document.createTextNode( this.processor.configuration.headerStyleText));
		radio.id = "headerStyle";
		for (let iterator: number = 0; iterator <  this.processor.configuration.headerStyle.length; iterator++) {
			let rInput: HTMLInputElement = document.createElement("input");
			rInput.value =  this.processor.configuration.headerStyle[iterator].value;
			rInput.type = "radio";
			rInput.name = "headerStyle";
			rInput.id = this.processor.configuration.headerStyle[iterator].text;
			rInput.onchange = function (dom: QAdminGenerator) { return function (event) { dom.processor.isCrescent = Number.parseInt(event.currentTarget.value) === 0; dom.processor.historic.StoreInHistoric("isCrescent", "set", dom.processor.isCrescent); dom.GenerateQSort(dom.processor.isCrescent, dom.processor.statAlign, dom.processor.qSort, dom.processor.statements.length, dom.GetQSortCheckFunctor(), dom.GetQSortInsertToTitleCellFunctor(), dom.GetQSortInsertFunctor()); }; }(this);
			if (iterator === 0)
				rInput.checked = true;
			radio.appendChild(rInput);
			let label: HTMLLabelElement = document.createElement("label");
			label.innerHTML =  this.processor.configuration.headerStyle[iterator].text;
			label.htmlFor = rInput.id;
			radio.appendChild(label);
		}
		radio = cell.appendChild(document.createElement("div"));
		radio.appendChild(document.createTextNode( this.processor.configuration.statementsAlignementText));
		radio.id = "alignementType";
		for (let iterator: number = 0; iterator < this.processor.configuration.statementsAlignement.length; iterator++)
		{
			let rInput: HTMLInputElement = document.createElement("input");
			rInput.value =  this.processor.configuration.statementsAlignement[iterator].value;
			rInput.type = "radio";
			rInput.name = "alignementType";
			rInput.id = this.processor.configuration.statementsAlignement[iterator].text;
			rInput.onchange = function (dom: QAdminGenerator) { return function (event) { dom.processor.statAlign = Number.parseInt(event.currentTarget.value); dom.processor.historic.StoreInHistoric("statAlign", "set", dom.processor.statAlign); dom.GenerateQSort(dom.processor.isCrescent, dom.processor.statAlign, dom.processor.qSort, dom.processor.statements.length, dom.GetQSortCheckFunctor(), dom.GetQSortInsertToTitleCellFunctor(), dom.GetQSortInsertFunctor());};}(this);
			if (iterator === 0)
				rInput.checked = true;
			radio.appendChild(rInput);
			let label: HTMLLabelElement = document.createElement("label");
			label.innerHTML = this.processor.configuration.statementsAlignement[iterator].text;
			label.htmlFor = rInput.id;
			radio.appendChild(label);
		}
		cell = row.insertCell();
		cell.style.width = "10%";
		button = cell.appendChild(document.createElement('button'))
		button.appendChild(document.createTextNode(this.processor.configuration.resetText));
		button.onclick = function (dom: QAdminGenerator) { return function (event) { dom.processor.historic.StoreInHistoric("threestates", "delete", dom.processor.threeStates); dom.processor.historic.StoreInHistoric("qsort", "delete", dom.processor.qSort); dom.processor.qSort = new Array<Tab>(); dom.processor.threeStates = new Array<Tab>(); dom.CleanNodeByName("simulator"); }; }(this);
		subdiv = document.createElement("div");
		subdiv.appendChild(table);
		div = subdiv.appendChild(document.createElement("div"));
		div.id = "simulator";
		main.appendChild(this.GenSubTitle("2." +  this.processor.configuration.phasesTitle, subdiv,  this.processor.configuration.phasesTitleDesc));

		table = document.createElement("table");
		table.style.width = "100%";
		row = table.insertRow();
		cell = row.insertCell();
		checkbox = cell.appendChild(document.createElement("input"));
		cell.style.width = "15%";
		checkbox.type = "checkbox";
		checkbox.name = "isTraceActive";
		checkbox.id = "isTraceActive";
		checkbox.checked = true;
		checkbox.onchange = function (historic: QAdminHistoric) { return function (event: any) { historic.StoreInHistoric("isActiveTrace", "set", event.target.checked); }; }(this.processor.historic);
		label = document.createElement("label");
		label.innerHTML = this.processor.configuration.isActiveTraceText;
		label.htmlFor = "isActiveTrace";
		cell.appendChild(label);
		cell = row.insertCell();
		cell.style.width = "45%";
		div = cell.appendChild(document.createElement("div"));
		div.appendChild(document.createTextNode(this.processor.configuration.interviewCalcTypeText));
		radio = cell.appendChild(document.createElement("div"));
		radio.id = "interviewType";
		for (let iterator: number = 0; iterator < this.processor.configuration.interviewCalcType.length; iterator++) {
			let rInput: HTMLInputElement = document.createElement("input");
			rInput.value =  this.processor.configuration.interviewCalcType[iterator].value;
			rInput.type = "radio";
			rInput.name = "interviewType";
			rInput.id =  this.processor.configuration.interviewCalcType[iterator].text;
			if (iterator === 0) {
				rInput.onchange = function (processor: QAdminProcessor) { return function (event: any) { (<HTMLInputElement>document.getElementById("interviewInput")).disabled = true; processor.interviewType = Number.parseInt(event.currentTarget.value); processor.SetInterviewState(); }; }(this.processor);
				rInput.checked = true;
			}
			else
				rInput.onchange = function (processor: QAdminProcessor) { return function (event: any) { (<HTMLInputElement>document.getElementById("interviewInput")).disabled = false; processor.interviewType = Number.parseInt(event.currentTarget.value); processor.SetInterviewState(); }; }(this.processor);
			radio.appendChild(rInput);
			let label: HTMLLabelElement = document.createElement("label");
			label.innerHTML =  this.processor.configuration.interviewCalcType[iterator].text;
			label.htmlFor = rInput.id;
			radio.appendChild(label);
		}
		cell = row.insertCell();
		cell.style.width = "30%";
		div = cell.appendChild(document.createElement("div"));
		div.appendChild(document.createTextNode( this.processor.configuration.interviewNumberOfStatementText));
		input = div.appendChild(document.createElement("input"))
		input.type = "number";
		input.id = "interviewInput";
		input.disabled = true;
		input.onchange = function (processor: QAdminProcessor) { return function (event) { processor.interviewNumber = Number.parseInt(event.target.value); }; }(this.processor)
		input.onblur = function (processor: QAdminProcessor) { return function () { processor.SetInterviewState(); }; }(this.processor);
		cell = row.insertCell();
		cell.style.width = "10%";
		button = cell.appendChild(document.createElement("button"));
		button.appendChild(document.createTextNode(this.processor.configuration.resetText));
		button.onclick = function (processor: QAdminProcessor) { return function () { processor.interviewType = 0; processor.interviewNumber = 0; this.SetInterview(); processor.SetInterviewState(); }; }(this.processor);
		subdiv = document.createElement("div");
		subdiv.appendChild(table);
		div = subdiv.appendChild(document.createElement("div"));
		div.id = "interviewDiv";
		let form: HTMLTableElement = div.appendChild(document.createElement("table"));
		form.id = "interviewTable";
		let functor: Function = function (dom: QAdminGenerator) { return function () { dom.ShowHideInterview(true); dom.UpdateWallOfTexts();}; }(this);
		main.appendChild(this.GenSubTitle("3." + this.processor.configuration.interviewTitle, subdiv, this.processor.configuration.interviewTitleDesc, "interview", this.processor.configuration.optionnalPartDeactivate, functor));

		table = document.createElement("table");
		table.style.width = "100%";
		row = table.insertRow();
		cell = row.insertCell();
		cell.style.width = "80%";
		button = cell.appendChild(document.createElement("button"));
		button.appendChild(document.createTextNode(this.processor.configuration.addExoQuizzText));
		button.onclick = function (dom: QAdminGenerator) { return function (event) { dom.AddExoQuizz(dom.processor.exoType); dom.processor.historic.StoreInHistoric("exoquizz", "set", dom.processor.exoQuizz); }; }(this)
		radio = cell.appendChild(document.createElement("div"));
		radio.id = "exoType";
		for (let iterator: number = 0; iterator <  this.processor.configuration.exoQuizzTypes.length; iterator++) {
			let rInput = document.createElement("input");
			rInput.value =  this.processor.configuration.exoQuizzTypes[iterator].value;
			rInput.type = "radio";
			rInput.name = "exoType";
			rInput.id = this.processor.configuration.exoQuizzTypes[iterator].text;
			rInput.onchange = function (processor: QAdminProcessor) { return function (event) { processor.exoType = Number.parseInt(event.currentTarget.value); }; }(this.processor);
			if (iterator === 0)
				rInput.checked = true;
			radio.appendChild(rInput);
			let label: HTMLLabelElement = document.createElement("label");
			label.innerHTML = this.processor.configuration.exoQuizzTypes[iterator].text;
			label.htmlFor = rInput.id;
			radio.appendChild(label);
		}
		cell = row.insertCell();
		cell.style.width = "10%";
		checkbox = cell.appendChild(document.createElement("input"));
		checkbox.type = "checkbox";
		checkbox.name = "isSendButtonsAtExogen";
		checkbox.id = "isSendButtonsAtExogen";
		checkbox.onchange = function (dom: QAdminGenerator) { return function (event: any) { dom.processor.isSendButtonsAtExogen = event.target.checked; dom.processor.historic.StoreInHistoric("isSendButtonsAtExogen", "set", event.target.checked); dom.UpdateWallOfTexts(); }; }(this);
		label = document.createElement("label");
		label.innerHTML = this.processor.configuration.isSendButtonsAtExogen;
		label.htmlFor = "isSendButtonsAtExogen";
		cell.appendChild(label);
		cell = row.insertCell();
		cell.style.width = "10%";
		button = cell.appendChild(document.createElement("button"));
		button.appendChild(document.createTextNode(this.processor.configuration.resetText));
		button.onclick = function (dom: QAdminGenerator) {return function () { dom.processor.historic.StoreInHistoric("exoquizz", "delete", dom.processor.exoQuizz); dom.processor.exoQuizz = new Array<Exogen>(); dom.DeleteExoView(); }; }(this);
		subdiv = document.createElement("div");
		subdiv.appendChild(table);
		div = subdiv.appendChild(document.createElement("div"));
		div.id = "exoDiv";
		form = div.appendChild(document.createElement("table"));
		form.id = "exoTable";
		functor = function (dom: QAdminGenerator) { return function () { dom.ShowHideExogen(true); dom.UpdateWallOfTexts();}; }(this);
		main.appendChild(this.GenSubTitle("4." + this.processor.configuration.exogenTitle, subdiv, this.processor.configuration.exogenTitleDesc, "exogen", this.processor.configuration.optionnalPartDeactivate, functor));

		table = document.createElement("table");
		table.style.width = "100%";
		row = table.insertRow();
		cell = row.insertCell();
		cell.style.width = "90%";
		button = cell.appendChild(document.createElement("button"));
		button.appendChild(document.createTextNode(this.processor.configuration.generateTextsList));
		button.onclick = function (dom: QAdminGenerator) { return function () { let table: HTMLElement = document.getElementById("textsTable"); if (table) { if (table.style.display === "") table.style.display = "none"; else table.style.display = ""; } else { dom.processor.historic.StoreInHistoric("texts", 'create', dom.processor.texts); dom.GenerateWallOfTexts(); } }; }(this);
		cell = row.insertCell();
		cell.style.width = "10%";
		button = cell.appendChild(document.createElement("button"));
		button.appendChild(document.createTextNode(this.processor.configuration.resetText));
		button.onclick = function (dom: QAdminGenerator) {return function () { dom.processor.historic.StoreInHistoric("texts", 'delete', dom.processor.texts); dom.processor.texts = new Array<Text>(); this.DeleteWallOfTexts(); };}(this);
		subdiv = document.createElement("div");
		subdiv.appendChild(table);
		div = subdiv.appendChild(document.createElement("div"));
		div.id = "texts";
		main.appendChild(this.GenSubTitle("5." +  this.processor.configuration.textsTitle, subdiv,  this.processor.configuration.textsTitleDesc));

		table = document.createElement("table");
		table.style.width = "100%";
		row = table.insertRow();
		cell = row.insertCell();
		cell.style.width = "100%";

		button = cell.appendChild(document.createElement("button"));
		button.appendChild(document.createTextNode(this.processor.configuration.generateConfigurationFile));
		button.onclick = function (processor: QAdminProcessor) { return function () { processor.GenerateConfigFile() }; }(this.processor);
		cell.appendChild(document.createElement("br"));
		cell.appendChild(document.createElement("br"));
		button = cell.appendChild(document.createElement("button"));
		button.appendChild(document.createTextNode( this.processor.configuration.saveDataText));
		button.onclick = function (processor: QAdminProcessor) { return function () { processor.SaveConfig(false) }; }(this.processor);
		cell.appendChild(document.createElement("br"));
		cell.appendChild(document.createElement("br"));
		button = cell.appendChild(document.createElement("button"));
		button.appendChild(document.createTextNode(this.processor.configuration.loadDataText));
		button.onclick = function (dom: QAdminGenerator) { return function () { dom.processor.LoadConfig(dom) }; }(this);
		subdiv = document.createElement("div");
		subdiv.appendChild(table);
		main.appendChild(this.GenSubTitle("6." +  this.processor.configuration.generalTitle, subdiv,  this.processor.configuration.generalTitleDesc));
	}

	GenerateMediaControl(parent: HTMLElement, id: number, type: string, source: string): HTMLMediaElement
	{
		let media: HTMLMediaElement = <HTMLMediaElement>parent.appendChild(document.createElement(type));
		if (type !== "img")
		{
			media.id = type + '-player-' + id;
			media.controls = true;
			/*media.controlsList = "nodownload";*/
		}
		if (source in this.processor.mediacontents)
			media.src = this.processor.mediacontents[source];
		else
			media.src = source;
		media.onclick = function (event) { event.stopPropagation(); };
		return media;
	}

	GenerateMediaStatement(statement: Statement): HTMLElement
	{
		let media: HTMLDivElement;
		let div: HTMLDivElement = document.createElement("div");
		if (statement.value.length < 1)
		{
			media = document.createElement("div");
			switch (statement.type) {
				case "audio":
					this.GenerateDropZone("dropZone" + statement.id, this.processor.configuration.statementDropAudioText, "", media, audioAcceptance, false, function (dom: QAdminGenerator, id: number) { return function (files: FileList) { dom.processor.ProcessAudioFile(dom, files[0], id) }; }(this, statement.id));
					if ("illustration" in statement) {
						if (statement.illustration.length < 1)
							this.GenerateDropZone("dropZone" + statement.id, this.processor.configuration.statementDropImageText, "", media, imageAcceptance, false, function (dom: QAdminGenerator, id: number) { return function (files: FileList) { dom.processor.ProcessImageFile(dom, files[0], id) }; }(this, statement.id));
						else
							this.GenerateMediaControl(media, statement.id, "img", statement.illustration);
					}
					break;
				case "video":
					this.GenerateDropZone("dropZone" + statement.id, this.processor.configuration.statementDropVideoText, "", media, videoAcceptance, false, function (dom: QAdminGenerator, id: number) { return function (event: any) { let dt = event.dataTransfer; let files = dt.files; dom.processor.ProcessVideoFile(dom, files[0], id) }; }(this, statement.id));
					break;
				case "img":
					this.GenerateDropZone("dropZone" + statement.id, this.processor.configuration.statementDropImageText, "", media, imageAcceptance, false, function (dom: QAdminGenerator, id: number) { return function (files: FileList) { dom.processor.ProcessImageFile(dom, files[0], id) }; }(this, statement.id));
					break;
			}
			div.appendChild(media);
		}
		else {
			this.GenerateMediaControl(div, statement.id, statement.type, statement.value);
			if ('description' in statement) {
				let description = document.createElement("textarea");
				description.value = statement.description;
				description.onclick = function (event: any) { event.preventDefault(); event.stopPropagation(); };
				description.onfocus = function (editor: TextEditor) { return function (event: any) { editor.Show(event.target); }; }(this.editor);
				description.onchange = function (processor: QAdminProcessor, id: number) { return function (event) { processor.GetStatement(id).description = event.target.value; }; }(this.processor, statement.id);
				div.appendChild(description);
				return div;
			}
			else if ('illustration' in statement) {
				if (statement.illustration.length < 1)
					this.GenerateDropZone("dropZone" + statement.id, this.processor.configuration.statementDropImageText, "", div, imageAcceptance, false, function (dom: QAdminGenerator, id: number) { return function (files: FileList) { dom.processor.ProcessImageFile(dom, files[0], id) }; }(this, statement.id));
				else
					this.GenerateMediaControl(div, statement.id, "img", statement.illustration);
			}
		}
		if ('description' in statement) {
			let description = document.createElement("textarea");
			description.value = statement.description;
			description.style.width = "90%";
			description.style.height = "90%";
			description.onclick = function (event: any) { event.preventDefault(); event.stopPropagation(); };
			description.onfocus = function (editor: TextEditor) { return function (event: any) { editor.Show(event.target); }; }(this.editor);
			description.onchange = function (processor: QAdminProcessor, id: number) { return function (event: any) { processor.GetStatement(id).description = event.target.value; }; }(this.processor, statement.id);
			description.onblur = function (processor: QAdminProcessor, id: number) { return function (event: any) { processor.SetStatementDescription(id); }; }(this.processor, statement.id);
			div.appendChild(description);
			let button = document.createElement("button");
			button.appendChild(document.createTextNode(this.processor.configuration.removeDescriptionText));
			button.onclick = function (dom: QAdminGenerator, statement: Statement) { return function (event) { delete statement.description; dom.ReloadCard(statement.id); event.preventDefault(); event.stopPropagation(); }; }(this, statement);
			div.appendChild(button);
		}
		else if (!("illustration" in statement)) {
			let button = document.createElement("button");
			button.appendChild(document.createTextNode( this.processor.configuration.addDescriptionText));
			button.onclick = function (dom: QAdminGenerator, statement: Statement) { return function (event) { statement["description"] = ""; dom.ReloadCard(statement.id); event.preventDefault(); event.stopPropagation(); }; }(this, statement);
			div.appendChild(button);
		}
		return div;
	}

	GenerateStatement(statement: Statement): HTMLElement
	{
		if (!statement)
			return null;
		if (statement.type === "text") {
			let text: HTMLTextAreaElement = document.createElement("textarea");
			text.value = statement.value;
			text.onfocus = function (editor: TextEditor) { return function (event: any) { editor.Show(event.target); }; }(this.editor);
			text.onchange = function (statement: Statement) { return function (event: any) { statement.value = event.target.value; }; }(statement);
			text.onblur = function (historic: QAdminHistoric, statement: Statement) { return function (event: any) { statement.value = event.target.value; historic.StoreInHistoric('statement', 'set', statement); }; }(this.processor.historic, statement);
			text.onclick = function (event: any) { event.preventDefault(); event.stopPropagation(); };
			return text;
		}
		else
			return this.GenerateMediaStatement(statement);
		alert( this.processor.configuration.unknowstatementype);
		return null;
	}

	DeleteCard(id: number)
	{
		let card: HTMLElement = document.getElementById(id.toString());
		if (card === null)
			return;
		let pickingZone = document.getElementById("pickingZone");
		pickingZone.removeChild(card);
	}

	GenerateEmptyCard(id: number)
	{
		let pickingZone: HTMLElement = document.getElementById("pickingZone");
		let li: HTMLDivElement = document.createElement('div');
		li.className = "nested-item";
		li.oncontextmenu = function (processor: QAdminProcessor, id: number) { return function (event: any) { event.preventDefault(); event.stopPropagation(); if (confirm(processor.configuration.deleteStatementText)) { processor.DeleteStatement(id); this.DeleteCard(id); } }; }(this.processor, id);
		li.id = this.processor.globalCounter.toString();
		this.processor.globalCounter += 1;
		pickingZone.appendChild(li);
	}

	GenerateCard(statement: Statement)
	{
		if (statement === null)
			return;
		let pickingZone = document.getElementById("pickingZone");
		let li = document.createElement('div');
		li.appendChild(this.GenerateStatement(statement));
		li.className = "nested-item";
		li.oncontextmenu = function (dom: QAdminGenerator, id: number) { return function (event: any) { event.preventDefault(); event.stopPropagation(); if (confirm(dom.processor.configuration.deleteStatementText)) { dom.processor.DeleteStatement(id); dom.DeleteCard(id); } }; }(this, statement.id);
		li.id = statement.id.toString();
		if (this.processor.globalCounter < statement.id)
			this.processor.globalCounter = statement.id + 1;
		pickingZone.appendChild(li);
	}

	ReloadCard(id: number)
	{
		let card: HTMLElement = document.getElementById(id.toString());
		if (card === null) {
			console.log("card\t" + id);
			return;
		}
		while (card.childNodes.length)
			card.removeChild(card.firstChild);
		let statement: HTMLElement = this.GenerateStatement(this.processor.GetStatement(id));
		if (statement)
			card.appendChild(statement);
		else
			console.log("statement\t" + id);
	}

	GenerateCards()
	{
		let pickingZone: HTMLElement = document.getElementById("pickingZone");
		pickingZone.className = "nestable";
		pickingZone.style.backgroundColor = "none";
		while (pickingZone.childNodes.length)
			pickingZone.removeChild(pickingZone.firstChild);
		for (let key in this.processor.statements)
			this.GenerateCard(this.processor.statements[key]);
	}

	AppendQuizzValues(div: HTMLElement, quizz: Exogen)
	{
		let input: HTMLInputElement;
		for (let iterator in quizz.values)
		{
			input = div.appendChild(document.createElement("input"));
			input.type = "text";
			input.value = quizz.values[iterator];
		}
	}

	AppendQuizzParameters(div: HTMLElement, quizz: Exogen)
	{
		for (let iterator in quizz.parameters)
		{
			switch (quizz.parameters[iterator].name) {
				case "required":
				case "min":
				case "max":
				case "step":
				case "max-length":
					break;
				default:
					div.appendChild(this.AddDefaultExoParameter(quizz.parameters[Number.parseInt(iterator)]));
			}
		}
	}

	AddDefaultExoParameter(parameter: ParameterValue): HTMLElement
	{
		let div: HTMLDivElement = document.createElement("div");
		div.id = "parameter_" + this.processor.exoCounter;
		div.appendChild(document.createTextNode( this.processor.configuration.exoquizzParameterNameText + " : "));
		let input = div.appendChild(document.createElement("input"));
		input.type = "text";
		input.onchange = function (processor: QAdminProcessor, id: string) { return function (event: any) { let quizz: Exogen = processor.GetExoQuizz(id); let parameter: ParameterValue = processor.GetExoQuizzParameter(quizz, event.target.defaultValue); if (!parameter) { let object: ParameterValue; object.name = event.target.value; quizz.parameters.add(object); } else parameter.name = event.target.value; event.target.defaultValue = event.target.value }; }(this.processor, parameter.name);
		input.id = "name" + this.processor.exoCounter;
		if (parameter.name)
			input.value = parameter.name;
		div.appendChild(document.createElement("br"));
		div.appendChild(document.createTextNode( this.processor.configuration.exoquizzParameterValueText + " : "));
		input = div.appendChild(document.createElement("input"));
		input.id = "value" + this.processor.exoCounter;
		input.type = "text";
		input.onchange = function (processor: QAdminProcessor, id: string, nameNode: string) { return function (event: any) { let quizz: Exogen = processor.GetExoQuizz(id); let name: string = document.getElementById(nameNode).nodeValue; let parameter: ParameterValue = processor.GetExoQuizzParameter(quizz, name); if (!parameter) { let object: ParameterValue; object.name = name; object.value = event.target.value; quizz.parameters.add(object); } else parameter.value = event.target.value; }; }(this.processor, parameter.name, "name" + this.processor.exoCounter);
		if (parameter.value)
			input.value = parameter.value;
		this.processor.exoCounter += 1;
		return div;
	}

	AddExoParameter(id: string, type: string, labeltext: string, quizz: Exogen): HTMLElement
	{
		let parameter: ParameterValue = this.processor.GetExoQuizzParameter(quizz, id);
		let div: HTMLDivElement = document.createElement("div");
		div.id = id;
		div.appendChild(document.createTextNode(labeltext));
		let input: HTMLInputElement = div.appendChild(document.createElement("input"));
		input.type = type;
		switch (type)
		{
			case "number":
			case "text":
				input.value = parameter ? parameter.value:"";
				break;
			case "checkbox":
				input.checked = parameter ? (Number.parseInt(parameter.value) > 0 || parameter.value === 'true' || parameter.value) : false;
				break;
		}
		input.id = labeltext + this.processor.exoCounter;
		return div;
	}

	AddExoQuizz(type: number)
	{
		let object: Exogen = { id: this.processor.exoCounter.toString(), type: "" };
		let id = this.processor.exoCounter;
		switch (type) {
			case 0:
				object.type = "text";
				break;
			case 1:
				object.type = "number";
				break;
			case 2:
				object.type = "radio";
				object.values = [];
				break;
			case 3:
				object.type = "checkbox";
				object.values = [];
				break;
			case 4:
				object.type = "range";
				object.values = [];
				break;
			default:
				return;
		}
		this.processor.exoCounter++;
		this.processor.exoQuizz.push(object);
		this.GenerateExoQuizz(object, id);
	}

	GenerateExoQuizz(quizz: Exogen, id: number)
	{
		let div: HTMLElement = document.getElementById("exoDiv");
		let form: HTMLTableElement = <HTMLTableElement>document.getElementById("exoTable");
		if (!form)
		{
			form = div.appendChild(document.createElement("table"));
			form.id = "exoTable";
		}
		let row: HTMLTableRowElement = form.insertRow();
		let cell: HTMLTableCellElement = row.insertCell();
		let celltemp: HTMLTableCellElement = row.insertCell();
		let cellSim: HTMLTableCellElement = row.insertCell();
		cell.style.width = "22%";
		cell.style.textAlign = "right";
		cell.appendChild(document.createElement("div"));
		div = cell.appendChild(document.createElement("div"));
		div.id = "id";
		div.appendChild(document.createTextNode( this.processor.configuration.exoquizzIdText + " : "));
		let input: HTMLInputElement = div.appendChild(document.createElement("input"));
		input.type = "text";
		input.value = quizz.id;
		input.onchange = function (id: number, dom: QAdminGenerator, cell: HTMLTableCellElement) { return function (event: any) { dom.processor.exoQuizz[id].id = event.target.value; dom.GenerateExoSimulation(cell, dom.processor.exoQuizz[id]);};  }(id, this, cellSim);
		input.onblur = function (processor: QAdminProcessor) { return function () { processor.historic.StoreInHistoric("exoquizz", "set", processor.exoQuizz); }; }(this.processor);
		div = cell.appendChild(document.createElement("div"));
		div.id = "text";
		div.appendChild(document.createTextNode( this.processor.configuration.exoquizzLabelText + " : "));
		input = div.appendChild(document.createElement("input"));
		input.type = "text";
		input.value = quizz.text;
		input.onchange = function (id: number, dom: QAdminGenerator, cell: HTMLTableCellElement) { return function (event: any) { dom.processor.exoQuizz[id].text = event.target.value; dom.GenerateExoSimulation(cell, dom.processor.exoQuizz[id]);}; }(id, this, cellSim);
		input.onblur = function (processor: QAdminProcessor) { return function () { processor.historic.StoreInHistoric("exoquizz", "set", processor.exoQuizz); }; }(this.processor);
		div = cell.appendChild(document.createElement("div"));
		div.id = "type";
		div.style.display = "none";
		div.appendChild(document.createTextNode(""));
		input = div.appendChild(document.createElement("input"));
		input.type = "text";
		input.value = quizz.type;
		cell = celltemp;
		cell.style.width = "22%";
		cell.style.textAlign = "left";
		let element: HTMLElement;
		switch (quizz.type)
		{
			case "number":
				element = cell.appendChild(this.AddExoParameter("max-length", "number", "max-length", quizz));
				element.onchange = function (dom: QAdminGenerator, name: string, quizz: Exogen, cell: HTMLTableCellElement) { return function (event: any) { let parameter: ParameterValue = dom.processor.GetExoQuizzParameter(quizz, name); if (!parameter) { let object: ParameterValue = { name: name, value: event.target.value }; if (!quizz.parameters) quizz.parameters = new Set<ParameterValue>(); quizz.parameters.add(object); } else parameter.value = event.target.value; dom.processor.historic.StoreInHistoric("exoquizz", "set", dom.processor.exoQuizz); dom.GenerateExoSimulation(cell, dom.processor.exoQuizz[id]); }; }(this, "max-length", quizz, cellSim);
				element.onblur = function (processor: QAdminProcessor) { return function () { processor.historic.StoreInHistoric("exoquizz", "set", processor.exoQuizz); }; }(this.processor);
				break;
			case "checkbox":
			case "radio":
				element = cell.appendChild(document.createElement("button"));
				element.appendChild(document.createTextNode( this.processor.configuration.addExoQuizzRadioValue));
				div = cell.appendChild(document.createElement("div"));
				div.id = "values";
				this.AppendQuizzValues(div, quizz);
				element.onclick = function (element: HTMLElement, dom: QAdminGenerator, quizz: Exogen, cell: HTMLTableCellElement)
				{
					return function ()
					{
						let input: HTMLInputElement = element.appendChild(document.createElement("input"));
						input.type = "text";
						if (!quizz.values)
							quizz.values = new Array<string>();
						input.onchange = function (dom: QAdminGenerator, id: number, quizz: Exogen, cell: HTMLTableCellElement)
						{
							return function (event: any)
							{
								quizz.values[id] = event.target.value;
								dom.GenerateExoSimulation(cell, quizz);
							};
						}(dom, quizz.values.length, quizz, cell);
						quizz.values.push("");
						input.onblur = function (processor: QAdminProcessor) { return function () { processor.historic.StoreInHistoric("exoquizz", "set", processor.exoQuizz); }; }(dom.processor);

					};
				}(div, this, quizz, cellSim);
				break;
			case "range":
				element = cell.appendChild(this.AddExoParameter("max", "number", "min", quizz));
				element.onchange = function (dom: QAdminGenerator, name: string, quizz: Exogen, cell: HTMLTableCellElement) { return function (event: any) { let parameter: ParameterValue = dom.processor.GetExoQuizzParameter(quizz, name); if (!parameter) { let object: ParameterValue = { name: name, value: event.target.value }; if (!quizz.parameters) quizz.parameters = new Set<ParameterValue>(); quizz.parameters.add(object); } else parameter.value = event.target.value; dom.processor.historic.StoreInHistoric("exoquizz", "set", dom.processor.exoQuizz); dom.GenerateExoSimulation(cell, dom.processor.exoQuizz[id]);}; }(this, "min", quizz, cellSim);
				element.onblur = function (processor: QAdminProcessor) { return function () { processor.historic.StoreInHistoric("exoquizz", "set", processor.exoQuizz); }; }(this.processor);
				element = cell.appendChild(this.AddExoParameter("max", "number", "max", quizz));
				element.onchange = function (dom: QAdminGenerator, name: string, quizz: Exogen, cell: HTMLTableCellElement) { return function (event: any) { let parameter: ParameterValue = dom.processor.GetExoQuizzParameter(quizz, name); if (!parameter) { let object: ParameterValue = { name: name, value: event.target.value }; if (!quizz.parameters) quizz.parameters = new Set<ParameterValue>(); quizz.parameters.add(object); } else parameter.value = event.target.value; dom.processor.historic.StoreInHistoric("exoquizz", "set", dom.processor.exoQuizz); dom.GenerateExoSimulation(cell, dom.processor.exoQuizz[id]);}; }(this, "max", quizz, cellSim);
				element.onblur = function (processor: QAdminProcessor) { return function () { processor.historic.StoreInHistoric("exoquizz", "set", processor.exoQuizz); }; }(this.processor);
				element = cell.appendChild(this.AddExoParameter("step", "number", "step", quizz));
				element.onchange = function (dom: QAdminGenerator, name: string, quizz: Exogen, cell: HTMLTableCellElement) { return function (event: any) { let parameter: ParameterValue = dom.processor.GetExoQuizzParameter(quizz, name); if (!parameter) { let object: ParameterValue = { name: name, value: event.target.value }; if (!quizz.parameters) quizz.parameters = new Set<ParameterValue>(); quizz.parameters.add(object); } else parameter.value = event.target.value; dom.processor.historic.StoreInHistoric("exoquizz", "set", dom.processor.exoQuizz); dom.GenerateExoSimulation(cell, dom.processor.exoQuizz[id]);}; }(this, "step", quizz, cellSim);
				element.onblur = function (processor: QAdminProcessor) { return function () { processor.historic.StoreInHistoric("exoquizz", "set", processor.exoQuizz); }; }(this.processor);
				element = cell.appendChild(document.createElement("button"));
				element.appendChild(document.createTextNode(this.processor.configuration.addExoQuizzRangeValue));
				this.AppendQuizzValues(div, quizz);
				div = cell.appendChild(document.createElement("div"));
				div.id = "values";
				element.onclick = function (element: HTMLElement, dom: QAdminGenerator, quizz: Exogen, cell: HTMLTableCellElement) {
					return function ()
					{
						if (!quizz.values)
							quizz.values = new Array<Option>();
						let div: HTMLDivElement = element.appendChild(document.createElement("div"));
						let input: HTMLInputElement = div.appendChild(document.createElement("input"));
						input.type = "number";
						input.onchange = function (dom: QAdminGenerator, id: number, quizz: Exogen, cell: HTMLTableCellElement) {
							return function (event: any)
							{
								quizz.values[id].value = event.target.value;
								dom.GenerateExoSimulation(cell, quizz);
							};
						}(dom, quizz.values.length, quizz, cell);
						input.onblur = function (processor: QAdminProcessor) { return function () { processor.historic.StoreInHistoric("exoquizz", "set", processor.exoQuizz); }; }(dom.processor);
						input = div.appendChild(document.createElement("input"));
						input.type = "text";
						input.onchange = function (dom: QAdminGenerator, id: number, quizz: Exogen, cell: HTMLTableCellElement) {
							return function (event: any) {
								quizz.values[id].label = event.target.value;
								dom.GenerateExoSimulation(cell, quizz);
							};
						}(dom, quizz.values.length, quizz, cell);
						input.onblur = function (processor: QAdminProcessor) { return function () { processor.historic.StoreInHistoric("exoquizz", "set", processor.exoQuizz); }; }(dom.processor);
						quizz.values.push(<Option>{ value: "", label: "" });

					};
				}(div, this, quizz, cellSim);
				break;
		}
		element = cell.appendChild(this.AddExoParameter("required", "checkbox", "isRequired", quizz));
		element.onchange = function (dom: QAdminGenerator, name: string, quizz: Exogen, cell: HTMLTableCellElement) { return function (event: any) { let parameter: ParameterValue = dom.processor.GetExoQuizzParameter(quizz, name); if (!parameter) { let object: ParameterValue = { name: name, value: event.target.checked }; if (!quizz.parameters) quizz.parameters = new Set<ParameterValue>(); quizz.parameters.add(object); } else parameter.value = event.target.checked; dom.processor.historic.StoreInHistoric("exoquizz", "set", dom.processor.exoQuizz); dom.GenerateExoSimulation(cell, dom.processor.exoQuizz[id]); }; }(this, "required", quizz, cellSim);
		element.onblur = function (processor: QAdminProcessor) { return function () { processor.historic.StoreInHistoric("exoquizz", "set", processor.exoQuizz); }; }(this.processor);
		/*	input=cell.appendChild(document.createElement("button"));
	
			input.appendChild(document.createTextNode( this.processor.configuration.addExoQuizzParameter));
			div=cell.appendChild(document.createElement("div"));
			div.id="parameters";
			this.AppendQuizzParameters(div,quizz);
			input.onclick=function(element,id){return function(event){element.appendChild(this.AddDefaultExoParameter(id));};}(div,id);
	*/
		cellSim.style.width = "22%";
		cellSim.style.textAlign = "center";
		this.GenerateExoSimulation(cellSim, quizz);

		cell = row.insertCell();
		cell.style.textAlign = "right";
		
		let button: HTMLButtonElement = cell.appendChild(document.createElement("button"));
		button.appendChild(document.createTextNode(this.processor.configuration.deleteExoQuizzText));
		button.onclick = function (dom: QAdminGenerator, id: number) { return function () { dom.processor.DeleteExoQuizz(id); dom.processor.historic.StoreInHistoric("exoquizz", "set", dom.processor.exoQuizz); dom.GenerateExoView(); }; }(this, id);
	}

	SetParameters(node: HTMLElement, parameters: Set<ParameterValue>)
	{
		let isRequired: boolean = false;
		for (let parameter of parameters)
		{
			node[parameter.name] = parameter.value;
			if (parameter.name === "required" && (parameter.value === true || parameter.value === "true"))
			{
				parameter.value = true;
				isRequired = true; 
			}
		}
		return isRequired;
	}

	GenerateExoSimulation(cell: HTMLTableCellElement, quizz: Exogen)
	{
		this.CleanNode(cell);
		let form: HTMLTableElement = cell.appendChild(document.createElement("table"));
		form.className = "tableExo";
		let row: HTMLTableRowElement = form.insertRow();
		row.className = "trExo";
		row.insertCell(0).appendChild(document.createTextNode(quizz.text));
		row.cells[0].className = "tdExo";
		let div: HTMLTableCellElement = row.insertCell(1);
		let input:HTMLInputElement, label: HTMLLabelElement, radio: HTMLDivElement, checkbox: HTMLDivElement, range: HTMLDivElement, datalist: HTMLDataListElement, option: HTMLOptionElement, asterix: boolean;
		switch (quizz.type)
		{
			case "text":
				input = document.createElement("input");
				input.id = quizz.id;
				input.type = "text";
				if (quizz.parameters && this.SetParameters(input, quizz.parameters))
					form.rows[0].cells[0].innerHTML += "(*)";
				div.appendChild(input);
				break;
			case "number":
				input = document.createElement("input");
				input.id = quizz.id;
				input.type = "number";
				if (quizz.parameters && this.SetParameters(input, quizz.parameters))
					form.rows[0].cells[0].innerHTML += "(*)";
				div.appendChild(input);
				break;
			case "radio":
				radio = document.createElement("div");
				radio.id = quizz.id;
				asterix = false;
				for (let iterator2 = 0; iterator2 < quizz.values.length; iterator2++) {
					input = document.createElement("input");
					input.value = quizz.values[iterator2];
					input.type = "radio";
					input.name = quizz.id;
					input.id = quizz.id + "_" + iterator2;
					radio.appendChild(input);
					label = document.createElement("label");
					label.innerHTML = quizz.values[iterator2];
					label.htmlFor = input.id;
					if (quizz.parameters && this.SetParameters(input, quizz.parameters))
						asterix = true;
					radio.appendChild(label);
				}
				if (asterix)
					form.rows[0].cells[0].innerHTML += "(*)";
				div.appendChild(radio);
				break;
			case "checkbox":
				checkbox = document.createElement("div");
				asterix = false;
				checkbox.id = quizz.id;
				for (let iterator2 = 0; iterator2 < quizz.values.length; iterator2++) {
					input = document.createElement("input");
					input.value = quizz.values[iterator2];
					input.type = "checkbox";
					input.name = quizz.id;
					input.id = quizz.id + "_" + quizz.values[iterator2];
					checkbox.appendChild(input);
					label = document.createElement("label");
					label.innerHTML = quizz.values[iterator2];
					label.htmlFor = input.id;
					checkbox.appendChild(label);
					if (quizz.parameters && this.SetParameters(input, quizz.parameters))
						asterix = true;
				}
				if (asterix)
					form.rows[0].cells[0].innerHTML += "(*)";
				div.appendChild(checkbox);
				break;
			case "range":
				range = document.createElement("div");
				range.id = quizz.id;
				input = document.createElement("input");
				input.type = "range";
				input.className = "rangeExo";
				input.id = quizz.id;
				if (quizz.values && quizz.values.length)
				{
					let datalistId: string = "datalist_" + quizz.id;
					input.setAttribute("list", datalistId);
					datalist = document.createElement("datalist");
					datalist.id = datalistId;
					for (let iterator2 = 0; iterator2 < quizz.values.length; iterator2++) {
						option = document.createElement("option");
						option.value = quizz.values[iterator2].value;
						option.label = quizz.values[iterator2].label;
						datalist.appendChild(option);
					}
					div.appendChild(datalist);
				}
				else
					(<any>input).values = quizz.values;
				if (quizz.parameters && this.SetParameters(input, quizz.parameters))
					form.rows[0].cells[0].innerHTML += "(*)";
				range.appendChild(input);
				div.appendChild(range);
				break;
		}
	}

	GenerateExoView() {
		this.DeleteExoView();
		for (let iterator in this.processor.exoQuizz)
			this.GenerateExoQuizz(this.processor.exoQuizz[iterator], Number.parseInt(iterator));
	}

	DeleteExoView() {
		let div = document.getElementById("exoDiv");
		let form = document.getElementById("exoTable");
		if (form)
			div.removeChild(form);
	}

	AddTextToWall(text, isBig: boolean)
	{
		switch (text.step)
		{
			case 'Presentation':
				if (!this.processor.isPresentation)
					return;
				break;
			case 'Interview':
				if (!this.processor.isActive.interview)
					return;
				break;
			case 'Exogen':
				if (!this.processor.isActive.exogen)
					return;
				break;
			case 'End':
				if (this.processor.isActive.exogen && this.processor.isSendButtonsAtExogen)
					return;
				break;
		}
		let form: HTMLTableElement = <HTMLTableElement>document.getElementById("textsTable");
		let row: HTMLTableRowElement = form.insertRow();
		let cell: HTMLTableCellElement = row.insertCell();
		cell.style.width = "25%";
		let textNode: HTMLSpanElement = cell.appendChild(document.createElement("span"));
		textNode.innerHTML = text.text;
		cell = row.insertCell();
		cell.style.width = "75%";
		let input: HTMLTextAreaElement = cell.appendChild(document.createElement("textarea"));
		//let input=cell.appendChild(document.createElement("div"));
		input.id = text.id;
		let pText: Text = this.processor.GetTextValue(text.id);
		if (text.default && !pText)
		{
			this.processor.SetTextValue(text.id, text.default);
			input.value = text.default;
		}
		else if (pText)
			input.value = pText.value;
		else if (this.processor.texts[text.id])
			this.processor.SetTextValue(text.id, "");
		if (isBig)
			input.className = "bigtextarea";
		input.onfocus = function (editor: TextEditor) { return function (event: any) { editor.Show(event.target); }; }(this.editor);
		input.onchange = function (processor: QAdminProcessor) {return function (event: any) { processor.SetTextValue(event.target.id, event.target.value); }; }(this.processor);
		input.onblur = function (processor: QAdminProcessor) {return function (event: any) { processor.SetText(event.target.id, event.target.value); }; }(this.processor);
	}

	GenerateWallOfTexts()
	{
		let div: HTMLElement = document.getElementById("texts");
		if (document.getElementById("textsTable"))
			div.removeChild(document.getElementById("textsTable"));
		let form: HTMLTableElement = div.appendChild(document.createElement("table"));
		form.id = "textsTable";
		for (let iterator in this.processor.configuration.texts)
			this.AddTextToWall(this.processor.configuration.texts[iterator], Number.parseInt(iterator) < 5);
	}

	UpdateWallOfTexts()
	{
		if (document.getElementById("textsTable"))
			this.GenerateWallOfTexts();
	}

	DeleteWallOfTexts()
	{
		if (document.getElementById("textsTable"))
			document.getElementById("texts").removeChild(document.getElementById("textsTable"));
	}

	SetTextValue(nodeid: string, value: string)
	{
		let node: HTMLTextAreaElement = <HTMLTextAreaElement>document.getElementById(nodeid);
		if (node)
			node.value = value;
	}

	SetInterview()
	{
		this.SetInputValue("interviewInput", this.processor.interviewNumber.toString());
		let radio: HTMLInputElement = <HTMLInputElement>document.getElementById("interviewInput");
		radio.disabled = this.processor.interviewType == 0;
		this.SetRadioToValue("interviewType", this.processor.interviewType);
	}

	SetRadioToValue(id: string, value: number)
	{
		let radio: any = document.getElementById(id);//radio element
		if (!radio || radio.childNodes.length <= value)
			return;
		for (let iterator: number = 0; iterator < radio.children.length; iterator++) {
			if (radio.children[iterator].value === value.toString())
				radio.children[iterator].checked = true;
			else
				radio.children[iterator].checked = false;
		}
	}

	SetCheckboxToValue(id: string, value: boolean)
	{
		let chekbox: HTMLInputElement = <HTMLInputElement>document.getElementById(id);
		if (value===false)
			chekbox.checked = false;
		else
			chekbox.checked = true;
	}

	SetQSortInputValue()
	{
		let tab: Array<number>;
		for (let key in this.processor.qSort)
			tab[tab.length] = Number.parseInt(key);
		tab.sort(function (a, b) { return b - a; });
		this.SetInputValue("qsortmin", tab[0].toString());
		this.SetInputValue("qsortmax", tab[tab.length - 1].toString());
	}

	SetInputValue(id: string, value: string)
	{
		let input: HTMLInputElement = <HTMLInputElement>document.getElementById(id);
		if (!input)
			return;
		input.value = value;
	}
}
